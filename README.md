# Eclipse openK User Modules - Statement Public Affairs Frontend

This application represents a user module for the 
[Eclipse openK User Modules](https://projects.eclipse.org/projects/technology.openk-usermodules)
project.

## Prerequisites

With `npm` in the path variable, run first `npm install` to 
download and install all dependencies of this module.
After that, all npm scripts for deploying, developing and 
testing are available.

Additionally, archiving the build as WAR file requires also
the Java Archiver command line tool in the path variable.

## Configuration

The whole application can be configured via certain properties in
file `./app.config.json`. Changes to these properties take only 
effect after (re-)building the application.

* `routes.spaBackend`: Route on which the website's backend is 
served
* `routes.portal`: Route on which the main portal is served 
* `routes.contactDataBase`: Route on which the contact data base  
module is served
* `routes.userDocu`: Route on which the user documentation is served
* `leaflet.urlTemplate`: URL template to the map tile server 
required by [Leaflet](https://leafletjs.com)
* `leaflet.attribution`: Attribution which is added to all 
Leaflet maps
* `leaflet.lat`/`leaflet.lng`/`leaflet.zoom`: Default coordinates
and zoom level to which all leaflet maps are initially configured
* `gis.urlTemplate`: URL template to the geographic information 
system (GIS)
* `gis.projectionFrom`: Coordinate projection used by the 
configured Leaflet tile server
* `gis.projectionTo`: Coordinate projection used in the 
configured GIS
* `nominatim.url`: URL to a [Nominatim](https://nominatim.org)
geocoding service
* `nominatim.searchQueryPrefix`: Prefix which is added automatically 
to every [Nominatim](https://nominatim.org) search query

The following key words are replaced in the given GIS URL template
for each map view:
* `{centerX}`, `{centerY}`: Center coordinates
* `{northEastX}`, `{northEastY}`: North east boundary coordinates
* `{northWestX}`, `{northWestY}`: North west boundary coordinates
* `{southEastX}`, `{southEastY}`: South east boundary coordinates
* `{southWestX}`, `{southWestY}`: South west boundary coordinates
* `{user}`: User name

All coordinates are transformed via a HTTP call to the back end.
For all available projections, please visit [Proj4j](https://trac.osgeo.org/proj4j/). 

## Build

Building the application is done via the Angular CLI or by the
provided scripts in the `./package.json` (which also provide all
necessary configuration steps):

* `npm run build` Build the application via the Angular CLI 
to the `./dist/statement-public-affairs` folder
* `npm run archive` Archive the build as WAR file 
to `./dist/statement-public-affairs-frontend.war`

Note that the archiving script requires the Java Archiver
(`jar`) command line tool in the path variable.

## Development

Run `npm run start` for setting up a local dev server which is
served on http://localhost:4200/. The local dev server is also
a proxy which can be configured by the file `./proxy.conf.json`.
It redirects all calls to the back end to a specific location.

Run `npm run test` to execute all tests via Karma and Jasmine.
This also sets up a local dev server with live reload.

Run `npm run test:report` instead to generate a coverage report
at the folder `./coverage/statement-public-affairs`.

Run `npm run lint` for linting all source files, both
typescript and (s)css code.

Run `npm run lint:report` to generate instead linting report
files at `./lint.ts.report.json` and `./lint.styles.report.json`.

Run `npm run sonar` to upload the test and linting reports
to a sonarqube instance via the sonar-scanner.
This can be configured either by the 
`./sonar-projects.properties` file or by arguments on the command 
line. For example, uploading the reports to a different
port can be done via:

```
npm run sonar -- -Dsonar.host.url=http://localhost:9001
```


## Storybook

Run `start:storybook` for setting up a local storybook server.

Run `build:storybook` instead for building the storybook webpage
for later use.

## Miscellaneous Scripts

Run `npm run list-licenses` to generate a listing of all packages
and their licenses used for production. A file is created at 
`./licenses.txt`. 
The content of this file is required in the `./NOTICE.md` file.

Run `npm run check-legal-headers` to check if all source files 
have a specific legal header.

## Git Hooks

Run `npm run pre-commit` to check if a commit passes the pre-commit
script. This script concatenates `npm run lint` and
`npm run check-legal-headers`.
