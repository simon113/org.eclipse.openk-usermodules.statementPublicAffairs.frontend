*******************************************************************************
  Copyright (c) 2019 Contributors to the Eclipse Foundation

  See the NOTICE file(s) distributed with this work for additional
  information regarding copyright ownership.

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License v. 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
*******************************************************************************

# Build environment

The 'build' script creates reproducable artifact builds.
It uses an node based docker image.

## Usage

To build the artifact, run the compile script from the project base folder (where the package.json is located).

The script deletes the target folder and then runs node build commands.

After a successful build, the artifact is located in the 'dist' folder.


