/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIAttachmentModel, IAPIAttachmentTag} from "../../../core/api/attachments";
import {createAttachmentFileMock, createAttachmentModelMock} from "../../../test";
import {IAttachmentsStoreState} from "../model";
import {
    getAttachmentControlValueSelector,
    getFilteredAttachmentTagsSelector,
    getOutboxAttachments,
    getStatementAttachmentsSelector,
    getStatementPdfAttachment
} from "./attachments.selectors";

describe("attachmentsSelectors", () => {

    const state: IAttachmentsStoreState = {
        entities: {
            17: createAttachmentModelMock(17, "tag17"),
            18: createAttachmentModelMock(18, "tag18"),
            19: createAttachmentModelMock(19, "tag19")
        },
        statementAttachments: {
            1919: [17, 19]
        },
        statementCache: {
            1919: [
                createAttachmentFileMock("Test1.pdf"),
                createAttachmentFileMock("Test2.pdf")
            ]
        }
    };

    it("getFilteredAttachmentTagsSelector", () => {
        const tags: IAPIAttachmentTag[] = Array(100).fill(0)
            .map((_, id) => ({id: "id" + id, label: "Tag" + id}));
        const projector = getFilteredAttachmentTagsSelector.projector;
        expect(projector(tags, {without: null})).toEqual(tags);
        expect(projector(tags, {without: ["id18", "id19", "id199"]}))
            .toEqual(tags.filter((_) => _.id !== "id18" && _.id !== "id19"));
    });

    it("getStatementAttachmentsSelector", () => {
        const projector = getStatementAttachmentsSelector.projector;
        expect(projector(null, null, null)).toEqual([]);
        expect(projector([17, 19], state.entities, null)).toEqual([state.entities[17], state.entities[19]]);
        expect(projector([17, 18, 19], state.entities, {
            restrictedTagIds: ["tag17", "tag18"]
        })).toEqual([state.entities[17], state.entities[18]]);
        expect(projector([17, 18, 19], state.entities, {
            forbiddenTagIds: ["tag17", "tag18"]
        })).toEqual([state.entities[19]]);
    });

    it("getAttachmentControlValueSelector", () => {
        const projector = getAttachmentControlValueSelector.projector;
        expect(projector([{id: 19, name: "name", tagIds: []}], null))
            .toEqual([{id: 19, name: "name", tagIds: [], isSelected: true}]);
    });

    it("getOutboxAttachments", () => {
        const projector = getOutboxAttachments.projector;
        const input: IAPIAttachmentModel[] = [
            {id: 19, name: "name", tagIds: ["outbox"]}, {id: 20, name: "name", tagIds: []}
        ] as IAPIAttachmentModel[];
        const expectedResult = [{id: 19, name: "name", tagIds: ["outbox"]}] as IAPIAttachmentModel[];
        expect(projector(input))
            .toEqual(expectedResult);
    });

    it("getStatementPdfAttachment", () => {
        const projector = getStatementPdfAttachment.projector;
        const input: IAPIAttachmentModel[] = [
            {id: 19, name: "name", tagIds: ["statement"]}, {id: 20, name: "name", tagIds: []}
        ] as IAPIAttachmentModel[];
        const expectedResult = [{id: 19, name: "name", tagIds: ["statement"]}] as IAPIAttachmentModel[];
        expect(projector(input))
            .toEqual(expectedResult);
    });

});
