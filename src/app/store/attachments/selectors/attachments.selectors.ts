/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createFeatureSelector, createSelector} from "@ngrx/store";
import {IAPIAttachmentModel, IAPIAttachmentTag} from "../../../core/api/attachments";
import {
    arrayJoin,
    filterDistinctValues,
    selectArrayProjector,
    selectEntityWithIdProjector,
    selectPropertyProjector,
    TStoreEntities
} from "../../../util/store";
import {queryParamsIdSelector} from "../../root/selectors";
import {ATTACHMENTS_NAME} from "../attachments-reducers.token";
import {IAttachmentControlValue, IAttachmentsStoreState} from "../model";

export const attachmentsStoreStateSelector = createFeatureSelector<IAttachmentsStoreState>(ATTACHMENTS_NAME);

const attachmentsEntitiesSelector = createSelector(
    attachmentsStoreStateSelector,
    selectPropertyProjector("entities", {})
);

const getStatementAttachmentIdEntities = createSelector(
    attachmentsStoreStateSelector,
    selectPropertyProjector("statementAttachments", {})
);

const getStatementAttachmentCacheEntitiesSelector = createSelector(
    attachmentsStoreStateSelector,
    selectPropertyProjector("statementCache", {})
);


export const getAttachmentTagsSelector = createSelector(
    attachmentsStoreStateSelector,
    selectArrayProjector("tags", [])
);

export const getFilteredAttachmentTagsSelector = createSelector(
    getAttachmentTagsSelector,
    (tags: IAPIAttachmentTag[], props: { without: string[] }) => {
        return arrayJoin(tags).filter((_) => filterDistinctValues(props.without).indexOf(_.id) === -1);
    }
);


export const getStatementAttachmentIds = createSelector(
    getStatementAttachmentIdEntities,
    queryParamsIdSelector,
    selectEntityWithIdProjector()
);

export const getStatementAttachmentsSelector = createSelector(
    getStatementAttachmentIds,
    attachmentsEntitiesSelector,
    (
        attachmentIds: number[],
        attachmentEntities: TStoreEntities<IAPIAttachmentModel>,
        props: { restrictedTagIds?: string[], forbiddenTagIds?: string[] }
    ): IAPIAttachmentModel[] => {
        const restrictedTagIds = arrayJoin(props?.restrictedTagIds);
        const forbiddenTagIds = arrayJoin(props?.forbiddenTagIds);
        return arrayJoin(attachmentIds)
            .map((attachmentId) => attachmentEntities[attachmentId])
            .filter((attachment) => {
                return restrictedTagIds.length === 0 || attachment.tagIds.some((_) => restrictedTagIds.indexOf(_) > -1);
            })
            .filter((attachment) => {
                return forbiddenTagIds.length === 0 || !attachment.tagIds.some((_) => forbiddenTagIds.indexOf(_) > -1);
            });
    }
);

export const getAllStatementAttachments = createSelector(
    getStatementAttachmentIds,
    attachmentsEntitiesSelector,
    (attachmentIds: number[],
     attachmentEntities: TStoreEntities<IAPIAttachmentModel>) => {
        return arrayJoin(attachmentIds)
            .map((attachmentId) => attachmentEntities[attachmentId]);
    }
);

export const getOutboxAttachments = createSelector(
    getAllStatementAttachments,
    (attachments) => {
        return arrayJoin(attachments).filter((attachment) =>
            attachment?.tagIds.some((_) => _ === "outbox") && attachment?.tagIds?.length === 1);
    }
);

export const getConsiderationAttachments = createSelector(
    getAllStatementAttachments,
    (attachments) => {
        return arrayJoin(attachments).filter((attachment) =>
            attachment?.tagIds.some((_) => _ === "consideration") && attachment?.tagIds?.length === 1);
    }
);

export const getStatementPdfAttachment = createSelector(
    getAllStatementAttachments,
    (attachments) => {
        return arrayJoin(attachments).filter((attachment) => attachment?.tagIds.some((_) => _ === "statement"));
    }
);

export const getStatementAttachmentCacheSelector = createSelector(
    getStatementAttachmentCacheEntitiesSelector,
    queryParamsIdSelector,
    selectEntityWithIdProjector()
);

export const getAttachmentControlValueSelector = createSelector(
    getStatementAttachmentsSelector,
    (attachments: IAPIAttachmentModel[], props: { restrictedTagIds?: string[], forbiddenTagIds?: string[] }): IAttachmentControlValue[] => {
        return attachments.map<IAttachmentControlValue>((_) => {
            return {
                ..._,
                isSelected: true
            };
        });
    }
);
