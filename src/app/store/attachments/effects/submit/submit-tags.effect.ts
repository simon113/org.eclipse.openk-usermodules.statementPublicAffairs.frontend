/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {concat, EMPTY, Observable, of} from "rxjs";
import {catchError, endWith, ignoreElements, mergeMap, startWith, switchMap} from "rxjs/operators";
import {AttachmentsApiService} from "../../../../core/api/attachments";
import {endWithObservable, ignoreError} from "../../../../util";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {setSettingsLoadingStateAction} from "../../../settings/actions";
import {submitTagsAction} from "../../../statements/actions";
import {fetchAttachmentTagsAction} from "../../actions";

@Injectable({providedIn: "root"})
export class SubmitTagsEffect {

    public submit$ = createEffect(() => this.actions.pipe(
        ofType(submitTagsAction),
        switchMap((action) => {
            return this.submit(action.labels).pipe(
                ignoreError()
            );
        })
    ));

    public constructor(
        public readonly actions: Actions,
        public readonly attachmentsApiService: AttachmentsApiService
    ) {

    }

    public submit(
        labels: string[]
    ): Observable<Action> {
        const errors: string[] = [];
        return this.addTags(labels, errors).pipe(
            endWithObservable(() => {
                return errors.length > 0 ? concat(
                    of(setErrorAction({error: EErrorCode.COULD_NOT_ADD_TAG, errorValue: {value: errors.toString().replace(/,/g, ", ")}})),
                    of(fetchAttachmentTagsAction())
                ) : of(fetchAttachmentTagsAction());
            }),
            startWith(setSettingsLoadingStateAction({state: {addingTags: true}})),
            endWith(setSettingsLoadingStateAction({state: {addingTags: false}}))
        );
    }

    public addTags(labels: string[], errors: any[]): Observable<Action> {
        return of(...labels).pipe(
            mergeMap((item) => {
                return this.attachmentsApiService.addNewTag(item).pipe(
                    catchError(() => {
                        errors.push(item);
                        return EMPTY;
                    }),
                    ignoreElements()
                );
            })
        );
    }

}
