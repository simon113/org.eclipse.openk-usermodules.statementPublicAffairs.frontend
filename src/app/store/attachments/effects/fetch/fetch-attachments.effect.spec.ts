/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {Observable, of, Subscription} from "rxjs";
import {IAPIAttachmentModel, IAPIAttachmentTag, SPA_BACKEND_ROUTE} from "../../../../core";
import {createAttachmentTagList} from "../../../../test";
import {fetchAttachmentsAction, fetchAttachmentTagsAction, setAttachmentsAction, setAttachmentTagsAction} from "../../actions";
import {FetchAttachmentsEffect} from "./fetch-attachments.effect";

describe("FetchAttachmentsEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: FetchAttachmentsEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                FetchAttachmentsEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(FetchAttachmentsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch attachments for a statement", () => {
        const statementId = 19;
        const tagList = createAttachmentTagList();
        const fetchAttachmentResult: IAPIAttachmentModel[] = [
            {
                id: 1,
                name: "fileName",
                type: "file",
                size: 150,
                timestamp: "2007-08-31T16:47+00:00",
                tagIds: []
            }
        ];

        const expectedResults = [
            setAttachmentsAction({statementId, entities: fetchAttachmentResult}),
            setAttachmentTagsAction({tags: tagList})
        ];
        const results: Action[] = [];

        actions$ = of(fetchAttachmentsAction({statementId}));
        subscription = effect.fetch$.subscribe((action) => results.push(action));

        expectFetchAttachments(statementId, fetchAttachmentResult);
        expectFetchAttachmentTags(tagList);
        expect(results).toEqual(expectedResults);
        httpTestingController.verify();
    });

    it("should fetch a list of attachment tags", () => {
        const tagList = createAttachmentTagList();
        const expectedResults = [
            setAttachmentTagsAction({tags: tagList})
        ];
        const results: Action[] = [];

        actions$ = of(fetchAttachmentTagsAction());
        subscription = effect.fetchTags$.subscribe((action) => results.push(action));
        expectFetchAttachmentTags(tagList);
        expect(results).toEqual(expectedResults);
        httpTestingController.verify();
    });

    it("should not dispatch any further actions when supplied with insufficient data", () => {
        const statementId = null;
        const results: Action[] = [];

        actions$ = of(fetchAttachmentsAction({statementId}));
        subscription = effect.fetch$.subscribe((action) => results.push(action));

        expect(results).toEqual([]);
        httpTestingController.verify();
    });

    function expectFetchAttachments(statementId: number, returnValue: IAPIAttachmentModel[]) {
        const url = `/statements/${statementId}/attachments`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

    function expectFetchAttachmentTags(returnValue: IAPIAttachmentTag[]) {
        const url = `/tags`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

});

