/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {IAPIAttachmentModel} from "../../../../core/api";
import {arrayJoin, deleteEntities, setEntitiesObject, TStoreEntities, updateEntitiesObject} from "../../../../util/store";
import {addAttachmentEntityAction, deleteAttachmentsAction, setAttachmentsAction, updateAttachmentTagsAction} from "../../actions";

export const attachmentEntitiesReducer = createReducer<TStoreEntities<IAPIAttachmentModel>>(
    {},
    on(setAttachmentsAction, (state, payload) => {
        return setEntitiesObject(state, payload.entities, (_) => _.id);
    }),
    on(addAttachmentEntityAction, (state, payload) => {
        return updateEntitiesObject(state, [payload.entity], (_) => _.id);
    }),
    on(deleteAttachmentsAction, (state, payload) => {
        return deleteEntities(state, payload.entityIds);
    }),
    on(updateAttachmentTagsAction, (state, payload) => {
        return updateEntitiesObject(state, arrayJoin(payload.items).map((_) => ({id: _.id, tagIds: _.tagIds})), (_) => _.id);
    })
);
