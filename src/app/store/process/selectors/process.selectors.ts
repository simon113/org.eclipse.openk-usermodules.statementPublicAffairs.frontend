/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createFeatureSelector, createSelector} from "@ngrx/store";
import {arrayJoin, selectEntityWithIdProjector, selectPropertyProjector} from "../../../util/store";
import {queryParamsIdSelector, queryParamsTaskIdSelector} from "../../root/selectors";
import {IProcessStoreState} from "../model";
import {PROCESS_FEATURE_NAME} from "../process-reducers.token";

export const processStateSelector = createFeatureSelector<IProcessStoreState>(PROCESS_FEATURE_NAME);

export const statementTasksSelector = createSelector(
    processStateSelector,
    queryParamsIdSelector,
    (processState: IProcessStoreState, statementId, props?: { statementId: number }) => {
        if (processState?.statementTasks == null) {
            return [];
        }

        statementId = props?.statementId != null ? props.statementId : statementId;

        const taskIds = statementId == null ? null : processState.statementTasks[statementId];
        return !Array.isArray(taskIds) ? [] : taskIds
            .map((taskId) => processState?.tasks[taskId])
            .filter((task) => task != null);
    }
);

export const taskSelector = createSelector(
    processStateSelector,
    queryParamsTaskIdSelector,
    (processState, taskId, props?: { taskId: string }) => {
        taskId = props?.taskId != null ? props.taskId : taskId;
        return processState?.tasks != null ? processState.tasks[taskId] : undefined;
    }
);

export const processDiagramSelector = createSelector(
    processStateSelector,
    queryParamsIdSelector,
    selectEntityWithIdProjector(undefined, "diagram")
);

export const historySelector = createSelector(
    processStateSelector,
    queryParamsIdSelector,
    selectEntityWithIdProjector(undefined, "history")
);

export const processNameSelector = createSelector(
    historySelector,
    selectPropertyProjector("processName")
);

export const processVersionSelector = createSelector(
    historySelector,
    selectPropertyProjector("processVersion")
);

export const currentActivityIds = createSelector(
    historySelector,
    (history) => arrayJoin(history?.currentProcessActivities)
        .map((activity) => activity.activityId)
);

export const processLoadingSelector = createSelector(
    processStateSelector,
    selectPropertyProjector("loading")
);
