/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which models the loading state of the whole settings store module.
 */
export interface ISettingsLoadingState {
    fetchingDepartments?: boolean;
    submittingDepartments?: boolean;
    fetchingTextblocks?: boolean;
    submittingTextblocks?: boolean;
    addingTags?: boolean;
    fetchingUsers?: boolean;
    submittingUserData?: boolean;
}
