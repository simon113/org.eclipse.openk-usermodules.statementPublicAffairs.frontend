/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {concat, merge, Observable} from "rxjs";
import {map, retry, switchMap} from "rxjs/operators";
import {SettingsApiService} from "../../../core";
import {catchErrorTo, retryAfter} from "../../../util";
import {intializeAction, setErrorAction} from "../../root/actions";
import {EErrorCode} from "../../root/model";
import {fetchSettingsAction, setSectorsAction, setStatementTypesAction} from "../actions";

@Injectable({providedIn: "root"})
export class FetchSettingsEffect {

    public initialize$ = createEffect(() => this.actions.pipe(
        ofType(intializeAction),
        switchMap(() => {
            return concat(
                this.fetchSettings().pipe(retryAfter(30000)),
                this.actions.pipe(
                    ofType(fetchSettingsAction),
                    switchMap(() => this.fetchSettings())
                )
            );
        })
    ));

    public constructor(private readonly actions: Actions, private readonly settingsApiService: SettingsApiService) {

    }

    public fetchSettings(): Observable<Action> {
        return merge<Action>(
            this.fetchStatementTypes(),
            this.fetchSectors()
        );
    }

    public fetchStatementTypes(): Observable<Action> {
        return this.settingsApiService.getStatementTypes().pipe(
            map((statementTypes) => setStatementTypesAction({statementTypes})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED}))
        );
    }

    public fetchSectors(): Observable<Action> {
        return this.settingsApiService.getSectors().pipe(
            map((sectors) => setSectorsAction({sectors})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED}))
        );
    }

}
