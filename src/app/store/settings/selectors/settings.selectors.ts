/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createFeatureSelector, createSelector} from "@ngrx/store";
import {ISelectOption} from "../../../shared/controls/select";
import {entitiesToArray, selectPropertyProjector} from "../../../util";
import {ISettingsStoreState} from "../model";
import {SETTINGS_FEATURE_NAME} from "../settings-reducers.token";

export const settingsStoreSelector = createFeatureSelector<ISettingsStoreState>(SETTINGS_FEATURE_NAME);

export const statementTypesSelector = createSelector(
    settingsStoreSelector,
    (state) => {
        return entitiesToArray(state?.statementTypes)
            .map<ISelectOption<number>>((t) => ({label: t?.name, value: t?.id}));
    }
);

const settingsLoadingStateSelector = createSelector(
    settingsStoreSelector,
    selectPropertyProjector("loading", {})
);

export const getSettingsLoadingSelector = createSelector(
    settingsLoadingStateSelector,
    (loading): boolean => Object.values({...loading}).some((_) => _)
);

export const getDepartmentsSettingsSelector = createSelector(
    settingsStoreSelector,
    selectPropertyProjector("departments", {})
);

export const getUsersSettingsSelector = createSelector(
    settingsStoreSelector,
    selectPropertyProjector("users", [])
);

export const getTextblockSettingsSelector = createSelector(
    settingsStoreSelector,
    selectPropertyProjector("textblock", {selects: {}, groups: [], negativeGroups: []})
);
