/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {arrayJoin, filterDistinctValues} from "../../../../util/store";
import {deleteEmailEntityAction, setEmailInboxAction} from "../../actions";

export const mailInboxReducer = createReducer<string[]>(
    [],
    on(setEmailInboxAction, (state, payload) => {
        return filterDistinctValues(arrayJoin(payload.entities).map((_) => _?.identifier));
    }),
    on(deleteEmailEntityAction, (state, payload) => {
        return arrayJoin(state).filter((_) => _ !== payload.mailId);
    })
);
