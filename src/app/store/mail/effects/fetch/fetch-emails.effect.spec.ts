/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {EMPTY, Observable, Subject, Subscription} from "rxjs";
import {IAPIEmailModel, SPA_BACKEND_ROUTE} from "../../../../core";
import {createEmailModelMock} from "../../../../test";
import {
    fetchEmailAction,
    fetchEmailInboxAction,
    setEmailEntitiesAction,
    setEmailInboxAction,
    setEmailLoadingStateAction
} from "../../actions";
import {FetchEmailsEffect} from "./fetch-emails.effect";

describe("FetchEmailsEffect", () => {
    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: FetchEmailsEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(FetchEmailsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch emails on fetchEmailInboxAction", () => {
        const results: Action[] = [];
        const spy = spyOn(effect, "fetchEmailInbox").and.returnValue(EMPTY);
        const actionSubject = new Subject<Action>();
        actions$ = actionSubject;
        subscription = effect.fetchInbox$.subscribe((_) => results.push(_));

        actionSubject.next(fetchEmailInboxAction());
        expect(spy).toHaveBeenCalledWith();
        expect(results).toEqual([]);
    });

    it("should fetch email on fetchEmailAction", () => {
        const mailId = "<Mail19>";
        const results: Action[] = [];
        const spy = spyOn(effect, "fetchEmail").and.returnValue(EMPTY);
        const actionSubject = new Subject<Action>();
        actions$ = actionSubject;
        subscription = effect.fetchEmail$.subscribe((_) => results.push(_));

        actionSubject.next(fetchEmailAction({mailId}));
        expect(spy).toHaveBeenCalledWith(mailId, undefined);
        spy.calls.reset();

        actionSubject.next(fetchEmailAction({mailId: null}));
        expect(spy).not.toHaveBeenCalled();

        expect(results).toEqual([]);
    });

    it("should fetch email inbox", () => {
        const results: Action[] = [];
        const entities = ["<Mail19>", "<Mail1919>"].map((_) => createEmailModelMock(_));

        subscription = effect.fetchEmailInbox().subscribe((_) => results.push(_));
        expectFetchEmailInboxRequest(entities);

        expect(subscription.closed).toBeTrue();
        expect(results).toEqual([
            setEmailLoadingStateAction({loading: {fetchingInbox: true}}),
            setEmailInboxAction({entities}),
            setEmailLoadingStateAction({loading: {fetchingInbox: false}})
        ]);
    });

    it("should fetch emails", () => {
        const mailId = "<Mail19>";
        const entity = createEmailModelMock(mailId);
        const results: Action[] = [];

        subscription = effect.fetchEmail(mailId).subscribe((_) => results.push(_));
        expectFetchEmailRequest(entity);

        expect(subscription.closed).toBeTrue();
        expect(results).toEqual([
            setEmailLoadingStateAction({loading: {fetching: true}}),
            setEmailEntitiesAction({entities: [entity]}),
            setEmailLoadingStateAction({loading: {fetching: false}})
        ]);
    });

    function expectFetchEmailInboxRequest(result: IAPIEmailModel[]) {
        const url = `/mail/inbox`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(result);
    }

    function expectFetchEmailRequest(result: IAPIEmailModel) {
        const url = `/mail/identifier/${result.identifier}`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(result);
    }

});

