/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {concat, Observable, of} from "rxjs";
import {endWith, exhaustMap, filter, map, retry, startWith, switchMap} from "rxjs/operators";
import {MailApiService} from "../../../../core/api/mail";
import {catchHttpError, EHttpStatusCodes} from "../../../../util/http";
import {catchErrorTo, ignoreError} from "../../../../util/rxjs";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {setStatementErrorAction} from "../../../statements/actions";
import {
    deleteEmailEntityAction,
    fetchEmailAction,
    fetchEmailInboxAction,
    setEmailEntitiesAction,
    setEmailInboxAction,
    setEmailLoadingStateAction
} from "../../actions";

@Injectable({providedIn: "root"})
export class FetchEmailsEffect {

    public fetchInbox$ = createEffect(() => this.actions.pipe(
        ofType((fetchEmailInboxAction)),
        exhaustMap(() => this.fetchEmailInbox())
    ));

    public fetchEmail$ = createEffect(() => this.actions.pipe(
        ofType(fetchEmailAction),
        filter((action) => action.mailId != null),
        switchMap((action) => this.fetchEmail(action.mailId, action.statementId))
    ));

    public constructor(
        public readonly actions: Actions,
        public readonly emailApiService: MailApiService
    ) {

    }

    public fetchEmailInbox(): Observable<Action> {
        return this.emailApiService.getInbox().pipe(
            map((entities) => setEmailInboxAction({entities})),
            startWith(setEmailLoadingStateAction({loading: {fetchingInbox: true}})),
            ignoreError(),
            endWith(setEmailLoadingStateAction({loading: {fetchingInbox: false}}))
        );
    }

    public fetchEmail(mailId: string, statementId?: number | "new"): Observable<Action> {
        return this.emailApiService.getEmail(mailId).pipe(
            map((entity) => setEmailEntitiesAction({entities: [entity]})),
            retry(2),
            catchHttpError(() => {
                return concat(
                    of(deleteEmailEntityAction({mailId})),
                    of(statementId ?
                        setStatementErrorAction({statementId, error: {errorMessage: EErrorCode.COULD_NOT_LOAD_MAIL_DATA}}) :
                        setErrorAction({error: EErrorCode.COULD_NOT_LOAD_MAIL_DATA}))
                );
            }, EHttpStatusCodes.NOT_FOUND),
            catchErrorTo(statementId ?
                setStatementErrorAction({statementId, error: {errorMessage: EErrorCode.COULD_NOT_LOAD_MAIL_DATA}}) :
                setErrorAction({error: EErrorCode.COULD_NOT_LOAD_MAIL_DATA})),
            startWith(setEmailLoadingStateAction({loading: {fetching: true}})),
            ignoreError(),
            endWith(setEmailLoadingStateAction({loading: {fetching: false}}))
        );
    }

}
