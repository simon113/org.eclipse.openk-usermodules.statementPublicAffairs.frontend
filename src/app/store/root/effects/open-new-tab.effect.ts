/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Inject, Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {EMPTY, of} from "rxjs";
import {filter, mergeMap, switchMap} from "rxjs/operators";
import {AuthService, CONTACT_DATA_BASE_ROUTE, SPA_BACKEND_ROUTE, URL_TOKEN, WINDOW} from "../../../core";
import {openContactDataBaseAction, openFileAction, setErrorAction} from "../actions";
import {EErrorCode} from "../model";

@Injectable({providedIn: "root"})
export class OpenNewTabEffect {

    public openContactDataBase$ = createEffect(() => this.actions.pipe(
        ofType(openContactDataBaseAction),
        filter(() => this.authenticationService.token != null),
        switchMap(() => {
            const tab = this.open(this.contactDataBaseRoute, true);
            return tab ? EMPTY : of(setErrorAction({error: EErrorCode.UNEXPECTED}));
        })
    ), {dispatch: true});

    public openFile$ = createEffect(() => this.actions.pipe(
        ofType(openFileAction),
        filter((action) => action.file instanceof File),
        mergeMap((action) => {
            const tab = this.openFile(action.file);
            return tab ? EMPTY : of(setErrorAction({error: EErrorCode.UNEXPECTED}));
        })
    ), {dispatch: true});

    public constructor(
        public actions: Actions,
        private readonly authenticationService: AuthService,
        @Inject(SPA_BACKEND_ROUTE) private readonly spaBackendRoute: string,
        @Inject(CONTACT_DATA_BASE_ROUTE) private readonly contactDataBaseRoute: string,
        @Inject(WINDOW) private readonly window: Window,
        @Inject(URL_TOKEN) private readonly url: typeof URL
    ) {

    }

    public async openFile(file: File) {
        const objectUrl = this.url.createObjectURL(file);
        const tab = await this.open(objectUrl, false, file.name);
        tab.onbeforeunload = () => this.url.revokeObjectURL(objectUrl);
        return tab;
    }

    public async open(url: string, withToken?: boolean, title?: string) {
        url += withToken ? "?accessToken=" + this.authenticationService.token : "";
        const tab = this.window.open(url, "_blank");
        if (title != null) {
            tab.onload = () => tab.document.title = title;
        }
        return tab;
    }

}
