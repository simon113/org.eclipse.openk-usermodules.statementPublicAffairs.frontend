/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {TranslateService} from "@ngx-translate/core";
import {MessageService} from "primeng/api";
import {filter, mergeMap, take} from "rxjs/operators";
import {setErrorAction} from "../actions";

@Injectable({providedIn: "root"})
export class ToastEffect {

    public toast$ = createEffect(() => this.actions.pipe(
        ofType(setErrorAction),
        filter((action) => action.statementId == null && action.error != null),
        mergeMap(async (action) => this.toast(action.error, action.errorValue))
    ), {dispatch: false});

    public constructor(
        public actions: Actions,
        private messageService: MessageService,
        private translateService: TranslateService
    ) {
    }

    public async toast(error: string, errorValue?: { [key: string]: string }) {
        this.messageService.add({
            severity: "error",
            life: 7000,
            summary: await this.getTranslation("shared.errorMessages.title"),
            detail: await this.getTranslation(error, errorValue)
        });
    }

    private async getTranslation(msg: string, value?: { [key: string]: string }) {
        return this.translateService.get(msg, value).pipe(take(1)).toPromise();
    }

}
