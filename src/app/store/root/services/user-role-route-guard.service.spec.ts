/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Location} from "@angular/common";
import {Component, NgZone} from "@angular/core";
import {async, TestBed} from "@angular/core/testing";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {MemoizedSelector} from "@ngrx/store";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {EAPIUserRoles} from "../../../core";
import {isLoadingSelector, OfficialInChargeRouteGuardService, UserRoleRouteGuardService, userRolesSelector} from "../../../store";

describe("UserRoleRouteGuardService", () => {

    let router: Router;
    let location: Location;
    let mockStore: MockStore;
    let service: UserRoleRouteGuardService;
    let userRolesSelectorMock: MemoizedSelector<any, string[]>;
    let isLoadingSelectorMock: MemoizedSelector<any, boolean>;

    function callInZone<T>(fn: () => T | Promise<T>): Promise<T> {
        const ngZone = TestBed.inject(NgZone);
        return new Promise<T>((res, rej) => {
            ngZone.run(() => Promise.resolve().then(() => fn()).then(res).catch(rej));
        });
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestComponent
            ],
            imports: [
                RouterTestingModule.withRoutes([{
                    path: "test",
                    pathMatch: "full",
                    component: TestComponent,
                    canActivate: [OfficialInChargeRouteGuardService]
                }])
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
        service = TestBed.inject(OfficialInChargeRouteGuardService);
        router = TestBed.inject(Router);
        location = TestBed.inject(Location);
        mockStore = TestBed.inject(MockStore);
        userRolesSelectorMock = mockStore.overrideSelector(userRolesSelector, []);
        isLoadingSelectorMock = mockStore.overrideSelector(isLoadingSelector, true);
    }));

    it("should allow access if user has role", async () => {
        userRolesSelectorMock.setResult([EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE]);
        const isRoutingSuccessfulPromise = callInZone(() => router.navigate(["test"]));
        isLoadingSelector.setResult(false);
        mockStore.refreshState();
        await expectAsync(isRoutingSuccessfulPromise).toBeResolvedTo(true);
        expect(location.path()).toBe("/test");
    });

    it("should prevent access if user has not allowed role", async () => {
        userRolesSelectorMock.setResult([EAPIUserRoles.SPA_APPROVER]);
        const isRoutingSuccessfulPromise = callInZone(() => router.navigate(["test"]));
        isLoadingSelector.setResult(false);
        mockStore.refreshState();
        await expectAsync(isRoutingSuccessfulPromise).toBeResolvedTo(true);
        expect(location.path()).toBe("/");
    });

    it("should check if certain user roles are allowed", async () => {
        service.config.allowed = [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE, EAPIUserRoles.SPA_APPROVER];
        expect(service.isUserRoleAllowed(EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE)).toBeTrue();
        expect(service.isUserRoleAllowed(EAPIUserRoles.SPA_APPROVER)).toBeTrue();
        expect(service.isUserRoleAllowed(EAPIUserRoles.DIVISION_MEMBER)).toBeFalse();
        expect(service.isUserRoleAllowed(null)).toBeFalse();

        service.config.allowed = null;
        expect(service.isUserRoleAllowed(EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE)).toBeFalse();

        service.config = null;
        expect(service.isUserRoleAllowed(EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE)).toBeFalse();
    });
});


@Component({template: ""})
class TestComponent {
}
