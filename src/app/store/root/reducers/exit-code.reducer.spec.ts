/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {openExitPageAction} from "../actions";
import {EExitCode} from "../model";
import {exitCodeReducer} from "./exit-code.reducer";

describe("exitCodeReducer", () => {

    it("should set the payload code as state", () => {
        let initialState: EExitCode;
        let action = openExitPageAction({code: EExitCode.FORBIDDEN});
        let state = exitCodeReducer(initialState, action);
        expect(state).toEqual(EExitCode.FORBIDDEN);

        initialState = EExitCode.LOGOUT;
        action = openExitPageAction(undefined);
        state = exitCodeReducer(initialState, action);
        expect(state).toEqual(undefined);

        initialState = EExitCode.NO_TOKEN;
        action = openExitPageAction({code: null});
        state = exitCodeReducer(initialState, action);
        expect(state).toEqual(null);
    });

});
