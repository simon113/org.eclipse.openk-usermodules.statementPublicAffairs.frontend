/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Action} from "@ngrx/store";
import {EAPIUserRoles, IAPIUserInfo} from "../../../core";
import {clearUserAction, setUserAction} from "../actions";
import {userReducer} from "./user.reducer";

describe("userReducer", () => {

    it("should set the user information to the state", () => {
        const user: IAPIUserInfo = {
            firstName: "firstName",
            lastName: "lastName",
            roles: [
                EAPIUserRoles.DIVISION_MEMBER,
                EAPIUserRoles.ROLE_SPA_ACCESS
            ],
            userName: "userName"
        };
        let action: Action = setUserAction(null);
        let state = userReducer(undefined, action);
        expect(state).toEqual(undefined);

        action = setUserAction({user});
        state = userReducer(undefined, action);
        expect(state).toEqual(user);

        action = clearUserAction();
        state = userReducer(user, action);
        expect(state).toEqual(undefined);
    });

});
