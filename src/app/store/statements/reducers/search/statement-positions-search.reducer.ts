/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {IAPIPositionSearchStatementModel} from "../../../../core/api/statements";
import {arrayJoin} from "../../../../util/store";
import {setStatementPositionSearchResultAction} from "../../actions";

export const statementPositionsSearchReducer = createReducer<IAPIPositionSearchStatementModel[]>(
    undefined,
    on(setStatementPositionSearchResultAction, (state, action) => {
        return arrayJoin(action.results)
            .filter((statement: any) => statement?.id != null);
    })
);
