/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {updateEntitiesObject} from "../../../../util/store";
import {updateStatementConfigurationAction} from "../../actions";

export const statementConfigurationReducer = createReducer(
    {},
    on(updateStatementConfigurationAction, (state, payload) => {
        return payload == null ? state : updateEntitiesObject(state, [payload.entity], () => payload.statementId);
    })
);
