/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {arrayToEntities, TStoreEntities} from "../../../../util/store";
import {updateStatementEntityAction, updateStatementInfoAction} from "../../actions";
import {IStatementEntity} from "../../model";

export const statementEntitiesReducer = createReducer<TStoreEntities<IStatementEntity>>(
    {},
    on(updateStatementEntityAction, (state, payload) => {
        return payload.statementId == null ? state : {
            ...state,
            [payload.statementId]: {
                ...state[payload.statementId],
                ...payload.entity
            }
        };
    }),
    on(updateStatementInfoAction, (state, payload) => {
        const statementEntitiesArray = (Array.isArray(payload.items) ? payload.items : [])
            .filter((item) => item?.id != null)
            .map((item) => ({...state[item.id], info: item}));

        if (statementEntitiesArray.length === 0) {
            return state;
        }

        return {
            ...state,
            ...arrayToEntities(statementEntitiesArray, (entity) => entity.info.id)
        };
    })
);
