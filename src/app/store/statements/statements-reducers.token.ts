/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {InjectionToken} from "@angular/core";
import {ActionReducerMap} from "@ngrx/store";
import {IStatementsStoreState} from "./model";
import {
    statementConfigurationReducer,
    statementEntitiesReducer,
    statementErrorReducer,
    statementLoadingReducer,
    statementSearchReducer
} from "./reducers";
import {statementPositionsSearchReducer} from "./reducers/search/statement-positions-search.reducer";

export const STATEMENTS_NAME = "statements";

export const STATEMENTS_REDUCER = new InjectionToken<ActionReducerMap<IStatementsStoreState>>("Statements store reducer", {
    providedIn: "root",
    factory: () => ({
        configuration: statementConfigurationReducer,
        entities: statementEntitiesReducer,
        error: statementErrorReducer,
        search: statementSearchReducer,
        positions: statementPositionsSearchReducer,
        loading: statementLoadingReducer
    })
});
