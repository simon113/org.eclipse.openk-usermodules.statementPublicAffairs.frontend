/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPIStatementModel} from "../../../core/api";
import {IStatementConfigurationEntity, IStatementEntity} from "../model";

export const fetchStatementDetailsAction = createAction(
    "[Details/Edit] Fetch statement details",
    props<{ statementId: number; withConfiguration?: boolean }>()
);

export const fetchProcessStateInfo = createAction(
    "[Details] Fetch process history",
    props<{ statementId: number; }>()
);

export const updateStatementEntityAction = createAction(
    "[API] Update statement entity",
    props<{ statementId: number, entity: IStatementEntity }>()
);

export const updateStatementInfoAction = createAction(
    "[API] Update statement info",
    props<{ items: IAPIStatementModel[] }>()
);

export const updateStatementConfigurationAction = createAction(
    "[API] Update statement configuration",
    props<{ statementId: number, entity: IStatementConfigurationEntity }>()
);

export const fetchStatementTextArrangementAction = createAction(
    "[Edit] Fetch statement text arrangement",
    props<{ statementId: number; }>()
);

export const fetchDashboardStatementsAction = createAction(
    "[Dashboard] Fetch dashboard statements"
);
