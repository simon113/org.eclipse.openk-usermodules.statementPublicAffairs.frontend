/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {EAPIProcessTaskDefinitionKey, TCompleteTaskVariable} from "../../../core/api/process";
import {IAPITextArrangementItemModel} from "../../../core/api/text";
import {IAttachmentControlValue} from "../../attachments/model";
import {IStatementEditorFormValue, IStatementInformationFormValue, IWorkflowFormValue} from "../model";

/**
 * This action submits the value of the statement information form to the back end.
 *
 * If new is set to true, a new statement will be created from the given values.
 * If not, statementId and taskId are required to update the values of a statement.
 *
 * If responsible is set to true, the current statement task will be completed. Also,
 * the new created task will be claimed und the page navigates further.
 *
 * If responsible is set to false or unset, the current statement task will not be
 * touched. But, if set to false, the page automatically navigates to the site
 * for creating the draft of a the negative answer.
 */
export const submitStatementInformationFormAction = createAction(
    "[New/Edit] Submit statement information form",
    props<{
        new?: boolean,
        statementId?: number,
        taskId?: string,
        value: IStatementInformationFormValue,
        // If set to true, the addBasicInfoTask will be completed and the addWorkflowData task will be claimed.
        responsible?: boolean
    }>()
);


export const submitWorkflowDataFormAction = createAction(
    "[Edit] Submit workflow data form",
    props<{ statementId: number, taskId: string, data: IWorkflowFormValue, completeTask?: boolean }>()
);


export const validateStatementArrangementAction = createAction(
    "[Edit] Validate statement arrangement",
    props<{ statementId: number, taskId: string, arrangement: IAPITextArrangementItemModel[] }>()
);

export const compileStatementArrangementAction = createAction(
    "[Edit] Compile statement arrangement",
    props<{ statementId: number, taskId: string, arrangement: IAPITextArrangementItemModel[] }>()
);

export const submitStatementEditorFormAction = createAction(
    "[Edit] Submit statement editor form",
    props<{
        statementId: number,
        taskId: string,
        value: IStatementEditorFormValue,
        options?: {
            completeTask?: TCompleteTaskVariable,
            claimNext?: boolean | EAPIProcessTaskDefinitionKey,
            compile?: boolean,
            contribute?: boolean,
            file?: File
        }
    }>()
);

export const submitConsiderationFilesAction = createAction(
    "[Edit] Submit considerations",
    props<{
        statementId: number,
        value: IAttachmentControlValue[]
    }>()
);

export const submitTagsAction = createAction(
    "[Edit] Submit tags",
    props<{
        labels: string[]
    }>()
);

export const sendStatementViaMailAction = createAction(
    "[Details] Resend statement via email",
    props<{ statementId: number, taskId: string, assignee: string }>()
);
