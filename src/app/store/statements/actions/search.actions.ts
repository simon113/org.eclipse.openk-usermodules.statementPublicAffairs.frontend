/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPIPaginationResponse, IAPIPositionSearchStatementModel, IAPISearchOptions, IAPIStatementModel} from "../../../core";
import {IAPIPositionSearchOptions} from "../../../core/api/shared/IAPIPositionSearchOptions";

export const startStatementSearchAction = createAction(
    "[Dashboard/Edit/List] Start search for statements",
    props<{ options: IAPISearchOptions }>()
);

export const setStatementSearchResultAction = createAction(
    "[API] Set search results for statements",
    props<{ results: IAPIPaginationResponse<IAPIStatementModel> }>()
);

export const startStatementPositionSearchAction = createAction(
    "[PositionSearch] Start search for statement positions",
    props<{ options: IAPIPositionSearchOptions }>()
);

export const setStatementPositionSearchResultAction = createAction(
    "[API] Set search results for statement positions",
    props<{ results: IAPIPositionSearchStatementModel[] }>()
);
