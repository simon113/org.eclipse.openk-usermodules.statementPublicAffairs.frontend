/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {EAPIProcessTaskDefinitionKey} from "../../../../core/api/process";
import {TStoreEntities} from "../../../../util/store";
import {IStatementEntity, IStatementEntityWithTasks} from "../../model";
import {
    finishedStatementListSelector,
    getDashboardDivisionMemberStatementsSelector,
    getDashboardOfficialInChargeStatementsSelector,
    getDashboardStatementsToApproveSelector,
    getOtherDashboardStatementsSelector,
    statementEntitiesSortedByDueDateSelector,
    statementListSelector,
    unfinishedStatementListSelector
} from "./statement-list.selectors";

describe("statementsSelectors", () => {

    const entities: TStoreEntities<IStatementEntity> = {
        19: undefined,
        190: {},
        191: {
            info: {} as any
        },
        1919: {
            info: {finished: true} as any
        }
    };

    const all = [entities[191].info, entities[1919].info];
    const finished = [entities[1919].info];
    const unfinished = [entities[191].info];

    const statementEntitiesWithTasks: IStatementEntityWithTasks[] = [
        {
            info: {
                id: 2,
                dueDate: "2020-10-21",
                finished: false
            },
            tasks: [
                {
                    authorized: true,
                    taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_WORK_FLOW_DATA
                }
            ]
        } as IStatementEntityWithTasks,
        {
            info: {
                id: 4,
                dueDate: "2021-10-21",
                finished: false
            },
            tasks: [
                {
                    authorized: true,
                    taskDefinitionKey: EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE
                }
            ]
        } as IStatementEntityWithTasks,
        {
            info: {
                id: 5,
                dueDate: "2022-10-21",
                finished: false
            },
            tasks: [
                {
                    authorized: false,
                    taskDefinitionKey: EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE
                }
            ]
        } as IStatementEntityWithTasks,
        {
            info: {
                id: 3,
                dueDate: "2020-10-22",
                finished: false
            },
            tasks: [
                {
                    authorized: true,
                    taskDefinitionKey: EAPIProcessTaskDefinitionKey.ENRICH_DRAFT
                },
                {
                    authorized: false,
                    taskDefinitionKey: EAPIProcessTaskDefinitionKey.APPROVE_STATEMENT
                }
            ]
        } as IStatementEntityWithTasks,
        {
            info: {
                id: 1,
                dueDate: "2020-10-19",
                finished: false
            },
            tasks: [
                {
                    authorized: false,
                    taskDefinitionKey: EAPIProcessTaskDefinitionKey.ENRICH_DRAFT
                },
                {
                    authorized: true,
                    taskDefinitionKey: EAPIProcessTaskDefinitionKey.APPROVE_STATEMENT
                }
            ]
        } as IStatementEntityWithTasks
    ];

    it("statementListSelector should return a list of all statements", () => {
        expect(statementListSelector.projector({})).toEqual([]);
        expect(statementListSelector.projector(entities)).toEqual([entities[191].info, entities[1919].info]);
    });

    it("finishedStatementListSelector should return a list of all finished statements", () => {
        expect(finishedStatementListSelector.projector([])).toEqual([]);
        expect(finishedStatementListSelector.projector(all)).toEqual(finished);
        expect(finishedStatementListSelector.projector(unfinished)).toEqual([]);
    });

    it("unfinishedStatementListSelector should return a list of all unfinished statements", () => {
        expect(unfinishedStatementListSelector.projector([])).toEqual([]);
        expect(unfinishedStatementListSelector.projector(all)).toEqual(unfinished);
        expect(unfinishedStatementListSelector.projector(finished)).toEqual([]);
    });

    it("statementEntitiesSortedByDueDateSelector should return all statements sorted by dueDate", () => {
        expect(statementEntitiesSortedByDueDateSelector.projector(statementEntitiesWithTasks).map((_) => _.info.id))
            .toEqual([1, 2, 3, 4, 5]);
    });

    it("getDashboardOfficialInChargeStatementsSelector should return all dashboard statements for official in charge", () => {
        expect(getDashboardOfficialInChargeStatementsSelector.projector(statementEntitiesWithTasks).map((_) => _.info.id))
            .toEqual([2, 4]);
    });

    it("getDashboardDivisionMemberStatementsSelector should return all dashboard statements for division member", () => {
        expect(getDashboardDivisionMemberStatementsSelector.projector(statementEntitiesWithTasks).map((_) => _.info.id))
            .toEqual([3]);
    });

    it("getDashboardStatementsToApproveSelector should return all dashboard statements for approver", () => {
        expect(getDashboardStatementsToApproveSelector.projector(statementEntitiesWithTasks).map((_) => _.info.id))
            .toEqual([1]);
    });

    it("getOtherDashboardStatementsSelector should return all dashboard statements that are not included in the other selectors", () => {
        expect(getOtherDashboardStatementsSelector.projector(
            statementEntitiesSortedByDueDateSelector.projector(statementEntitiesWithTasks),
            getDashboardOfficialInChargeStatementsSelector.projector(statementEntitiesWithTasks),
            getDashboardDivisionMemberStatementsSelector.projector(statementEntitiesWithTasks),
            getDashboardStatementsToApproveSelector.projector(statementEntitiesWithTasks)
        ).map((_) => _.info.id)).toEqual([5]);
    });
});
