/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {EAPIProcessTaskDefinitionKey} from "../../../../core/api/process";
import {IAPITextArrangementItemModel, IAPITextBlockGroupModel} from "../../../../core/api/text";
import {arrayJoin, filterDistinctValues, selectArrayProjector, selectPropertyProjector, TStoreEntities} from "../../../../util/store";
import {taskSelector} from "../../../process/selectors";
import {IStatementEditorControlConfiguration} from "../../model";
import {getStatementTextConfigurationSelector} from "../statement-configuration.selectors";
import {statementArrangementSelector} from "../statement.selectors";
import {getStatementErrorSelector} from "../statements-store-state.selectors";

const getTextConfigurationSelectorSelector = createSelector(
    getStatementTextConfigurationSelector,
    selectPropertyProjector("configuration")
);

export const getStatementStaticTextReplacementsSelector = createSelector(
    getStatementTextConfigurationSelector,
    selectPropertyProjector("replacements", {})
);

export const getStatementTextSelectsSelector = createSelector(
    getTextConfigurationSelectorSelector,
    selectPropertyProjector("selects", {})
);

export const getStatementTextBlockGroupsSelector = createSelector(
    getTextConfigurationSelectorSelector,
    selectArrayProjector("groups", [])
);

export const getStatementTextBlockGroupsForNegativeAnswerSelector = createSelector(
    getTextConfigurationSelectorSelector,
    selectArrayProjector("negativeGroups", [])
);

export const getStatementTextBlockGroupsForCurrentTaskSelector = createSelector(
    taskSelector,
    getStatementTextBlockGroupsSelector,
    getStatementTextBlockGroupsForNegativeAnswerSelector,
    (task, groups, negativeGroups): IAPITextBlockGroupModel[] => {
        return task?.taskDefinitionKey === EAPIProcessTaskDefinitionKey.CREATE_NEGATIVE_RESPONSE ? negativeGroups : groups;
    }
);

export const getStatementArrangementForCurrentTaskSelector = createSelector(
    statementArrangementSelector,
    getStatementTextBlockGroupsForCurrentTaskSelector,
    (arrangement, groups) => {
        const availableIds = filterDistinctValues(
            arrayJoin(...groups.map((group) => group.textBlocks.map((textBlock) => textBlock.id)))
        );
        const usedIds = filterDistinctValues(arrangement.map((item) => item.textblockId));
        const isSomeTextBlockNotAvailable = usedIds.some((textBlockId) => availableIds.indexOf(textBlockId) === -1);
        return isSomeTextBlockNotAvailable ? [] : arrangement;
    }
);

export const getStatementArrangementErrorSelector = createSelector(
    getStatementErrorSelector,
    selectArrayProjector("arrangement", [])
);

export const getStatementEditorControlConfigurationSelector = createSelector(
    getStatementTextBlockGroupsForCurrentTaskSelector,
    getStatementStaticTextReplacementsSelector,
    getStatementTextSelectsSelector,
    (
        textBlockGroups: IAPITextBlockGroupModel[],
        replacements: TStoreEntities<string>,
        selects: TStoreEntities<string[]>,
        props: IAPITextArrangementItemModel[]
    ): IStatementEditorControlConfiguration[] => {
        const textBlocks = arrayJoin(...textBlockGroups.map(selectPropertyProjector("textBlocks")));
        return arrayJoin(props)
            .map<IStatementEditorControlConfiguration>((item) => {
                const textBlock = textBlocks.find((_) => _.id === item?.textblockId);
                return {
                    textBlock,
                    selects,
                    replacements,
                    value: item
                };
            });
    }
);
