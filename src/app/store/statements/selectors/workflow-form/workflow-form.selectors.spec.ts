/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createWorkflowDataMock} from "../../../../test";
import {IDepartmentOptionValue} from "../../model";
import {
    departmentGroupsObjectSelector,
    departmentGroupsSelector,
    departmentOptionsSelector,
    selectedDepartmentGroupsObjectSelector,
    selectedDepartmentSelector,
    workflowFormValueSelector
} from "./workflow-form.selectors";

describe("statementSelectors", () => {

    function getValues(size, groupName): IDepartmentOptionValue[] {
        return Array(size).fill("Department ")
            .map((_, index) => _ + index).sort()
            .map((name) => ({name, groupName}));
    }

    it("departmentGroupsObjectSelector", () => {
        const allDepartments = {} as any;
        const selectedDepartments = {} as any;
        expect(departmentGroupsObjectSelector.projector(null, null)).not.toBeDefined();
        expect(departmentGroupsObjectSelector.projector(null, {mandatoryDepartments: selectedDepartments})).toEqual(selectedDepartments);
        expect(departmentGroupsObjectSelector.projector({}, {mandatoryDepartments: selectedDepartments})).toEqual(selectedDepartments);
        expect(departmentGroupsObjectSelector.projector(
            {allDepartments}, {mandatoryDepartments: selectedDepartments})).toEqual(allDepartments);
    });

    it("selectedDepartmentGroupsObjectSelector", () => {
        const suggestedDepartments = {} as any;
        const selectedDepartments = {} as any;
        expect(selectedDepartmentGroupsObjectSelector.projector(null, null)).not.toBeDefined();
        expect(selectedDepartmentGroupsObjectSelector.projector({suggestedDepartments}, null)).toEqual(suggestedDepartments);
        expect(selectedDepartmentGroupsObjectSelector.projector({suggestedDepartments}, {})).toEqual(suggestedDepartments);
        expect(selectedDepartmentGroupsObjectSelector.projector({suggestedDepartments}, {selectedDepartments}))
            .toEqual(selectedDepartments);
    });

    it("departmentGroupsSelector ", () => {
        const groups = {
            GroupB: getValues(1, "GroupB").map((_) => _.name),
            GroupA: getValues(9, "GroupA").map((_) => _.name)
        };
        const result = [
            {
                label: "GroupA",
                options: getValues(9, "GroupA")
            },
            {
                label: "GroupB",
                options: getValues(1, "GroupB")
            }
        ];
        expect(departmentGroupsSelector.projector(null)).toEqual([]);
        expect(departmentGroupsSelector.projector(undefined)).toEqual([]);
        expect(departmentGroupsSelector.projector(groups)).toEqual(result);
    });

    it("departmentOptionsSelector ", () => {
        const groups = [
            {
                label: "GroupA",
                options: getValues(1, "GroupA")
            },
            {
                label: "GroupB",
                options: getValues(9, "GroupB")
            }
        ];
        const result = [...getValues(1, "GroupA"), ...getValues(9, "GroupB")]
            .map((value) => ({label: value.name, value}));
        expect(departmentOptionsSelector.projector([])).toEqual([]);
        expect(departmentOptionsSelector.projector(groups)).toEqual(result);
    });

    it("selectedDepartmentSelector", () => {
        const selectedGroupObject = {
            GroupA: ["Department 2"],
            GroupB: ["Department 3", "Department 4", "Department 5"]
        };
        const groups = [
            {
                label: "GroupA",
                options: getValues(3, "GroupA")
            },
            {
                label: "GroupB",
                options: getValues(9, "GroupB")
            }
        ];
        const result = [...getValues(3, "GroupA").slice(2, 3), ...getValues(9, "GroupB").slice(3, 6)];

        expect(selectedDepartmentSelector.projector(null, null)).toEqual([]);
        expect(selectedDepartmentSelector.projector(null, [])).toEqual([]);
        expect(selectedDepartmentSelector.projector({}, null)).toEqual([]);
        expect(selectedDepartmentSelector.projector(selectedGroupObject, groups)).toEqual(result);
    });

    it("workflowFormValueSelector", () => {
        const departments = {
            selected: getValues(19, "Group"),
            indeterminate: getValues(19, "Group")
        };
        const geoPosition = "geoPosition";
        const parentIds = [18, 19];

        let result = workflowFormValueSelector.projector(undefined, undefined, undefined);
        expect(result.departments).toEqual({
            selected: [],
            indeterminate: []
        });
        expect(result.geographicPosition).not.toBeDefined();
        expect(result.parentIds).toEqual([]);

        result = workflowFormValueSelector.projector(undefined, departments.selected, departments.indeterminate, undefined);
        expect(result.departments).toEqual(departments);
        expect(result.geographicPosition).not.toBeDefined();
        expect(result.parentIds).toEqual([]);

        result = workflowFormValueSelector.projector(
            createWorkflowDataMock({geoPosition: undefined}), departments.selected, departments.indeterminate, undefined);
        expect(result.departments).toEqual(departments);
        expect(result.geographicPosition).not.toBeDefined();
        expect(result.parentIds).toEqual([]);

        result = workflowFormValueSelector.projector(
            createWorkflowDataMock({geoPosition}), departments.selected, departments.indeterminate, parentIds);
        expect(result.departments).toEqual(departments);
        expect(result.geographicPosition).toBe(geoPosition);
        expect(result.parentIds).toEqual(parentIds);
    });
});
