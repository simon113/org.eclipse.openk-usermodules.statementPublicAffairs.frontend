/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {IAPIDepartmentGroups} from "../../../../core/api/settings";
import {ISelectOption, ISelectOptionGroup} from "../../../../shared/controls/select";
import {arrayJoin, objectToArray} from "../../../../util/store";
import {IDepartmentOptionValue, IWorkflowFormValue} from "../../model";
import {getStatementDepartmentConfigurationSelector} from "../statement-configuration.selectors";
import {statementContributionsSelector, statementParentIdsSelector, statementWorkflowSelector} from "../statement.selectors";

export const departmentGroupsObjectSelector = createSelector(
    getStatementDepartmentConfigurationSelector,
    statementWorkflowSelector,
    (configuration, workflow) => {
        return configuration?.allDepartments != null ? configuration.allDepartments : workflow?.mandatoryDepartments;
    }
);

export const selectedDepartmentGroupsObjectSelector = createSelector(
    getStatementDepartmentConfigurationSelector,
    statementWorkflowSelector,
    (configuration, workflow) => {
        return workflow?.mandatoryDepartments == null ? configuration?.suggestedDepartments : workflow.mandatoryDepartments;
    }
);

export const optionalDepartmentGroupsObjectSelector = createSelector(
    statementWorkflowSelector,
    (workflow) => {
        return workflow?.optionalDepartments == null ? {} : workflow.optionalDepartments;
    }
);

export const departmentGroupsSelector = createSelector(
    departmentGroupsObjectSelector,
    (departmentGroupsObject): ISelectOptionGroup<IDepartmentOptionValue>[] => {
        return groupsToOptionsObject(departmentGroupsObject);
    }
);

export const departmentOptionsSelector = createSelector(
    departmentGroupsSelector,
    (groups): ISelectOption<IDepartmentOptionValue>[] => {
        const groupOptions = groups.map<IDepartmentOptionValue[]>((group) => group.options);
        return arrayJoin(...groupOptions).map((value) => ({label: value.name, value}));
    }
);

export const selectedDepartmentSelector = createSelector(
    selectedDepartmentGroupsObjectSelector,
    departmentGroupsSelector,
    (selectedGroupObject, groups): IDepartmentOptionValue[] => {
        const groupOptions = groupsToOptions(groups, selectedGroupObject);
        return arrayJoin(...groupOptions);
    }
);

export const optionalDepartmentSelector = createSelector(
    optionalDepartmentGroupsObjectSelector,
    departmentGroupsSelector,
    (selectedGroupObject, groups): IDepartmentOptionValue[] => {
        const groupOptions = groupsToOptions(groups, selectedGroupObject);
        return arrayJoin(...groupOptions);
    }
);

export const getContributionsSelector = createSelector(
    statementContributionsSelector,
    departmentGroupsSelector,
    (contributions, groups) => {
        const groupOptions = groupsToOptions(groups, contributions);
        return {selected: arrayJoin(...groupOptions), indeterminate: []};
    }
);

export const workflowFormValueSelector = createSelector(
    statementWorkflowSelector,
    selectedDepartmentSelector,
    optionalDepartmentSelector,
    statementParentIdsSelector,
    (workflow, selectedDepartments, optionalDepartments, parentIds): IWorkflowFormValue => {
        return {
            departments: {selected: arrayJoin(selectedDepartments), indeterminate: arrayJoin(optionalDepartments)},
            geographicPosition: workflow?.geoPosition,
            parentIds: arrayJoin(parentIds)
        };
    }
);

export const requiredContributionsGroupsSelector = createSelector(
    selectedDepartmentGroupsObjectSelector,
    optionalDepartmentGroupsObjectSelector,
    departmentGroupsSelector,
    (selectedDepartments, optionalDepartments, departmentGroups): ISelectOptionGroup<IDepartmentOptionValue>[] => {
        return departmentGroups.filter((group) => {
            return selectedDepartments[group.label] != null || optionalDepartments[group.label] != null;
        });
    }
);

export const requiredContributionsOptionsSelector = createSelector(
    departmentOptionsSelector,
    selectedDepartmentGroupsObjectSelector,
    optionalDepartmentGroupsObjectSelector,
    (options, selectedGroups, optionalGroups): ISelectOption<IDepartmentOptionValue>[] => {
        return options.filter((option) => {
            return arrayJoin(selectedGroups[option.value.groupName]).some((_) => option.value.name === _)
                || arrayJoin(optionalGroups[option.value.groupName]).some((_) => option.value.name === _);
        });
    }
);

export function groupsToOptionsObject(departmentGroups: IAPIDepartmentGroups) {
    return objectToArray(departmentGroups)
        .filter((obj) => obj.key != null)
        .sort((a, b) => a.key.localeCompare(b.key))
        .map<ISelectOptionGroup>((obj) => {
            return {
                label: obj.key,
                options: [...obj.value].sort().map((name) => ({groupName: obj.key, name}))
            };
        });
}

export function groupsToOptions(groups: ISelectOptionGroup<IDepartmentOptionValue>[], selectedGroupObject: IAPIDepartmentGroups) {
    return (Array.isArray(groups) && selectedGroupObject != null ? groups : [])
        .filter((group) => Array.isArray(selectedGroupObject[group.label]))
        .map<IDepartmentOptionValue[]>((group) => {
            return group.options.filter((option) => {
                return selectedGroupObject[group.label].find((name) => name === option.name) != null;
            });
        });
}
