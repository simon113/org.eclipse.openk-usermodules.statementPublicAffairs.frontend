/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ISettingsStoreState} from "../../settings/model";
import {IStatementConfigurationEntity} from "../model";
import {getStatementSectorsSelector} from "./statement-configuration.selectors";
import {
    statementCommentsSelector,
    statementEntitiesSelector,
    statementInfoSelector,
    statementParentIdsSelector,
    statementSelector,
    statementWorkflowSelector
} from "./statement.selectors";
import {getStatementSearchSelector} from "./statements-store-state.selectors";

describe("statementSelectors", () => {

    function expectNullChecks<T>(
        projector: (arg: T, ...args: any[]) => any,
        key: keyof T,
        result: any = {1: 9},
        resultOnNull?: any
    ) {
        expect(projector(null)).toEqual(resultOnNull);
        expect(projector(undefined)).toEqual(resultOnNull);
        expect(projector({} as any)).toEqual(resultOnNull);
        expect(projector({[key]: result} as any)).toEqual(result);
    }

    it("statementSelector", () => {
        const entities = {19: {}};
        expect(statementSelector.projector(undefined, undefined)).not.toBeDefined();
        expect(statementSelector.projector({}, undefined)).not.toBeDefined();
        expect(statementSelector.projector({}, null)).not.toBeDefined();
        expect(statementSelector.projector({}, 19)).not.toBeDefined();
        expect(statementSelector.projector(entities, 19)).toBe(entities[19]);
    });

    it("statementEntitiesSelector", () => {
        expectNullChecks(statementEntitiesSelector.projector, "entities", {1: 9}, {});
    });

    it("statementInfoSelector", () => {
        expectNullChecks(statementInfoSelector.projector, "info");
    });

    it("statementWorkflowSelector", () => {
        expectNullChecks(statementWorkflowSelector.projector, "workflow");
    });

    it("statementParentIdsSelector", () => {
        expectNullChecks(statementParentIdsSelector.projector, "parentIds", [1, 2, 3], []);
    });

    it("statementCommentsSelector", () => {
        expectNullChecks(statementCommentsSelector.projector, "comments", [1, 2, 3], []);
    });


    it("getStatementSearchSelector", () => {
        const search = {} as any;
        expect(getStatementSearchSelector.projector(undefined)).not.toBeDefined();
        expect(getStatementSearchSelector.projector({search})).toBe(search);
    });

    it("getStatementSectorsSelector", () => {
        let settings: Partial<ISettingsStoreState> = {};
        let configuration: Partial<IStatementConfigurationEntity> = {};

        expect(getStatementSectorsSelector.projector(settings, configuration)).not.toBeDefined();
        expect(getStatementSectorsSelector.projector(null, null)).not.toBeDefined();
        settings = {
            sectors: {
                "Ort#Ortsteil": ["Strom", "Gas", "Beleuchtung"]
            }
        };
        expect(getStatementSectorsSelector.projector(settings, null)).toBe(settings.sectors);
        expect(getStatementSectorsSelector.projector(settings, configuration)).toBe(settings.sectors);
        configuration = {
            sectors: {
                "Stadt#Stadtteil": ["Strom", "Gas"]
            }
        };
        expect(getStatementSectorsSelector.projector(null, configuration)).toBe(configuration.sectors);
        expect(getStatementSectorsSelector.projector(settings, configuration)).toBe(configuration.sectors);
    });

});
