/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {merge, Observable, of, Subscription} from "rxjs";
import {IAPICommentModel, SPA_BACKEND_ROUTE} from "../../../../core";
import {addCommentAction, deleteCommentAction, fetchCommentsAction, updateStatementEntityAction} from "../../actions";
import {CommentsEffect} from "./comments.effect";

describe("CommentsEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: CommentsEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                CommentsEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(CommentsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch comments", () => {
        const statementId = 19;
        const comments: IAPICommentModel[] = [getCommentObject(190)];
        const expectedResult = updateStatementEntityAction({statementId, entity: {comments}});
        const results: Action[] = [];

        actions$ = of(fetchCommentsAction({statementId}));
        subscription = effect.fetch$.subscribe((action) => results.push(action));

        expectFetchRequest(statementId, comments);
        expect(results).toEqual([expectedResult]);
        httpTestingController.verify();
    });

    it("should delete comments", () => {
        const statementId = 19;
        const commentId = 1919;
        const comments: IAPICommentModel[] = [getCommentObject(190)];
        const expectedResult = updateStatementEntityAction({statementId, entity: {comments}});
        const results: Action[] = [];

        actions$ = of(deleteCommentAction({statementId, commentId}));
        subscription = effect.delete$.subscribe((action) => results.push(action));

        expectDeleteRequest(statementId, commentId);
        expectFetchRequest(statementId, comments);

        expect(results).toEqual([expectedResult]);
        httpTestingController.verify();
    });

    it("should add comments", () => {
        const statementId = 19;
        const text = "1919";
        const comments: IAPICommentModel[] = [getCommentObject(190)];
        const expectedResult = updateStatementEntityAction({statementId, entity: {comments}});
        const results: Action[] = [];

        actions$ = of(addCommentAction({statementId, text}));
        subscription = effect.add$.subscribe((action) => results.push(action));

        expectAddRequest(statementId, text);
        expectFetchRequest(statementId, comments);

        expect(results).toEqual([expectedResult]);
        httpTestingController.verify();
    });

    it("should not make API calls if action data is missing", () => {
        actions$ = of(...[
            fetchCommentsAction({statementId: null}),
            addCommentAction({statementId: null, text: "19"}),
            deleteCommentAction({statementId: null, commentId: 19}),
            deleteCommentAction({statementId: 19, commentId: null})
        ]);
        subscription = merge(effect.fetch$, effect.add$, effect.delete$).subscribe();
        expect(() => httpTestingController.verify()).not.toThrow();
    });

    function getCommentObject(id: number, editable: boolean = false): IAPICommentModel {
        return {
            id, userName: "hugo", firstName: "Hugo", lastName: "Haferkamp", editable,
            text: "CommentText", timestamp: ""
        };
    }

    function expectFetchRequest(statementId: number, returnValue: IAPICommentModel[]) {
        const url = `/process/statements/${statementId}/comments`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

    function expectAddRequest(statementId: number, expectedBody: string) {
        const url = `/process/statements/${statementId}/comments`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("PUT");
        expect(request.request.body).toBe(expectedBody);
        request.flush(null);
    }

    function expectDeleteRequest(statementId: number, commentId: number) {
        const url = `/process/statements/${statementId}/comments/${commentId}`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("DELETE");
        request.flush(204);
    }

});

