/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpParams} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {fakeAsync, TestBed, tick} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {concat, NEVER, Observable, of, Subscription, timer} from "rxjs";
import {map} from "rxjs/operators";
import {IAPIPaginationResponse, IAPISearchOptions, IAPIStatementModel, SPA_BACKEND_ROUTE} from "../../../../core";
import {createPaginationResponseMock, createStatementModelMock} from "../../../../test";
import {objectToHttpParams} from "../../../../util/http";
import {
    setStatementLoadingAction,
    setStatementSearchResultAction,
    startStatementSearchAction,
    updateStatementInfoAction
} from "../../actions";
import {SearchStatementsEffect} from "./search-statements.effect";

describe("SearchStatementsEffect", () => {

    const DEBOUNCE_TIME = 200;

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: SearchStatementsEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                SearchStatementsEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(SearchStatementsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch statements after a debounce time", fakeAsync(() => {
        const options: IAPISearchOptions = {q: "Darmstadt Heppenheim"};
        const results: Action[] = [];
        const content: IAPIStatementModel[] = Array(19).fill(0)
            .map((_, id) => createStatementModelMock(id));

        const expectedResults: Action[] = [
            setStatementLoadingAction({loading: {search: true}}),
            updateStatementInfoAction({items: content}),
            setStatementSearchResultAction({results: createPaginationResponseMock(content)}),
            setStatementLoadingAction({loading: {search: false}})
        ];

        actions$ = concat(
            of(startStatementSearchAction({options})),
            timer(DEBOUNCE_TIME - 2).pipe(map(() => startStatementSearchAction({options}))),
            // For testing the debounce time, the observable shall never end; otherwise, it will emit its last value
            NEVER
        );
        subscription = effect.search$.subscribe((action) => results.push(action));

        tick(DEBOUNCE_TIME - 1);
        expect(results).toEqual([]);
        tick(DEBOUNCE_TIME);
        expectFetchRequest(options, createPaginationResponseMock(content));
        expect(results).toEqual(expectedResults);

        httpTestingController.verify();
    }));

    it("should only update store when content is provided", fakeAsync(() => {
        const options: IAPISearchOptions = {q: "Darmstadt Heppenheim"};
        const results: Action[] = [];

        const expectedResults: Action[] = [
            setStatementLoadingAction({loading: {search: true}}),
            setStatementLoadingAction({loading: {search: false}})
        ];

        actions$ = of(startStatementSearchAction({options}));
        subscription = effect.search$.subscribe((action) => results.push(action));

        expectFetchRequest(options, null);
        expect(results).toEqual(expectedResults);

        httpTestingController.verify();
    }));

    function expectFetchRequest(options: IAPISearchOptions, returnValue: IAPIPaginationResponse<IAPIStatementModel>) {
        const params = new HttpParams({
            fromObject: objectToHttpParams({
                q: options.q,
                size: options.size == null ? undefined : "" + options.size
            })
        });
        const url = `/statementsearch` + (params.keys().length > 0 ? `?${params.toString()}` : "");
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

});

