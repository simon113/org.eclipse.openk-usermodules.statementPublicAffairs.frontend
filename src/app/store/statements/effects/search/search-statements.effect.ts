/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable, of} from "rxjs";
import {concatMap, debounceTime, endWith, filter, startWith, switchMap} from "rxjs/operators";
import {IAPISearchOptions, StatementsApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util/rxjs";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {
    setStatementLoadingAction,
    setStatementSearchResultAction,
    startStatementSearchAction,
    updateStatementInfoAction
} from "../../actions";

@Injectable({providedIn: "root"})
export class SearchStatementsEffect {

    public search$ = createEffect(() => this.actions.pipe(
        ofType(startStatementSearchAction),
        debounceTime(200),
        concatMap((action) => this.search(action.options))
    ));

    public constructor(
        private readonly actions: Actions,
        private readonly statementsApiService: StatementsApiService
    ) {

    }

    public search(options: IAPISearchOptions): Observable<Action> {
        return this.statementsApiService.getStatementSearch(options).pipe(
            filter((results) => Array.isArray(results?.content)),
            switchMap((results) => {
                return of(
                    updateStatementInfoAction({items: results.content}),
                    setStatementSearchResultAction({results})
                );
            }),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setStatementLoadingAction({loading: {search: true}})),
            endWith(setStatementLoadingAction({loading: {search: false}}))
        );
    }

}
