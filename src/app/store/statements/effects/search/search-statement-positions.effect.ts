/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable, of} from "rxjs";
import {concatMap, debounceTime, endWith, filter, startWith, switchMap} from "rxjs/operators";
import {StatementsApiService} from "../../../../core";
import {IAPIPositionSearchOptions} from "../../../../core/api/shared/IAPIPositionSearchOptions";
import {catchErrorTo} from "../../../../util/rxjs";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {setStatementLoadingAction, setStatementPositionSearchResultAction, startStatementPositionSearchAction} from "../../actions";

@Injectable({providedIn: "root"})
export class SearchStatementPositionsEffect {

    public search$ = createEffect(() => this.actions.pipe(
        ofType(startStatementPositionSearchAction),
        debounceTime(200),
        concatMap((action) => this.search(action.options))
    ));

    public constructor(
        private readonly actions: Actions,
        private readonly statementsApiService: StatementsApiService
    ) {

    }

    public search(options: IAPIPositionSearchOptions): Observable<Action> {
        return this.statementsApiService.getStatementPositionsSearch(options).pipe(
            filter((results) => Array.isArray(results)),
            switchMap((results) => {
                return of(
                    setStatementPositionSearchResultAction({results})
                );
            }),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setStatementLoadingAction({loading: {positionSearch: true}})),
            endWith(setStatementLoadingAction({loading: {positionSearch: false}}))
        );
    }

}
