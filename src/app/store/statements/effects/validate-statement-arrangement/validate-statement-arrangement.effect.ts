/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable} from "rxjs";
import {endWith, filter, map, startWith, switchMap} from "rxjs/operators";
import {IAPITextArrangementItemModel, TextApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util/rxjs";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {setStatementErrorAction, setStatementLoadingAction, validateStatementArrangementAction} from "../../actions";

@Injectable({providedIn: "root"})
export class ValidateStatementArrangementEffect {

    public validate$ = createEffect(() => this.actions.pipe(
        ofType(validateStatementArrangementAction),
        filter((action) => action.statementId != null && action.taskId != null),
        switchMap((action) => this.validate(action.statementId, action.taskId, action.arrangement))
    ));

    public constructor(private readonly actions: Actions, private readonly textApiService: TextApiService) {

    }

    public validate(statementId: number, taskId: string, arrangement: IAPITextArrangementItemModel[]): Observable<Action> {
        return this.textApiService.validateArrangement(statementId, taskId, arrangement).pipe(
            map((result) => {
                return setStatementErrorAction({
                    statementId,
                    error: result.valid ?
                        {arrangement: null, errorMessage: null} :
                        {arrangement: result.errors, errorMessage: EErrorCode.INVALID_TEXT_ARRANGEMENT}
                });
            }),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setStatementLoadingAction({loading: {submittingStatementEditorForm: true}})),
            endWith(setStatementLoadingAction({loading: {submittingStatementEditorForm: false}}))
        );
    }

}
