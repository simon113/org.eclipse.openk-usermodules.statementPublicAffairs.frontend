/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {concat, EMPTY, Observable} from "rxjs";
import {endWith, exhaustMap, filter, map, retry, startWith} from "rxjs/operators";
import {IAPIWorkflowData, StatementsApiService} from "../../../../core";
import {catchErrorTo, emitOnComplete, ignoreError, throwAfterActionType} from "../../../../util/rxjs";
import {ProcessTaskEffect} from "../../../process/effects";
import {EErrorCode, setErrorAction} from "../../../root";
import {setStatementLoadingAction, submitWorkflowDataFormAction, updateStatementEntityAction} from "../../actions";
import {IWorkflowFormValue} from "../../model";
import {reduceDepartmentOptionsToGroups} from "../../util";

@Injectable({providedIn: "root"})
export class SubmitWorkflowFormEffect {

    public readonly submit$ = createEffect(() => this.actions.pipe(
        ofType(submitWorkflowDataFormAction),
        filter((action) => action.statementId != null && action.taskId != null && action.data != null),
        exhaustMap((action) => this.submitWorkflowForm(action.statementId, action.taskId, action.data, action.completeTask))
    ));


    public constructor(
        private readonly actions: Actions,
        private readonly statementsApiService: StatementsApiService,
        private readonly taskEffect: ProcessTaskEffect
    ) {

    }

    private submitWorkflowForm(
        statementId: number,
        taskId: string,
        data: IWorkflowFormValue,
        completeTask?: boolean
    ): Observable<Action> {
        return concat(
            concat(
                this.postWorkflowData(statementId, taskId, data),
                this.postParentIds(statementId, taskId, data)
            ).pipe(emitOnComplete()),
            completeTask ? this.taskEffect.completeTask(statementId, taskId, {}, true) : EMPTY
        ).pipe(
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            throwAfterActionType(setErrorAction),
            ignoreError(),
            startWith(setStatementLoadingAction({loading: {submittingWorkflowData: true}})),
            endWith(setStatementLoadingAction({loading: {submittingWorkflowData: false}}))
        );
    }

    private postWorkflowData(
        statementId: number,
        taskId: string,
        data: IWorkflowFormValue
    ): Observable<Action> {
        const body: IAPIWorkflowData = {
            mandatoryDepartments: reduceDepartmentOptionsToGroups(data.departments.selected),
            optionalDepartments: reduceDepartmentOptionsToGroups(data.departments.indeterminate),
            geoPosition: data.geographicPosition
        };
        return this.statementsApiService.postWorkflowData(statementId, taskId, body).pipe(
            map(() => updateStatementEntityAction({statementId, entity: {workflow: body}})),
            retry(3)
        );
    }

    private postParentIds(
        statementId: number,
        taskId: string,
        data: IWorkflowFormValue
    ): Observable<Action> {
        const parentIds = data.parentIds;
        return this.statementsApiService.postParentIds(statementId, taskId, parentIds).pipe(
            map(() => updateStatementEntityAction({statementId, entity: {parentIds}})),
            retry(3)
        );
    }

}
