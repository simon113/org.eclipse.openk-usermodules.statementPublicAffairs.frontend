/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FormControl, Validators} from "@angular/forms";
import {IAPIPartialStatementModel} from "../../../../core/api/statements";
import {createFormGroup} from "../../../../util/forms";
import {createAttachmentForm, IAttachmentFormValue} from "../../../attachments/model";

export interface IStatementInformationFormValue extends IAPIPartialStatementModel {

    attachments?: IAttachmentFormValue;

}

export function createStatementInformationForm() {
    return createFormGroup<IStatementInformationFormValue>({
        title: new FormControl(undefined, [Validators.required]),
        creationDate: new FormControl(undefined, [Validators.required]),
        dueDate: new FormControl(undefined, [Validators.required]),
        receiptDate: new FormControl(undefined, [Validators.required]),
        typeId: new FormControl(undefined, [Validators.required]),
        city: new FormControl(undefined, [Validators.required]),
        district: new FormControl(undefined, [Validators.required]),
        contactId: new FormControl(undefined, [Validators.required]),
        sourceMailId: new FormControl(null),
        customerReference: new FormControl(),
        attachments: createAttachmentForm()
    });
}
