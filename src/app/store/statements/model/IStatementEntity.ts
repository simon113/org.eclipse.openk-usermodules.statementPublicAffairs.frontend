/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIContactPersonDetails} from "../../../core/api/contacts/IAPIContactPersonDetails";
import {IAPIProcessTask} from "../../../core/api/process";
import {IAPIDepartmentGroups} from "../../../core/api/settings";
import {IAPICommentModel, IAPIStatementModel, IAPIWorkflowData} from "../../../core/api/statements";
import {IAPITextArrangementItemModel} from "../../../core/api/text";

export interface IStatementEntity {

    info?: IAPIStatementModel;

    workflow?: IAPIWorkflowData;

    contributions?: IAPIDepartmentGroups;

    parentIds?: number[];

    childrenIds?: number[];

    comments?: IAPICommentModel[];

    arrangement?: IAPITextArrangementItemModel[];

    file?: File;

    editedByMe?: boolean;

    mandatoryDepartmentsCount?: number;

    mandatoryContributionsCount?: number;

    optionalForMyDepartment?: boolean;

    completedForMyDepartment?: boolean;

    contactInfo?: IAPIContactPersonDetails;

}

export interface IStatementEntityWithTasks extends IStatementEntity {
    tasks?: IAPIProcessTask[];
}
