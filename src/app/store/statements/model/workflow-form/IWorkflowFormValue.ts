/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FormControl} from "@angular/forms";
import {createFormGroup} from "../../../../util/forms";
import {IDepartmentOptionValue} from "./IDepartmentOptionValue";

export interface IWorkflowFormValue {

    departments: {
        selected: IDepartmentOptionValue[],
        indeterminate: IDepartmentOptionValue[]
    };

    geographicPosition: string;

    parentIds: number[];

}

export function createWorkflowForm() {
    return createFormGroup<IWorkflowFormValue>({
        departments: new FormControl([]),
        geographicPosition: new FormControl(""),
        parentIds: new FormControl([])
    });
}
