/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable, of} from "rxjs";
import {endWith, startWith, switchMap, throttleTime} from "rxjs/operators";
import {GeoApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util/rxjs";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {searchMapAction, setMapSearchLoadingAction, setSearchResponseAction} from "../../actions";

@Injectable({providedIn: "root"})
export class NominatimSearchEffect {

    public search$ = createEffect(() => this.actions.pipe(
        ofType(searchMapAction),
        throttleTime(200),
        switchMap((action) => this.search(action.q))
    ));

    public constructor(
        public actions: Actions,
        public geoApiService: GeoApiService
    ) {

    }

    public search(q: string): Observable<Action> {
        return this.geoApiService.search(q).pipe(
            switchMap((response) => {
                return response.length > 0 ?
                    of(setSearchResponseAction({response})) :
                    of(setSearchResponseAction({response}), setErrorAction({error: EErrorCode.SEARCH_NO_RESULT}));
            }),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setMapSearchLoadingAction({loading: true})),
            endWith(setMapSearchLoadingAction({loading: false}))
        );
    }

}
