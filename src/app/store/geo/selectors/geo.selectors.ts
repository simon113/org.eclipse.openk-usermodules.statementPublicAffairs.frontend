/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createFeatureSelector, createSelector} from "@ngrx/store";
import {arrayJoin, selectArrayProjector, selectPropertyProjector} from "../../../util/store";
import {GEO_NAME} from "../geo-reducers.token";
import {IGeoStoreState} from "../model";

export const geoStateSelector = createFeatureSelector<IGeoStoreState>(GEO_NAME);

export const getNominatimResponseContentSelector = createSelector(
    geoStateSelector,
    selectArrayProjector("responseContent", [])
);

export const getNominatimSearchResultSelector = createSelector(
    getNominatimResponseContentSelector,
    (result) => {
        return arrayJoin(result).length === 0 ? undefined : {lat: parseFloat(result[0].lat), lng: parseFloat(result[0].lon)};
    }
);

export const getNominatimLoadingSelector = createSelector(
    geoStateSelector,
    selectPropertyProjector("loading")
);
