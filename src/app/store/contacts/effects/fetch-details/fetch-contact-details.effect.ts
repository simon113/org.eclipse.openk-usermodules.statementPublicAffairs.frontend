/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable} from "rxjs";
import {endWith, filter, map, retry, startWith, switchMap} from "rxjs/operators";
import {ContactsApiService} from "../../../../core/api/contacts";
import {catchHttpErrorTo, EHttpStatusCodes} from "../../../../util/http";
import {catchErrorTo} from "../../../../util/rxjs";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {fetchContactDetailsAction, setContactEntityAction, setContactsLoadingState} from "../../actions";

@Injectable({providedIn: "root"})
export class FetchContactDetailsEffect {

    public fetch$ = createEffect(() => this.actions.pipe(
        ofType(fetchContactDetailsAction),
        filter((action) => action.contactId != null),
        switchMap((action) => this.fetch(action.contactId, action.statementId))
    ));

    public constructor(public actions: Actions, public contactsApiService: ContactsApiService) {

    }

    public fetch(contactId: string, statementId?: number | "new"): Observable<Action> {
        return this.contactsApiService.getContactDetails(contactId).pipe(
            map((result) => {
                return setContactEntityAction({
                    entity: {
                        id: contactId,
                        ...result,
                    }
                });
            }),
            catchHttpErrorTo(
                setErrorAction({statementId, error: EErrorCode.CONTACT_MODULE_NO_ACCESS}),
                EHttpStatusCodes.FORBIDDEN
            ),
            retry(2),
            catchErrorTo(setErrorAction({statementId, error: EErrorCode.FAILED_LOADING_CONTACT})),
            startWith(setContactsLoadingState({state: {fetching: true}})),
            endWith(setContactsLoadingState({state: {fetching: false}}))
        );
    }


}
