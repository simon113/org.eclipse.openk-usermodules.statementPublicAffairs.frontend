/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {TStoreEntities, updateEntitiesObject} from "../../../../util";
import {setContactEntityAction, setContactsSearchAction} from "../../actions";
import {IContactEntity} from "../../model";

export const contactEntitiesReducer = createReducer<TStoreEntities<IContactEntity>>(
    {},
    on(setContactEntityAction, (state, payload) => {
        return updateEntitiesObject(state, [payload.entity], (item) => item.id);
    }),
    on(setContactsSearchAction, (state, payload) => {
        return updateEntitiesObject(state, payload.results?.content, (item) => item.id);
    })
);
