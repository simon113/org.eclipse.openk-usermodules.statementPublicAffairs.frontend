/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createFeatureSelector, createSelector} from "@ngrx/store";
import {arrayJoin} from "../../../util";
import {CONTACTS_NAME} from "../contacts-reducers.token";
import {IContactEntity, IContactsStoreState} from "../model";

export const contactsStateSelector = createFeatureSelector<IContactsStoreState>(CONTACTS_NAME);

export const contactEntitiesSelector = createSelector(
    contactsStateSelector,
    (state) => ({...state?.entities})
);

export const getContactSearchSelector = createSelector(
    contactsStateSelector,
    (state) => state?.search
);

export const getContactSearchContentSelector = createSelector(
    contactEntitiesSelector,
    getContactSearchSelector,
    (entities, search) => {
        return arrayJoin(search?.content).map((id) => entities[id]);
    }
);

export const getContactDetailsSelector = createSelector(
    contactEntitiesSelector,
    (entities, props: { id: string }): IContactEntity => entities[props.id]
);

export const getContactLoadingSelector = createSelector(
    contactsStateSelector,
    (state) => state?.loading
);
