/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPITextBlockModel} from "../../../../core/api/text";
import {textToBlockDataArray} from "./block-data-helper";

@Pipe({
    name: "getBlockDataFromBlockModel"
})
export class GetBlockDataFromBlockModelPipe implements PipeTransform {

    public transform(blockModel: IAPITextBlockModel, replacements?: { [key: string]: string }):
        Array<{ value: string, type: string, placeholder?: string, options?: string[] }> {

        if (blockModel == null) {
            return [];
        }

        blockModel = {
            ...blockModel,
            text: blockModel.text?.endsWith("\n") ? blockModel.text + " " : blockModel.text
        };

        return textToBlockDataArray(blockModel, undefined, replacements, undefined, false);
    }
}
