/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {timer} from "rxjs";
import {DateControlComponent} from "../../../controls/date-control";
import {SelectComponent} from "../../../controls/select/components/select";

@Component({
    selector: "app-text-replacement",
    templateUrl: "./text-replacement.component.html",
    styleUrls: ["./text-replacement.component.scss"]
})
export class TextReplacementComponent {

    @Input()
    public appEditable = false;

    @Input()
    public appType: string;

    @Input()
    public appValue: string;

    @Input()
    public appPlaceholder: string;

    @Input()
    public appOptions: string[] = [];

    @Input()
    public appDisabled: boolean;

    @Output()
    public appEdit = new EventEmitter<boolean>();

    @ViewChild("inputElement") inputElement: ElementRef;

    @ViewChild("dateElement") dateElement: DateControlComponent;

    @ViewChild("selectElement") selectElement: SelectComponent;

    @ViewChild("editBox") editBox: ElementRef;

    @Output()
    public appValueChange = new EventEmitter<string>();

    public async onClick() {
        this.appEditable = true;
        this.appEdit.emit(this.appEditable);
        this.resizeInput();
        switch (this.appType) {
            case "input":
                await timer(0).toPromise();
                this.inputElement.nativeElement.focus();
                break;
            case "date":
                await timer(0).toPromise();
                this.dateElement.toggle(true);
                this.dateElement.inputElement.nativeElement.focus();
                break;
            case "select":
                await timer(0).toPromise();
                this.selectElement.toggle();
                break;
        }
    }

    public onFocusOut() {
        this.appEditable = false;
        this.appEdit.emit(this.appEditable);
    }

    public resizeInput() {
        if (this.appType === "input" && this.inputElement) {
            this.inputElement.nativeElement.style.width = "1px";
            this.inputElement.nativeElement.style.width = this.inputElement.nativeElement.scrollWidth + "px";
        }
    }

    public inputValue(value: string) {
        this.resizeInput();
        this.appValueChange.emit(value);
    }
}
