/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkDragDrop, CdkDropList} from "@angular/cdk/drag-drop";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {I18nModule} from "../../../../core/i18n";
import {IExtendedTextBlockModel} from "../../model";
import {TextBlockModule} from "../../text-block.module";
import {TextBlockSelectComponent} from "./text-block-select.component";

describe("TextBlockSelectComponent", () => {
    let component: TextBlockSelectComponent;
    let fixture: ComponentFixture<TextBlockSelectComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                TextBlockModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextBlockSelectComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(component).toBeTruthy();
    });

    it("should emit the index on drop when drop destination is a different container", () => {
        const container: CdkDropList<IExtendedTextBlockModel> = {} as CdkDropList<IExtendedTextBlockModel>;
        const dragEvent: CdkDragDrop<IExtendedTextBlockModel> = {
            previousContainer: container,
            container,
            prevousIndex: 0
        } as unknown as CdkDragDrop<IExtendedTextBlockModel>;

        spyOn(component.appRemove, "emit").and.callThrough();
        component.drop(dragEvent);
        expect(component.appRemove.emit).not.toHaveBeenCalled();
        component.drop({...dragEvent, container: {data: []} as unknown as CdkDropList<IExtendedTextBlockModel>});
        expect(component.appRemove.emit).toHaveBeenCalledWith(dragEvent.previousIndex);
    });
});

