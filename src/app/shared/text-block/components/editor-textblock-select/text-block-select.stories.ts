/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {boolean, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {IAPITextBlockGroupModel, IAPITextBlockModel} from "../../../../core/api/text";
import {I18nModule} from "../../../../core/i18n";
import {TextBlockModule} from "../../text-block.module";

const textBlock: IAPITextBlockModel = {
    id: "Textbaustein 1.1",
    text: "Ich bin ein Test Block Inhalt mit einem Select <s:irgendeinnamen> und einem Freitextfeld <f:text>",
    excludes: [],
    requires: []
};

const textBlocks: IAPITextBlockModel[] = [
    {...textBlock, id: "Textbaustein 1.1"},
    {...textBlock, id: "Textbaustein 1.2"},
    {...textBlock, id: "Textbaustein 2.1"},
    {...textBlock, id: "Textbaustein 3.4"}
];

const groups: IAPITextBlockGroupModel[] = [
    {
        groupName: "Textblockgruppe 1",
        textBlocks: textBlocks.map((_) => ({..._, id: _.id + (Math.random())}))
    },
    {
        groupName: "Textblockgruppe 2",
        textBlocks: textBlocks.map((_) => ({..._, id: _.id + (Math.random())}))
    }
];

storiesOf("Shared", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            TextBlockModule,
            I18nModule
        ]
    }))
    .add("TextBlockSelectComponent", () => ({
        template: `
            <div style="padding: 1em; overflow: auto; width: 50%; background: #ffffff;">
                      <app-text-block-select
                        [appGroups]="groups"
                        [appShortMode]="shortMode">
                        </app-text-block-select>
            </div>
        `,
        props: {
            groups,
            shortMode: boolean("short mode", false)
        }
    }));
