/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {objectToArray} from "../../../util/store";


@Pipe({name: "objToArray"})
export class ObjToArrayPipe implements PipeTransform {

    /**
     * Converts an object to an array containing its key value pairs.
     */
    public transform<T extends object>(value: T, keepNullOrUndefined: boolean = false) {
        return objectToArray<T>({...value}, keepNullOrUndefined)
            .sort((a, b) => {
                return a.key.localeCompare(b.key);
            });
    }

}
