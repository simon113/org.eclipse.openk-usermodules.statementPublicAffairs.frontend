/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ObjKeysToArrayPipe} from "./obj-keys-to-array.pipe";

describe("ObjToArrayPipe", () => {

    const pipe = new ObjKeysToArrayPipe();

    it("should convert an object to an array of its keys", () => {
        const testObject = {
            prop1: "propValue1",
            prop2: undefined,
            prop3: null
        };
        expect(pipe.transform(testObject)).toEqual(["prop1"]);
        expect(pipe.transform(testObject, true)).toEqual(["prop1", "prop2", "prop3"]);
        expect(pipe.transform(undefined)).toEqual([]);
        expect(pipe.transform(null)).toEqual([]);
    });

});
