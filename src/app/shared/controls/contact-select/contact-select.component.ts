/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {IAPIContactPerson} from "../../../core/api/contacts/IAPIContactPerson";
import {IAPIContactPersonDetails} from "../../../core/api/contacts/IAPIContactPersonDetails";
import {AbstractControlValueAccessorComponent} from "../common";

/**
 * This component displays a paginated list of contacts.
 * Contacts from the list can be selected by clicking on the row. On selection the detailed information for the selected contact, if it
 * exists, is displayed right next to the table of contacts. The value of this component is the id of the selected contact.
 * There is also a searchbar on top of the table to be able to perform a filtering search on the list of statements. The search bar input
 * text is emitted to the parent component and the filtering of the actual array of statements needs to be done in that parent component.
 */
@Component({
    selector: "app-contact-select",
    templateUrl: "./contact-select.component.html",
    styleUrls: ["./contact-select.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ContactSelectComponent),
            multi: true
        }
    ]
})
export class ContactSelectComponent extends AbstractControlValueAccessorComponent<string> {

    @Input()
    public appEntries: IAPIContactPerson[];

    @Input()
    public appDetails: IAPIContactPersonDetails;

    @Input()
    public appIsLoading: boolean;

    @Input()
    public appPage: number;

    @Input()
    public appMessage: string;

    @Output()
    public appPageChange = new EventEmitter<{ page: number, size: number }>();

    @Input()
    public appTotalPages: number;

    @Input()
    public appSearch: string;

    @Input()
    public appPageSize: number;

    @Output()
    public appSearchChange = new EventEmitter<string>();

    @Output()
    public appOpenContactModule = new EventEmitter<void>();

}
