/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ElementRef, ViewChild} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {CommonControlsModule} from "../../common-controls.module";

@Component({
    selector: `app-host-component`,
    template: `
        <textarea #input appAutoTextFieldResize></textarea>`
})
class TestHostComponent {
    @ViewChild("input") public input: ElementRef;
}

describe("TextFieldDirective", () => {

    let component: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [CommonControlsModule],
            declarations: [TestHostComponent],
            providers: []
        }).compileComponents();
    }));

    beforeEach(async () => {
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        await fixture.whenStable();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should resize the textarea on value change", async () => {
        component.input.nativeElement.style.height = "1px";
        component.input.nativeElement.value =
            "A text that has to be of a certain length so it needs multiple lines to display. This should be enough.";
        fixture.detectChanges();
        await fixture.whenStable();
        expect(component.input.nativeElement.scrollTop).toBe(0);
    });

    it("should resize the textarea on input", async () => {
        component.input.nativeElement.style.height = "1px";
        component.input.nativeElement.value =
            "A text that has to be of a certain length so it needs multiple lines to display. This should be enough.";
        component.input.nativeElement.dispatchEvent(new Event("input"));
        await fixture.whenStable();
        expect(component.input.nativeElement.scrollTop).toBe(0);
    });
});
