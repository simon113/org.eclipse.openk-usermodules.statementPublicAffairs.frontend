/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {EventEmitter, Input, Output} from "@angular/core";
import {ControlValueAccessor} from "@angular/forms";

export abstract class AbstractControlValueAccessorComponent<T> implements ControlValueAccessor {

    @Input()
    public appDisabled: boolean;

    @Input()
    public appValue: T;

    @Output()
    public appValueChange: EventEmitter<T> = new EventEmitter<T>();

    public onChange = (_: T) => null;

    public onTouch = () => null;

    /**
     * Sets a new value to the control.
     * @param obj New value for the control
     * @param emit If true, the new value will be emitted via the appValueChange event emitter.
     */
    public writeValue(obj: T, emit?: boolean) {
        this.appValue = obj;
        if (emit) {
            this.appValueChange.emit(this.appValue);
            this.onChange(this.appValue);
            this.onTouch();
        }
    }

    public registerOnChange(fn: any): void {
        this.onChange = typeof fn === "function" ? fn : this.onChange;
    }

    public registerOnTouched(fn: any): void {
        this.onTouch = typeof fn === "function" ? fn : this.onTouch;
    }

    public setDisabledState(isDisabled: boolean) {
        this.appDisabled = isDisabled;
    }

}
