/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {AbstractControlValueAccessorComponent} from "./abstract-control-value-accessor.component";

class ControlValueAccessorSpec extends AbstractControlValueAccessorComponent<number> {

}

describe("ControlValueAccessor", () => {

    let component: ControlValueAccessorSpec;

    beforeEach((() => {
        component = new ControlValueAccessorSpec();
    }));

    it("#.writeValue should change appValue", () => {
        const onChangeSpy = spyOn(component, "onChange");
        const onTouchSpy = spyOn(component, "onTouch");
        component.appValue = 0;
        component.writeValue(19);
        expect(component.appValue).toBe(19);
        expect(onChangeSpy).not.toHaveBeenCalled();
        expect(onTouchSpy).not.toHaveBeenCalled();

        component.writeValue(190, true);
        expect(component.appValue).toBe(190);
        expect(onChangeSpy).toHaveBeenCalledWith(190);
        expect(onTouchSpy).toHaveBeenCalled();
    });

    it("#.setDisable should set appDisabled", () => {
        expect(component.appDisabled).not.toBeDefined();
        component.setDisabledState(true);
        expect(component.appDisabled).toBe(true);
        component.setDisabledState(true);
        expect(component.appDisabled).toBe(true);
    });

    it("should register listeners", () => {
        // These calls should be ignored (because null is not a function)
        component.registerOnChange(null);
        component.registerOnTouched(null);
        expect(component.onChange).not.toThrow();
        expect(component.onTouch).not.toThrow();

        const fn = () => 19;
        component.registerOnChange(fn);
        component.registerOnTouched(fn);
        expect(component.onChange).toBe(fn);
        expect(component.onTouch).toBe(fn);
    });

});
