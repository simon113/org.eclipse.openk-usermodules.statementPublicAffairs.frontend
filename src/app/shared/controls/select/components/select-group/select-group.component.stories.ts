/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {action} from "@storybook/addon-actions";
import {boolean, number, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {ISelectOption, ISelectOptionGroup} from "../../model";
import {SelectModule} from "../../select.module";

function getValue(size: number, min: number, max: number): number[] {
    return Array(size).fill(0).map(() => Math.floor(min + Math.random() * (max - min)));
}

function getOptions(size: number): ISelectOption[] {
    return Array(size).fill(0).map((_, index) => {
        return {label: "Option " + index, value: index};
    });
}

function getGroups(...groupSizes: number[]): ISelectOptionGroup[] {
    let optionId = 0;
    return groupSizes.map((size, index) => {
        return {
            label: "Group " + index,
            options: Array(size).fill(0).map(() => optionId++)
        };
    });
}

storiesOf("Shared/Controls", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [SelectModule]}))
    .add("SelectGroupComponent", () => ({
        template: `
            <app-select-group
                (appValueChange)="appValueChange($event)"
                [appDisabled]="appDisabled"
                [appGroups]="appGroups"
                [appHideControls]="appHideControls"
                [appOptions]="appOptions"
                [appPairSize]="appPairSize"
                [appValue]="{selected: appSelected, indeterminate: appIndeterminateValues}"
                [appIndeterminate]="appIndeterminate"
                style="padding: 1em; width: calc(100% - 2em);">
            </app-select-group>
        `,
        props: {
            appDisabled: boolean("appDisabled", false),
            appHideControls: boolean("appHideControls", false),
            appValueChange: action("appValueChange"),
            appPairSize: number("appPairSize", 2),
            appSelected: [...getValue(6, 0, 10), ...getValue(4, 16, 22)],
            appIndeterminateValues: [...getValue(5, 0, 26)],
            appGroups: getGroups(10, 6, 6, 6),
            appOptions: getOptions(38),
            appIndeterminate: boolean("appIndeterminate", false)
        }
    }));
