/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, forwardRef, Input} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {arrayJoin, filterDistinctValues} from "../../../../../util/store";
import {AbstractControlValueAccessorComponent} from "../../../common";
import {ISelectOption, ISelectOptionGroup} from "../../index";

@Component({
    selector: "app-select-group",
    templateUrl: "./select-group.component.html",
    styleUrls: ["./select-group.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SelectGroupComponent),
            multi: true
        }
    ]
})
export class SelectGroupComponent<TOptionValueType = any>
    extends AbstractControlValueAccessorComponent<{ selected: TOptionValueType[], indeterminate?: TOptionValueType[] }> {

    private static id = 0;

    /**
     * Divides the options array into groups; each group is displayed in subgroups.
     */
    @Input()
    public appGroups: ISelectOptionGroup<TOptionValueType>[] = [];

    /**
     * If true, all inputs are replaced with simple icons.
     */
    @Input()
    public appHideControls: boolean;

    /**
     * This ID is combined with the indecies of the checkboxes and placed on every input element.
     */
    @Input()
    public appId = "DepartmentControlComponent" + SelectGroupComponent.id++;

    /**
     * List of selectable options displayed in the groups
     */
    @Input()
    public appOptions: ISelectOption<TOptionValueType>[] = [];

    /**
     * Defines the size of a sub group in the component.
     */
    @Input()
    public appPairSize;

    @Input()
    public appIndeterminate = false;

    /**
     * This function is used by the NgFor directive and controls when the elements are rendered anew.
     */
    public readonly trackByIndex = (index: number) => index;

    public toggleCheckbox(value: TOptionValueType, isSelected: boolean, isIndeterminate: boolean, inputElement: HTMLInputElement) {

        if (this.appDisabled || value == null) {
            return;
        }

        let newAppValue = {
            selected: this.appValue.selected.filter((val) => val !== value),
            indeterminate: this.appValue.indeterminate.filter((val) => val !== value)
        };
        if (isSelected) {
            if (this.appIndeterminate && !isIndeterminate) {
                newAppValue = {
                    ...newAppValue,
                    indeterminate: arrayJoin(newAppValue.indeterminate, [value])
                };
            }
        } else if (!isIndeterminate || !this.appIndeterminate) {
            newAppValue = {
                ...newAppValue,
                selected: arrayJoin(newAppValue.selected, [value])
            };
        } else if (isIndeterminate) {
            if (inputElement != null) {
                inputElement.checked = false;
            }
        }
        this.writeValue(newAppValue, true);
    }

    public writeValue(obj: { selected: TOptionValueType[], indeterminate?: TOptionValueType[] }, emit?: boolean) {
        // All values are filtered and sorted before writing and emitting them.
        super.writeValue({
            selected: filterDistinctValues(obj?.selected).sort(),
            indeterminate: filterDistinctValues(obj?.indeterminate).sort()
        }, emit);
    }

}

