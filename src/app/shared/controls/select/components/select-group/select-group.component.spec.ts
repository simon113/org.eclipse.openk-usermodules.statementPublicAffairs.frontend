/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ViewChild} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {FormsModule, NgForm} from "@angular/forms";
import {By} from "@angular/platform-browser";
import {ISelectOption, ISelectOptionGroup} from "../../model";
import {SelectModule} from "../../select.module";
import {SelectGroupComponent} from "./select-group.component";

describe("SelectGroupComponent", () => {
    let component: SelectGroupSpecComponent;
    let fixture: ComponentFixture<SelectGroupSpecComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SelectModule,
                FormsModule
            ],
            declarations: [
                SelectGroupSpecComponent
            ]
        }).compileComponents();
    }));

    beforeEach(async () => {
        fixture = TestBed.createComponent(SelectGroupSpecComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        await fixture.whenStable();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should register to Angular forms", async () => {
        component.appValue = {
            selected: [0, 1, 2],
            indeterminate: []
        };
        fixture.detectChanges();
        await fixture.whenStable();
        expect(component.ngForm.value).toBeDefined();
        expect(component.ngForm.value.selectGroup).toEqual({
            selected: [0, 1, 2],
            indeterminate: []
        });

        component.ngForm.setValue({
            selectGroup: {
                selected: [3, 4],
                indeterminate: [5, 6]
            }
        });
        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.component.appValue).toEqual(
            {
                selected: [3, 4],
                indeterminate: [5, 6]
            }
        );
    });

    it("should only render inputs if controls are not hidden", async () => {
        component.createGroups(10, 6, 6, 6);
        fixture.detectChanges();
        await fixture.whenStable();
        component.createOptions(28);
        fixture.detectChanges();
        await fixture.whenStable();

        let inputs = fixture.debugElement
            .queryAll(By.css(".select-group-entry--input"))
            .map((element) => element.nativeElement as HTMLInputElement);
        expect(inputs.length).toBe(28);

        component.appHideControls = true;
        fixture.detectChanges();
        await fixture.whenStable();

        inputs = fixture.debugElement
            .queryAll(By.css(".select-group-entry--input"))
            .map((element) => element.nativeElement as HTMLInputElement);
        expect(inputs.length).toBe(0);
    });

    it("should select and deselect options if not disabled", async () => {
        component.createGroups(10, 6, 6, 6);
        component.createOptions(28);
        fixture.detectChanges();
        await fixture.whenStable();

        component.component.toggleCheckbox(18, false, false, undefined);
        component.component.toggleCheckbox(19, false, false, undefined);

        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.component.appValue).toEqual({
            selected: [18, 19],
            indeterminate: []
        });
        expect(component.ngForm.value.selectGroup).toEqual({
            selected: [18, 19],
            indeterminate: []
        });

        component.component.toggleCheckbox(18, true, false, undefined);

        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.component.appValue).toEqual({
            selected: [19],
            indeterminate: []
        });
        expect(component.ngForm.value.selectGroup).toEqual({
            selected: [19],
            indeterminate: []
        });
    });

    it("should not select or deselect options if disabled", async () => {
        component.ngForm.control.disable();
        component.ngForm.control.patchValue({
            selectGroup: {
                selected: [19],
                indeterminate: []
            }
        });
        component.createGroups(10, 6, 6, 6);
        component.createOptions(28);
        fixture.detectChanges();
        await fixture.whenStable();

        component.component.toggleCheckbox(null, false, false, undefined);
        component.component.toggleCheckbox(18, false, false, undefined);

        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.component.appValue).toEqual({
            selected: [19],
            indeterminate: []
        });
        expect(component.ngForm.value.selectGroup).toEqual({
            selected: [19],
            indeterminate: []
        });

        component.component.toggleCheckbox(19, true, false, undefined);
        component.component.toggleCheckbox(null, true, false, undefined);

        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.component.appValue).toEqual({
            selected: [19],
            indeterminate: []
        });
        expect(component.ngForm.value.selectGroup).toEqual({
            selected: [19],
            indeterminate: []
        });
    });

    it("should set state to indeterminate from selected if appIndeterminate is set", async () => {
        component.createGroups(10, 6, 6, 6);
        component.createOptions(28);
        component.component.appDisabled = false;
        fixture.detectChanges();
        await fixture.whenStable();

        component.component.toggleCheckbox(18, false, false, undefined);

        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.component.appValue).toEqual({
            selected: [18],
            indeterminate: []
        });

        component.component.toggleCheckbox(18, true, false, undefined);

        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.component.appValue).toEqual({
            selected: [],
            indeterminate: []
        });

        component.component.appIndeterminate = true;

        component.component.toggleCheckbox(18, true, false, undefined);

        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.component.appValue).toEqual({
            selected: [],
            indeterminate: [18]
        });
    });
})
;

@Component({
    selector: "app-select-group-spec",
    template: `
        <form>
            <app-select-group
                name="selectGroup"
                [appHideControls]="appHideControls"
                [appGroups]="appGroups"
                [appOptions]="appOptions"
                [ngModel]="appValue">
            </app-select-group>
        </form>
    `
})
class SelectGroupSpecComponent {

    public appHideControls: boolean;

    public appGroups: ISelectOptionGroup[];

    public appOptions: ISelectOption[];

    public appValue: {
        selected: number[],
        indeterminate: number[]
    };

    @ViewChild(SelectGroupComponent)
    public component: SelectGroupComponent;

    @ViewChild(NgForm)
    public ngForm: NgForm;

    public createOptions(size: number) {
        return this.appOptions = Array(size).fill(0).map((_, index) => {
            return {label: "Option " + index, value: index};
        });
    }

    public createGroups(...groupSizes: number[]) {
        let optionId = 0;
        return this.appGroups = groupSizes.map((size, index) => {
            return {
                label: "Group " + index,
                options: Array(size).fill(0).map(() => optionId++)
            };
        });
    }

}
