/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {DropDownModule} from "../../layout/drop-down";
import {SharedPipesModule} from "../../pipes";
import {SelectComponent, SelectGroupComponent} from "./components";
import {ArrayToSelectOptionsPipe, FilterOptionsByGroupPipe, SelectedPipe} from "./pipes";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DropDownModule,
        MatIconModule,
        SharedPipesModule
    ],
    declarations: [
        SelectComponent,
        SelectGroupComponent,
        SelectedPipe,
        ArrayToSelectOptionsPipe,
        FilterOptionsByGroupPipe
    ],
    exports: [
        SelectComponent,
        SelectGroupComponent,
        SelectedPipe,
        ArrayToSelectOptionsPipe,
        FilterOptionsByGroupPipe
    ]
})
export class SelectModule {

}
