/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {IAPISearchOptions, IAPIStatementModel} from "../../../../core";
import {arrayJoin, filterDistinctValues} from "../../../../util/store";
import {StatementTableComponent} from "../../../layout/statement-table";
import {AbstractControlValueAccessorComponent} from "../../common";
import {ISelectOption} from "../../select";

/**
 * This component displays a list of statements. The columns to displayed are specified in the columns constant.
 * Statements from the list can be selected with a checkbox. The value of this component is the array of the selected statements ids.
 * There is also a searchbar on top of the table to be able to perform a filtering search on the list of statements. The search bar input
 * text is emitted to the parent component and the filtering of the actual array of statements needs to be done in that parent component.
 * An initial value can be set to the search bar with the input property appSearchText.
 */
@Component({
    selector: "app-statement-select",
    templateUrl: "statement-select.component.html",
    styleUrls: ["statement-select.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => StatementSelectComponent),
            multi: true
        }
    ]
})
export class StatementSelectComponent extends AbstractControlValueAccessorComponent<number[]> {

    @Input()
    public appIsLoading: boolean;

    @Input()
    public appSearchText: string;

    @Input()
    public appStatementTypeOptions: ISelectOption<number>[];

    @Input()
    public appSearchContent: IAPIStatementModel[];

    @Output()
    public appSearch: EventEmitter<IAPISearchOptions> = new EventEmitter();

    public columns = [...StatementTableComponent.STATEMENT_SELECT_COLUMNS];

    public toggle(id: number, isSelected: boolean) {
        if (isSelected) {
            const value = filterDistinctValues(arrayJoin(this.appValue, [id]))
                .sort((a, b) => a - b);
            this.writeValue(value, true);
        } else {
            const value = arrayJoin(this.appValue)
                .filter((statementId) => statementId !== id);
            this.writeValue(value, true);
        }
    }

}
