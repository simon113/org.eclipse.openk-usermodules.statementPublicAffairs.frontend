/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {CollapsibleModule} from "../layout/collapsible";
import {FilePreviewComponent} from "./file-preview.component";

@NgModule({
    imports: [
        CommonModule,
        CollapsibleModule,
        MatIconModule
    ],
    declarations: [
        FilePreviewComponent
    ],
    exports: [
        FilePreviewComponent
    ]
})
export class FilePreviewModule {

}
