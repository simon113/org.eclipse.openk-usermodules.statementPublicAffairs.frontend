/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {FileDropComponent} from "./file-drop.component";

describe("FileDropComponent", () => {
    let component: FileDropComponent;
    let fixture: ComponentFixture<FileDropComponent>;
    let fileListMock: FileListMock;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FileDropComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FileDropComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    beforeEach(() => {
        fileListMock = new FileListMock([
            new File(["File", "A"], "FileA.txt"),
            new File(["File", "B"], "FileB.txt"),
            new File(["File", "C"], "FileC.txt")
        ]);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should prevent default browser file drops", () => {
        const dragEvent = new DragEvent("dragover");
        const prevenDefaultSpy = spyOn(dragEvent, "preventDefault");
        component.onDocumentDrag(dragEvent);
        expect(prevenDefaultSpy).toHaveBeenCalled();
    });

    it("should not prevent default browser file drops if disabled", () => {
        component.appDisabled = true;
        const dragEvent = new DragEvent("dragover");
        const prevenDefaultSpy = spyOn(dragEvent, "preventDefault");
        component.onDocumentDrag(dragEvent);
        expect(prevenDefaultSpy).not.toHaveBeenCalled();
    });

    it("should call onInput when files are dropped", () => {
        const onInputSpy = spyOn(component, "onInput");

        component.onDrop(null);
        expect(onInputSpy).not.toHaveBeenCalled();
        component.onDrop(new DragEvent("drop"));
        expect(onInputSpy).not.toHaveBeenCalled();
        component.onDrop({dataTransfer: {files: fileListMock.toFileList()}} as DragEvent);
        expect(onInputSpy).toHaveBeenCalledWith(fileListMock.toFileList());
    });

    it("should open file select dialog", () => {
        const clickSpy = spyOn(component.inputElement.nativeElement, "click");
        component.appDisabled = true;
        component.openDialog();
        expect(clickSpy).not.toHaveBeenCalled();

        component.appDisabled = false;
        component.openDialog();
        expect(clickSpy).toHaveBeenCalled();
    });

    it("should emit file array on input", () => {
        const fileDropEmitSpy = spyOn(component.appFileDrop, "emit");
        component.appDisabled = true;
        component.onInput(fileListMock.toFileList());
        expect(fileDropEmitSpy).not.toHaveBeenCalled();

        component.appDisabled = false;
        component.onInput(fileListMock.toFileList());
        expect(fileDropEmitSpy).toHaveBeenCalledWith(fileListMock.array);
        component.onInput(null);
        expect(fileDropEmitSpy).toHaveBeenCalledWith([]);
    });

});


class FileListMock {

    constructor(public array: File[]) {

    }

    public get length(): number {
        return this.array.length;
    }

    public item(idx) {
        return this.array[idx];
    }

    public toFileList() {
        return this as any as FileList;
    }


}
