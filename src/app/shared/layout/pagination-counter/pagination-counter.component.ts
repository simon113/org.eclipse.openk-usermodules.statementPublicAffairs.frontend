/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {ISelectOption} from "../../controls/select/model";

@Component({
    selector: "app-pagination-counter",
    templateUrl: "./pagination-counter.component.html",
    styleUrls: ["./pagination-counter.component.scss"],
})
export class PaginationCounterComponent {

    @Input()
    public appDisabled: boolean;

    @Input()
    public appPage: number;

    @Input()
    public appPageSizeOptions: ISelectOption[] = [
        {label: "10", value: 10},
        {label: "25", value: 25},
        {label: "50", value: 50}
    ];

    @Input()
    public appPageSize = this.appPageSizeOptions[0].value;

    @Input()
    public appTotalPages: number;

    @Output()
    public appPageChange = new EventEmitter<{ page: number, size: number }>();

    public getPreviousPage(pageSize: number) {
        this.appPageSize = pageSize;
        this.appPageChange.emit({page: this.appPage - 1, size: this.appPageSize});
    }

    public getNextPage(pageSize: number) {
        this.appPageSize = pageSize;
        this.appPageChange.emit({page: this.appPage + 1, size: this.appPageSize});
    }

}
