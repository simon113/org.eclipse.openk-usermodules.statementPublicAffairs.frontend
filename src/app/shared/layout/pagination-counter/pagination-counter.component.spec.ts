/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {I18nModule} from "../../../core/i18n";
import {PaginationCounterComponent} from "./pagination-counter.component";
import {PaginationCounterModule} from "./pagination-counter.module";

describe("PaginationCounterComponent", () => {
    let component: PaginationCounterComponent;
    let fixture: ComponentFixture<PaginationCounterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                PaginationCounterModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PaginationCounterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(component).toBeTruthy();
    });

    it("should emit appPageChange with the correct page number", () => {
        component.appPage = 3;
        spyOn(component.appPageChange, "emit").and.callThrough();
        component.getNextPage(10);
        expect(component.appPageChange.emit).toHaveBeenCalledWith({page: 4, size: 10});
        component.getPreviousPage(10);
        expect(component.appPageChange.emit).toHaveBeenCalledWith({page: 2, size: 10});
    });
});
