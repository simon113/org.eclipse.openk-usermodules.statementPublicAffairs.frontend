/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {I18nModule} from "../../../../core/i18n";
import {IStatementTableEntry} from "../model";
import {StatementTableModule} from "../statement-table.module";
import {StatementTableComponent} from "./statement-table.component";

describe("StatementTableComponent", () => {
    let component: StatementTableComponent;
    let fixture: ComponentFixture<StatementTableComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                StatementTableModule,
                I18nModule,
                RouterTestingModule
            ]
        }).compileComponents();
    });
    beforeEach(() => {
        fixture = TestBed.createComponent(StatementTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should emit appToggleSelect with the id and corresponding checked value", () => {
        const selectSpy = spyOn(component.appSelect, "emit").and.callThrough();
        component.select(1);
        expect(selectSpy).toHaveBeenCalledWith({id: 1, value: true});
        component.deselect(1);
        expect(selectSpy).toHaveBeenCalledWith({id: 1, value: true});
    });

    it("should track rows by id", () => {
        expect(component.trackBy(1919, null)).not.toBeDefined();
        expect(component.trackBy(1919, {...{} as IStatementTableEntry})).not.toBeDefined();
        expect(component.trackBy(1919, {...{} as IStatementTableEntry, id: 19})).toBe(19);
    });

});
