/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Directive, Input, OnChanges, OnDestroy, SimpleChanges, TemplateRef} from "@angular/core";
import {SideMenuRegistrationService} from "../services";
import {TSideMenuContentPosition} from "../TSideMenuContentPosition";

@Directive({
    selector: "[appSideMenu]"
})
export class SideMenuDirective implements OnDestroy, OnChanges {

    @Input()
    public appSideMenu: TSideMenuContentPosition = "bottom";

    @Input()
    public appSideMenuLeft: boolean;

    @Input()
    public appSideMenuTitle: string;

    @Input()
    public appSideMenuStyle: { [styleProperty: string]: any; };

    public test: boolean;

    public constructor(
        public readonly templateRef: TemplateRef<any>,
        public readonly sideMenuRegistrationService: SideMenuRegistrationService
    ) {

    }

    public ngOnChanges(changes: SimpleChanges) {
        const keys: Array<keyof SideMenuDirective> = ["appSideMenu", "appSideMenuLeft", "appSideMenuTitle", "appSideMenuStyle"];
        if (keys.filter((_) => changes[_] != null).length > 0) {
            this.sideMenuRegistrationService.register(this);
        }
    }

    public ngOnDestroy() {
        this.sideMenuRegistrationService.deregister(this);
    }

}
