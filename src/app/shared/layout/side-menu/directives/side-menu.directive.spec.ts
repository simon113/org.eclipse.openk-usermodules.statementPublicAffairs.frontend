/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, SimpleChange, SimpleChanges, ViewChild} from "@angular/core";
import {async, TestBed} from "@angular/core/testing";
import {SideMenuRegistrationService} from "../services";
import {SideMenuModule} from "../side-menu.module";
import {SideMenuDirective} from "./side-menu.directive";

describe("SideMenuDirective", () => {
    let directive: SideMenuDirective;
    let registration: SideMenuRegistrationService;
    let resetSpyCalls = () => null;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SideMenuDirectiveSpecComponent],
            imports: [SideMenuModule]
        }).compileComponents();
    }));

    beforeEach(async () => {
        const fixture = TestBed.createComponent(SideMenuDirectiveSpecComponent);
        directive = fixture.componentInstance.directive;
        registration = fixture.componentInstance.registration;
        fixture.detectChanges();
        await fixture.whenStable();
        const spies = [spyOn(registration, "register"), spyOn(registration, "deregister")];
        resetSpyCalls = () => spies.forEach((_) => _.calls.reset());
    });

    it("should create", () => {
        expect(directive).toBeTruthy();
    });

    it("should re-register on changes", () => {
        expect(registration.register).not.toHaveBeenCalled();
        ["appSideMenu", "appSideMenuLeft", "appSideMenuTitle", "appSideMenuStyle"]
            .map<SimpleChanges>((_) => ({[_]: new SimpleChange(0, 1, false)}))
            .forEach((changes) => {
                resetSpyCalls();
                directive.ngOnChanges(changes);
                expect(registration.register).toHaveBeenCalledWith(directive);
            });
        resetSpyCalls();
        directive.ngOnChanges({});
        expect(registration.register).not.toHaveBeenCalled();
    });

    it("should deregister on destroy", () => {
        resetSpyCalls();
        expect(registration.register).not.toHaveBeenCalled();
        directive.ngOnDestroy();
        expect(registration.deregister).toHaveBeenCalledWith(directive);
    });
});

@Component({
    selector: "app-side-menu-directive-spec",
    template: `
        <div *appSideMenu="'top'">
        </div>
    `
})
class SideMenuDirectiveSpecComponent {

    @ViewChild(SideMenuDirective, {static: true})
    public directive: SideMenuDirective;

    public constructor(public readonly registration: SideMenuRegistrationService) {

    }

}
