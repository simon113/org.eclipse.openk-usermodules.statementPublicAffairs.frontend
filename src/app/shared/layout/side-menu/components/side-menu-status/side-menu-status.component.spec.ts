/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {SideMenuModule} from "../../side-menu.module";
import {SideMenuStatusComponent} from "./side-menu-status.component";

describe("SideMenuStatusComponent", () => {
    let component: SideMenuStatusComponent;
    let fixture: ComponentFixture<SideMenuStatusComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [SideMenuModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SideMenuStatusComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
