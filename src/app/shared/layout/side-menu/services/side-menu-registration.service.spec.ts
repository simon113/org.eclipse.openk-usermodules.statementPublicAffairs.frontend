/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ViewChild} from "@angular/core";
import {async, TestBed} from "@angular/core/testing";
import {take} from "rxjs/operators";
import {SideMenuDirective} from "../directives";
import {SideMenuModule} from "../side-menu.module";
import {SideMenuRegistrationService} from "./side-menu-registration.service";

describe("SideMenuRegistrationService", () => {
    let directive: SideMenuDirective;
    let registration: SideMenuRegistrationService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SideMenuDirectiveSpecComponent],
            imports: [SideMenuModule]
        }).compileComponents();
    }));

    beforeEach(async () => {
        const fixture = TestBed.createComponent(SideMenuDirectiveSpecComponent);
        directive = fixture.componentInstance.directive;
        registration = fixture.componentInstance.registration;
        fixture.detectChanges();
        await fixture.whenStable();
    });

    it("should return content of side menu", async () => {
        const getContent$ = registration.content$.pipe(take(1));
        await expectAsync(getContent$.toPromise()).toBeResolvedTo([undefined, undefined, directive]);
        registration.deregister(directive);
        await expectAsync(getContent$.toPromise()).toBeResolvedTo([undefined, undefined, undefined]);
    });

    it("should return if content is available", async () => {
        const getHasContent$ = registration.hasContent$.pipe(take(1));
        await expectAsync(getHasContent$.toPromise()).toBeResolvedTo(true);
        registration.deregister(directive);
        await expectAsync(getHasContent$.toPromise()).toBeResolvedTo(false);
    });

    it("should return if the side menu should be placed on the left side", async () => {
        const getLeft$ = registration.left$.pipe(take(1));
        await expectAsync(getLeft$.toPromise()).toBeResolvedTo(false);
        directive.appSideMenuLeft = true;
        registration.register(directive);
        await expectAsync(getLeft$.toPromise()).toBeResolvedTo(true);
        registration.deregister(directive);
        await expectAsync(getLeft$.toPromise()).toBeResolvedTo(false);
    });

    it("should return a style object for the side menu", async () => {
        const getStyle$ = registration.style$.pipe(take(1));
        await expectAsync(getStyle$.toPromise()).toBeResolvedTo(undefined);
        directive.appSideMenuStyle = {padding: "0"};
        registration.register(directive);
        await expectAsync(getStyle$.toPromise()).toBeResolvedTo({padding: "0"});
    });

    it("should return the title of the side menu", async () => {
        const getTitle$ = registration.title$.pipe(take(1));
        await expectAsync(getTitle$.toPromise()).toBeResolvedTo("Side Menu Spec");
        registration.deregister(directive);
        await expectAsync(getTitle$.toPromise()).toBeResolvedTo(undefined);
    });

});

@Component({
    selector: "app-side-menu-directive-spec",
    template: `
        <div *appSideMenu="'bottom'; title: 'Side Menu Spec'">
        </div>
    `
})
class SideMenuDirectiveSpecComponent {

    @ViewChild(SideMenuDirective, {static: true})
    public directive: SideMenuDirective;

    public constructor(public readonly registration: SideMenuRegistrationService) {

    }

}
