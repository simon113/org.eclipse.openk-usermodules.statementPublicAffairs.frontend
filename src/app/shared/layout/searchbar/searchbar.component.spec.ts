/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {By} from "@angular/platform-browser";
import {I18nModule} from "../../../core/i18n";
import {SearchbarComponent} from "./searchbar.component";
import {SearchbarModule} from "./searchbar.module";

describe("SearchbarComponent", () => {
    let component: SearchbarComponent;
    let fixture: ComponentFixture<SearchbarComponent>;
    let inputElement: HTMLInputElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [I18nModule, SearchbarModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        inputElement = fixture.debugElement.query(By.css(".search--bar--input")).nativeElement;
    });

    it("should emit the current value of the input field", () => {
        spyOn(component.appSearch, "emit").and.callThrough();

        inputElement.value = "on input";
        inputElement.dispatchEvent(new InputEvent("input"));
        expect(component.appSearch.emit).toHaveBeenCalledWith("on input");

        inputElement.value = "on enter";
        inputElement.dispatchEvent(new KeyboardEvent("keydown", {key: "enter"}));
        expect(component.appSearch.emit).toHaveBeenCalledWith("on enter");
    });

});
