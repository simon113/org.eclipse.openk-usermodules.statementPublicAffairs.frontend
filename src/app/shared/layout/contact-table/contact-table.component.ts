/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {IAPIContactPerson} from "../../../core/api/contacts/IAPIContactPerson";

type AvailableColumns = "lastName" | "firstName" | "email" | "company";

@Component({
    selector: "app-contact-table",
    templateUrl: "./contact-table.component.html",
    styleUrls: ["./contact-table.component.scss"],
})
export class ContactTableComponent {

    @Input()
    public appDisabled: boolean;

    @Input()
    public appEntries: IAPIContactPerson[];

    @Input()
    public appSelectedId: string;

    @Output()
    public appSelectedIdChange = new EventEmitter<string>();

    public columnsToDisplay: AvailableColumns[] = ["lastName", "firstName", "email", "company"];

    public onSelect(id: string) {
        this.appSelectedId = this.appSelectedId === id ? undefined : id;
        this.appSelectedIdChange.emit(this.appSelectedId);
    }

}
