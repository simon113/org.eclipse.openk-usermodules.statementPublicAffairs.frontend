/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {IAPIContactPerson} from "../../../core/api/contacts/IAPIContactPerson";
import {I18nModule} from "../../../core/i18n";
import {ContactTableComponent} from "./contact-table.component";
import {ContactTableModule} from "./contact-table.module";

describe("ContactTableComponent", () => {
    let component: ContactTableComponent;
    let fixture: ComponentFixture<ContactTableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                ContactTableModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ContactTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(component).toBeTruthy();
    });

    it("should emit appSelectedContact", () => {
        spyOn(component.appSelectedIdChange, "emit").and.callThrough();
        const contact: IAPIContactPerson = {
            companyId: "string",
            companyName: "string",
            email: "string",
            id: "string",
            firstName: "string",
            lastName: "string"
        };

        component.onSelect(contact.id);
        expect(component.appSelectedIdChange.emit).toHaveBeenCalledWith(contact.id);
        component.onSelect(contact.id);
        expect(component.appSelectedIdChange.emit).toHaveBeenCalledWith(undefined);
    });
});
