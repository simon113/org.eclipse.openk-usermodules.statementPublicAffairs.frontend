/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {SharedPipesModule} from "../../../../shared/pipes";
import {EditDebugComponent} from "./edit-debug.component";

describe("EditDebugComponent", () => {
    let component: EditDebugComponent;
    let fixture: ComponentFixture<EditDebugComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditDebugComponent
            ],
            imports: [
                SharedPipesModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditDebugComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should convert objects to string", () => {
        expect(component.toString({responsible: "Boolean"})).toBe("{\"responsible\":\"Boolean\"}");
        expect(component.toString("19")).toBe("19");
    });

    it("should submit all required booleans", () => {
        component.appTask = {requiredVariables: {variable: "Boolean"}} as any;
        expect(() => component.submit(true)).not.toThrow();
    });
});
