/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {SimpleChange} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {TranslateService} from "@ngx-translate/core";
import {take} from "rxjs/operators";
import {EAPIProcessTaskDefinitionKey, EAPIUserRoles, I18nModule, IAPIProcessTask} from "../../../../core";
import {EErrorCode} from "../../../../store/root/model";
import {StatementDetailsModule} from "../../statement-details.module";
import {StatementDetailsSideMenuComponent} from "./statement-details-side-menu.component";

describe("StatementDetailsSideMenuComponent", () => {
    let component: StatementDetailsSideMenuComponent;
    let fixture: ComponentFixture<StatementDetailsSideMenuComponent>;
    let translateService: TranslateService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementDetailsModule, I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementDetailsSideMenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        translateService = TestBed.inject(TranslateService);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should call update on input changes", () => {
        const updateSpy = spyOn(component, "update");
        const keys: Array<keyof StatementDetailsSideMenuComponent> = ["appUserRoles", "appTasks", "appUserName"];
        updateSpy.calls.reset();

        component.ngOnChanges({});
        expect(updateSpy).not.toHaveBeenCalled();

        keys.map((_) => ({[_]: new SimpleChange(0, 1, false)}))
            .forEach((changes) => {
                component.ngOnChanges(changes);
                expect(updateSpy).toHaveBeenCalledWith();
            });
    });

    it("should update add buttons based upon current user roles and tasks", () => {
        component.update();
        expect(component.buttonLayout).toEqual([]);

        component.appUserRoles = [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE];
        component.appTasks = [{
            ...{} as IAPIProcessTask,
            statementId: 19,
            taskId: "abcde",
            authorized: true,
            taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA
        }];
        component.update();
        expect(component.buttonLayout.length).toEqual(2);

        component.appUserRoles = [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE];
        component.appTasks = [{
            ...{} as IAPIProcessTask,
            statementId: 19,
            taskId: "abcde",
            authorized: false,
            taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA
        }];
        component.update();
        expect(component.buttonLayout.length).toEqual(0);
    });

    it("should update info message based upon current user name and tasks", async () => {
        await component.update();
        expect(component.buttonLayout).toEqual([]);

        component.appUserName = "hugo";
        component.appUserRoles = [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE];
        component.appTasks = [{
            ...{} as IAPIProcessTask,
            statementId: 19,
            taskId: "abcde",
            taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA,
            authorized: true,
            assignee: "hugo"
        }];
        await component.update();
        expect(component.infoMessage).not.toBeDefined();

        component.appUserName = "noAssignee";
        await component.update();
        const expectedInfoMessage = await translateService.get(EErrorCode.CLAIMED_BY_OTHER_USER, {user: "hugo"})
            .pipe(take(1)).toPromise();
        expect(component.infoMessage).toEqual(expectedInfoMessage);
    });
});
