/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {RouterTestingModule} from "@angular/router/testing";
import {number, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {StatementDetailsModule} from "../../../statement-details.module";
import {ProcessDiagramComponent} from "./process-diagram.component";
import {diagram} from "./StellungnahmeXmlAsString";


storiesOf("Features / 02 Process", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({declarations: [], imports: [StatementDetailsModule, RouterTestingModule]}))
    .add("ProcessDiagramComponent", () => ({
        template: `
        <div style="height: 600px;">
            <app-process-diagram #history [appShowEnlargeButton]="false"
                           [appStatementId]="appStatementId" [appWorkflowXml]="appWorkflowXml" [appActivitiesToSelect]="[appActivitiesToSelect]">
            </app-process-diagram>
        </div>
        `,
        props: {
            appStatementId: number("statementId", 1),
            appActivitiesToSelect: text("field to highlight", "addWorkflowData"),
            appWorkflowXml: diagram
        }
    }));


