/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ElementRef, ViewChild} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {MatIconModule} from "@angular/material/icon";
import {By} from "@angular/platform-browser";
import {I18nModule} from "../../../../../core/i18n";
import {StatementDetailsModule} from "../../../statement-details.module";
import {IProcessHistoryData} from "./IProcessHistoryData";
import {ProcessHistoryComponent} from "./process-history.component";

@Component({
    selector: `app-host-component`,
    template: `
        <div style="height: 200px;"><app-process-history #history [appProcessHistoryData]="appData"></app-process-history></div>
        `
})
class TestHostComponent {
    public appData: Array<IProcessHistoryData>;
    @ViewChild("history", {read: ElementRef}) history: ElementRef;

    public constructor() {
        this.appData = (new Array(40)).fill({icon: "icon"});
    }
}

describe("ProcessHistoryComponent", () => {
    let component: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ProcessHistoryComponent, TestHostComponent],
            imports: [
                StatementDetailsModule,
                I18nModule,
                MatIconModule
            ],
        }).compileComponents();
    });
    beforeEach(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should scroll to the bottom of the list of elements", async () => {

        const childDebugElement = fixture.debugElement.query(By.directive(ProcessHistoryComponent));
        const childComponent = childDebugElement.componentInstance;

        expect(childComponent.table.nativeElement.scrollTop).toEqual(0);
        await childComponent.ngAfterViewInit();
        fixture.detectChanges();
        await fixture.whenStable();

        // scrollHeight - host-component-height 200px + border 2px (box-sizing: border-box)
        expect(childComponent.table.nativeElement.scrollTop).toEqual(childComponent.table.nativeElement.scrollHeight - 200 + 2);
    });
});
