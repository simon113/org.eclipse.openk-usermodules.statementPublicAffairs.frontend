/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {MatIconModule} from "@angular/material/icon";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core/i18n";
import {CardModule} from "../../../../shared/layout/card";
import {addCommentAction, deleteCommentAction, fetchStatementDetailsAction, queryParamsIdSelector} from "../../../../store";
import {StatementDetailsComponent} from "./statement-details.component";

describe("StatementDetailsComponent", () => {
    let component: StatementDetailsComponent;
    let fixture: ComponentFixture<StatementDetailsComponent>;
    let mockStore: MockStore;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [StatementDetailsComponent],
            imports: [RouterTestingModule, MatIconModule, CardModule, I18nModule],
            providers: [provideMockStore({initialState: {statements: {}}})]
        }).compileComponents();
        mockStore = TestBed.inject(MockStore);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should dispatch fetch details action", () => {
        const queryParamsIdSelectorMock = mockStore.overrideSelector(queryParamsIdSelector, undefined);
        const dispatchSpy = spyOn(mockStore, "dispatch");
        const action = fetchStatementDetailsAction({statementId: 19});
        queryParamsIdSelectorMock.setResult(19);
        mockStore.refreshState();
        expect(dispatchSpy).toHaveBeenCalledWith(action);
        component.ngOnDestroy();
    });

    it("should dispatch add comment action", async () => {
        const dispatchSpy = spyOn(mockStore, "dispatch");

        const queryParamsIdSelectorMock = mockStore.overrideSelector(queryParamsIdSelector, undefined);
        queryParamsIdSelectorMock.setResult(19);
        mockStore.refreshState();

        await component.addComment("test comment");
        expect(dispatchSpy).toHaveBeenCalledWith(addCommentAction({
            statementId: 19,
            text: "test comment"
        }));
    });

    it("should dispatch delete comment action", async () => {
        const dispatchSpy = spyOn(mockStore, "dispatch");

        const queryParamsIdSelectorMock = mockStore.overrideSelector(queryParamsIdSelector, undefined);
        queryParamsIdSelectorMock.setResult(19);
        mockStore.refreshState();

        await component.deleteComment(2);
        expect(dispatchSpy).toHaveBeenCalledWith(deleteCommentAction({
            statementId: 19,
            commentId: 2
        }));
    });
});
