/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {take} from "rxjs/operators";
import {startAttachmentDownloadAction} from "../../../../store/attachments/actions";
import {getOutboxAttachments, getStatementPdfAttachment} from "../../../../store/attachments/selectors";
import {queryParamsIdSelector} from "../../../../store/root/selectors";

/**
 * This component displays the statements attachments that are meant for the statement response (outbox)
 * and the generated Statement.pdf that is also sent in the response,
 * The files can be downloaded by pressing a button.
 */

@Component({
    selector: "app-statement-details-outbox",
    templateUrl: "./statement-details-outbox.component.html",
    styleUrls: ["./statement-details-outbox.component.scss"]
})
export class StatementDetailsOutboxComponent {

    @Input()
    public appCollapsed = false;

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public statementAttachment$ = this.store.pipe(select(getStatementPdfAttachment));

    public outboxAttachments$ = this.store.pipe(select(getOutboxAttachments));

    public constructor(public store: Store) {
    }

    public async downloadAttachment(attachmentId: number) {
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        this.store.dispatch(startAttachmentDownloadAction({statementId, attachmentId}));
    }

}
