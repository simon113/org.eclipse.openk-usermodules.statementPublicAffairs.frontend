/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {FormArray} from "@angular/forms";
import {select, Store} from "@ngrx/store";
import {delay, take, takeUntil} from "rxjs/operators";
import {startAttachmentDownloadAction} from "../../../../store/attachments/actions";
import {IAttachmentControlValue} from "../../../../store/attachments/model";
import {getConsiderationAttachments, getStatementAttachmentCacheSelector} from "../../../../store/attachments/selectors";
import {setErrorAction} from "../../../../store/root/actions";
import {isOfficialInChargeSelector, queryParamsIdSelector} from "../../../../store/root/selectors";
import {submitConsiderationFilesAction} from "../../../../store/statements/actions";
import {statementLoadingSelector} from "../../../../store/statements/selectors";
import {createFormGroup} from "../../../../util/forms";
import {AbstractReactiveFormComponent} from "../../../forms/abstract";

/**
 * This component displays the list of files saved as consideration result to the statement.
 * For the role of official in charge files can be uploaded as consideration via the file drop component.
 */

@Component({
    selector: "app-statement-details-considerations",
    templateUrl: "./statement-details-considerations.component.html",
    styleUrls: ["./statement-details-considerations.component.scss"]
})
export class StatementDetailsConsiderationsComponent
    extends AbstractReactiveFormComponent<{ considerations: IAttachmentControlValue[] }> implements OnInit, OnDestroy {

    @Input()
    public appCollapsed = false;

    @Input()
    public appFinished: boolean;

    @Input()
    public appFormGroup = createFormGroup<{ considerations: IAttachmentControlValue[] }>({
        considerations: new FormArray([])
    });

    public isOfficialInCharge$ = this.store.pipe(select(isOfficialInChargeSelector));

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public considerations$ = this.store.pipe(select(getConsiderationAttachments));

    public fileCache$ = this.store.pipe(select(getStatementAttachmentCacheSelector));

    public isStatementLoading$ = this.store.pipe(select(statementLoadingSelector));

    public constructor(public readonly store: Store) {
        super();
    }

    public ngOnInit() {
        this.fileCache$.pipe(delay(0), takeUntil(this.destroy$))
            .subscribe((values) => this.setValueForArray(values, "considerations"));
    }

    public async submit() {
        await this.clearErrors();
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        const value = this.getValue("considerations");
        this.store.dispatch(submitConsiderationFilesAction({
            statementId,
            value
        }));
    }

    public async downloadAttachment(attachmentId: number) {
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        this.store.dispatch(startAttachmentDownloadAction({statementId, attachmentId}));
    }

    private async clearErrors() {
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        const loading = await this.isStatementLoading$.pipe(take(1)).toPromise();
        if (statementId != null && !loading) {
            this.store.dispatch(setErrorAction({statementId, error: null}));
        }
    }

}
