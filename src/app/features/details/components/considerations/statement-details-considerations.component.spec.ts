/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core/i18n";
import {startAttachmentDownloadAction} from "../../../../store/attachments/actions";
import {IAttachmentControlValue} from "../../../../store/attachments/model";
import {setErrorAction} from "../../../../store/root/actions";
import {queryParamsIdSelector} from "../../../../store/root/selectors";
import {submitConsiderationFilesAction} from "../../../../store/statements/actions";
import {StatementDetailsModule} from "../../statement-details.module";
import {StatementDetailsConsiderationsComponent} from "./statement-details-considerations.component";

describe("StatementDetailsConsiderationsComponent", () => {
    let component: StatementDetailsConsiderationsComponent;
    let fixture: ComponentFixture<StatementDetailsConsiderationsComponent>;
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementDetailsModule,
                I18nModule
            ],
            providers: [
                provideMockStore({
                    initialState: {},
                    selectors: [
                        {
                            selector: queryParamsIdSelector,
                            value: 19
                        }
                    ]
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementDetailsConsiderationsComponent);
        component = fixture.componentInstance;
        store = fixture.componentRef.injector.get(Store);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should dispatch submitConsiderationFilesAction on submitting with the current files from cache", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        const file: IAttachmentControlValue = {name: "file1", tagIds: []};
        component.setValueForArray([file], "considerations");
        await component.submit();
        expect(dispatchSpy).toHaveBeenCalledWith(setErrorAction({statementId: 19, error: null}));
        expect(dispatchSpy).toHaveBeenCalledWith(submitConsiderationFilesAction({
            statementId: 19, value: [file]
        }));
    });

    it("should dispatch startAttachmentDownloadAction with correct attachmentId", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.downloadAttachment(15);
        expect(dispatchSpy).toHaveBeenCalledWith(startAttachmentDownloadAction({statementId: 19, attachmentId: 15}));
    });
});
