/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core/i18n";
import {openGisAction} from "../../../../store/geo/actions";
import {userNameSelector} from "../../../../store/root/selectors";
import {ILeafletBounds} from "../../../map/directives/leaflet";
import {StatementDetailsModule} from "../../statement-details.module";
import {StatementDetailsGeographicPositionComponent} from "./statement-details-geographic-position.component";

describe("StatementDetailsGeographicPositionComponent", () => {
    const user = "userName";

    let component: StatementDetailsGeographicPositionComponent;
    let fixture: ComponentFixture<StatementDetailsGeographicPositionComponent>;
    let store: MockStore;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementDetailsModule,
                I18nModule
            ],
            providers: [
                provideMockStore({
                    selectors: [{selector: userNameSelector, value: user}]
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementDetailsGeographicPositionComponent);
        store = TestBed.inject(MockStore);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should open GIS", () => {
        spyOn(store, "dispatch");
        const bounds = {} as ILeafletBounds;
        component.openGis(bounds);
        expect(store.dispatch).toHaveBeenCalledWith(openGisAction({bounds, user}));
    });

});
