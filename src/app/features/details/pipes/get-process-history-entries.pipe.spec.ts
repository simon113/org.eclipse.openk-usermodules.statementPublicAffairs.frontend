/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {IAPIProcessActivity, IAPIStatementHistory} from "../../../core/api/process";
import {GetProcessHistoryEntriesPipe} from "./get-process-history-entries.pipe";

describe("GetProcessHistoryEntriesPipe", () => {

    const pipe = new GetProcessHistoryEntriesPipe();

    describe("transform", () => {

        const history: IAPIStatementHistory = {
            processName: "processName",
            processVersion: 0,
            finishedProcessActivities: [
                {
                    activityName: "activityName",
                    activityType: "userTask",
                    assignee: "assignee",
                    endTime: "endTime",
                    canceled: false
                } as IAPIProcessActivity
            ],
            currentProcessActivities: [
                {
                    activityName: "activityName1",
                    activityType: "serviceTask",
                    assignee: "assignee1",
                    endTime: "endTime1",
                    canceled: true
                } as IAPIProcessActivity
            ]
        };

        it("should return empty array for invalid input", () => {
            let result = pipe.transform(null);
            expect(result).toEqual([]);
            result = pipe.transform(undefined);
            expect(result).toEqual([]);
        });

        it("should combine active and finished activities and returns a list of all user and service task as process history data", () => {
            const result = pipe.transform(history);
            expect(result).toEqual([
                {
                    icon: "account_circle",
                    activityName: "activityName",
                    assignee: "assignee",
                    endTime: "endTime",
                    cancelled: false
                },
                {
                    icon: "group_work",
                    activityName: "activityName1",
                    assignee: "assignee1",
                    endTime: "endTime1",
                    cancelled: true
                }
            ]);
        });
    });
});

