/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPIProcessActivity, IAPIStatementHistory} from "../../../core/api/process";
import {arrayJoin} from "../../../util/store";
import {IProcessHistoryData} from "../components/process-information/process-history";


/**
 * From the process history data combines finished and ongoing tasks, then filters for only user and service tasks.
 */

@Pipe({name: "getProcessHistoryEntries"})
export class GetProcessHistoryEntriesPipe implements PipeTransform {

    private userTaskIcon = "account_circle";

    private serviceTaskIcon = "group_work";

    public transform(value: IAPIStatementHistory): IProcessHistoryData[] {
        return arrayJoin(value?.finishedProcessActivities, value?.currentProcessActivities)
            .filter((activity) => activity.activityType === "userTask" || activity.activityType === "serviceTask")
            .map((activity) => this.mapToEntry(activity));
    }

    private mapToEntry(activity: IAPIProcessActivity): IProcessHistoryData {
        return {
            icon: this.getIcon(activity.activityType),
            activityName: activity.activityName,
            assignee: activity.assignee,
            endTime: activity.endTime,
            cancelled: activity.canceled
        };
    }

    private getIcon(activityType: string) {
        switch (activityType) {
            case "userTask":
                return this.userTaskIcon;
            case "serviceTask":
                return this.serviceTaskIcon;
            default:
                return "";
        }
    }

}
