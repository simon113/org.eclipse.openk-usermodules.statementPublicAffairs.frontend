/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {By} from "@angular/platform-browser";
import {timer} from "rxjs";
import {diagram} from "../components/process-information/process-diagram/StellungnahmeXmlAsString";
import {StatementDetailsModule} from "../statement-details.module";
import {BpmnDirective} from "./bpmn-directive";

@Component({
    selector: `app-host-component`,
    template: `
        <div [bpmnDiagram]="{xml: xml, currentActivities: currentActivities}" appBpmn></div>`
})
class TestHostComponent {
    public xml = diagram;
    public currentActivities: Array<string> = ["addWorkflowData"];
}

describe("BpmnDirective", () => {

    let component: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [StatementDetailsModule],
            declarations: [TestHostComponent],
            providers: []
        })
            .compileComponents();
    }));

    beforeEach(async () => {
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        await fixture.whenStable();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should render the diagram and mark the right element", async () => {
        let highlightedElements = [];
        for (let i = 0; i < 10; i++) {
            await timer(100).toPromise();
            highlightedElements = fixture.debugElement.queryAll(By.css(".highlight-bpmn"));
            if (highlightedElements.length > 0) {
                break;
            }
        }
        expect(highlightedElements.length).toEqual(1);
        expect(highlightedElements[0].nativeElement.getAttribute("data-element-id")).toBe(component.currentActivities[0]);
    });

    it("should change the marker when input changes", async () => {
        component.currentActivities = ["addBasicInfoData"];
        fixture.detectChanges();
        await fixture.whenStable();
        let highlightedElements = [];
        for (let i = 0; i < 10; i++) {
            await timer(100).toPromise();
            highlightedElements = fixture.debugElement.queryAll(By.css(".highlight-bpmn"));
            if (highlightedElements.length > 0) {
                break;
            }
        }
        expect(highlightedElements.length).toEqual(1);
        expect(highlightedElements[0].nativeElement.getAttribute("data-element-id")).toBe(component.currentActivities[0]);
    });
});
