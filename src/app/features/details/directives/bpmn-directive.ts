/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Directive, ElementRef, Input, NgZone, OnDestroy} from "@angular/core";
import * as BpmnJS from "bpmn-js/dist/bpmn-navigated-viewer.production.min.js";


/**
 * This directive takes a bpmn diagram as xml input and attaches it to the dom node.
 */

@Directive({
    selector: "[appBpmn]"
})
export class BpmnDirective implements OnDestroy {

    private xml: string;

    private activities: string[];

    private viewer: BpmnJS;

    public constructor(public elementRef: ElementRef<HTMLCanvasElement>, public readonly ngZone: NgZone) {

    }

    @Input()
    public set bpmnDiagram(bpmnInfo: { xml: string, currentActivities: string[] }) {
        if (bpmnInfo?.xml == null) {
            return;
        }

        this.xml = bpmnInfo.xml;
        this.activities = bpmnInfo.currentActivities;

        this.ngZone.runOutsideAngular(() => this.import());
    }

    public ngOnDestroy() {
        this.ngZone.runOutsideAngular(() => this.destroy());
    }

    private import() {
        if (this.viewer == null) {
            this.viewer = new BpmnJS();
            this.viewer.attachTo(this.elementRef.nativeElement);
            this.viewer.on("import.done", () => this.onImportDone());
        }
        this.viewer.importXML(this.xml);
    }

    private destroy() {
        if (this.viewer != null) {
            this.viewer.destroy();
            this.viewer = null;
        }
    }

    private onImportDone() {
        const canvas = this.viewer.get("canvas");
        canvas.zoom("fit-viewport");
        if (Array.isArray(this.activities)) {
            this.activities.forEach((activity) => {
                try {
                    canvas.addMarker(activity, "highlight-bpmn");
                } catch (e) {
                    return;
                }
            });
        }
    }

}
