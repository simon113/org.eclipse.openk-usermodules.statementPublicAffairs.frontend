/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {action} from "@storybook/addon-actions";
import {text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../core";
import {AppNavigationFrameModule} from "../../app-navigation-frame.module";

const template = `
    <div style="width: 100%; display: flex; justify-content: center; padding: 1em;">
        <app-nav-drop-down (appLogOut)="appLogOut($event)">
            {{ngContent}}
        </app-nav-drop-down>
    </div>
`;

storiesOf("Features / 01 Navigation", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            I18nModule,
            AppNavigationFrameModule
        ]
    }))
    .add("NavDropDownComponent", () => ({
        template,
        props: {
            ngContent: text("ngContent", "Hugo Haferkamp"),
            appLogOut: action("appLogOut")
        }
    }));
