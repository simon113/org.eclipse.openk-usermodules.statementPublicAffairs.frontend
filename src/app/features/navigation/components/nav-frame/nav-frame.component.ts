/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {EAPIUserRoles} from "../../../../core/api/core";
import {EExitCode} from "../../../../store";

@Component({
    selector: "app-nav-frame",
    templateUrl: "./nav-frame.component.html",
    styleUrls: ["./nav-frame.component.scss"]
})
export class NavFrameComponent {

    @Input()
    public appCurrentUrl: string;

    @Input()
    public appExitCode: EExitCode;

    @Input()
    public appIsLoading = false;

    @Input()
    public appMenuHidden = false;

    @Input()
    public appPortalRoute: string;

    @Input()
    public appUserName: string;

    @Input()
    public appUserRoles: EAPIUserRoles[];

    @Input()
    public appVersion: string;

    @Output()
    public readonly appCloseWindow = new EventEmitter<void>();

    @Output()
    public readonly appLogout = new EventEmitter<void>();

}
