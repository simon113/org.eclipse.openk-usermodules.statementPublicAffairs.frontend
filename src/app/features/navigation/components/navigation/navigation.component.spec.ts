/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {StoreModule} from "@ngrx/store";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {MessageService} from "primeng/api";
import {ToastModule} from "primeng/toast";
import {I18nModule, WINDOW} from "../../../../core";
import {logOutAction} from "../../../../store";
import {AppNavigationFrameModule} from "../../app-navigation-frame.module";
import {NavigationComponent} from "./navigation.component";

class WindowMock {
    public opener: any;

    public close() {
    }
}

describe("NavigationComponent", () => {
    let component: NavigationComponent;
    let fixture: ComponentFixture<NavigationComponent>;
    let store: MockStore;
    const initialState = {};

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                AppNavigationFrameModule,
                I18nModule,
                RouterTestingModule,
                StoreModule,
                ToastModule
            ],
            providers:
                [
                    provideMockStore({initialState}),
                    {provide: WINDOW, useClass: WindowMock},
                    MessageService
                ],
        }).compileComponents();
    });
    beforeEach(() => {
        fixture = TestBed.createComponent(NavigationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        store = TestBed.inject(MockStore);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should try to close window if opener is set", () => {
        spyOn(component.window, "close");
        component.window.opener = "any value";
        component.closeWindow();
        expect(component.window.close).toHaveBeenCalled();
    });

    it("should not close the window when opener is not set", () => {
        spyOn(component.window, "close");
        component.window.opener = null;
        component.closeWindow();
        expect(component.window.close).not.toHaveBeenCalled();
    });

    it("should dispatch the logout action", () => {
        const expectedAction = logOutAction();
        spyOn(store, "dispatch");
        component.logOut();
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction);
    });
});
