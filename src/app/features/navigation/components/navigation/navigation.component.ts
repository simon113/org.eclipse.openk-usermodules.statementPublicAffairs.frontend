/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Inject} from "@angular/core";
import {Router} from "@angular/router";
import {select, Store} from "@ngrx/store";
import {PORTAL_ROUTE, WINDOW} from "../../../../core";
import {
    exitCodeSelector,
    isLoadingSelector,
    isLoggedInSelector,
    logOutAction,
    overallVersionSelector,
    userFullNameSelector,
    userRolesSelector
} from "../../../../store";

@Component({
    selector: "app-navigation",
    templateUrl: "./navigation.component.html",
    styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent {

    public exitCode$ = this.store.pipe(select(exitCodeSelector));

    public isLoading$ = this.store.pipe(select(isLoadingSelector));

    public isLoggedIn$ = this.store.pipe(select(isLoggedInSelector));

    public userName$ = this.store.pipe(select(userFullNameSelector));

    public userRoles$ = this.store.pipe(select(userRolesSelector));

    public version$ = this.store.pipe(select(overallVersionSelector));

    public constructor(
        public readonly store: Store,
        public readonly router: Router,
        @Inject(WINDOW) public readonly window: Window,
        @Inject(PORTAL_ROUTE) public readonly appPortalRoute: string
    ) {

    }

    public logOut() {
        this.store.dispatch(logOutAction());
    }

    public closeWindow() {
        try {
            if (this.window.opener != null) {
                this.window.close();
            }
        } catch (e) {
            return;
        }
    }

}
