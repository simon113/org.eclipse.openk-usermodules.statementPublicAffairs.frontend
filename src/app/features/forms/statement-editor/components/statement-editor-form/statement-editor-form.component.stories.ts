/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockStore} from "@ngrx/store/testing";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../../core/i18n";
import {getStatementTextConfigurationSelector, statementArrangementSelector} from "../../../../../store";
import {createStatementTextConfigurationMock} from "../../../../../test";
import {StatementEditorModule} from "../../statement-editor.module";
import {StatementEditorFormComponent} from "./statement-editor-form.component";

const configuration = createStatementTextConfigurationMock();

const arrangement = [];

storiesOf("Features / Forms", module)
    .addDecorator(moduleMetadata({
        imports: [
            I18nModule,
            RouterTestingModule,
            StatementEditorModule,
            BrowserAnimationsModule
        ],
        providers: [
            provideMockStore({
                selectors: [
                    {
                        selector: getStatementTextConfigurationSelector,
                        value: configuration
                    },
                    {
                        selector: statementArrangementSelector,
                        value: arrangement
                    }
                ]
            })
        ]
    }))
    .add("StatementEditorFormComponent", () => ({
        template: `
            <app-statement-editor-form style="padding: 1em; box-sizing: border-box">
            </app-statement-editor-form>
        `,
        props: {}
    }));
