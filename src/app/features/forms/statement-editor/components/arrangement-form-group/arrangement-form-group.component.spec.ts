/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkDragDrop} from "@angular/cdk/drag-drop";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {FormArray} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../../core/i18n";
import {IExtendedTextBlockModel} from "../../../../../shared/text-block/model";
import {StatementEditorModule} from "../../statement-editor.module";
import {ArrangementFormGroupComponent} from "./arrangement-form-group.component";

describe("ArrangementFormGroupComponent", () => {

    let component: ArrangementFormGroupComponent;
    let fixture: ComponentFixture<ArrangementFormGroupComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementEditorModule,
                BrowserAnimationsModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArrangementFormGroupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should add a text arrangement model from a blockmodel", () => {
        const textBlock: IExtendedTextBlockModel = {
            id: "id",
            text: "text",
            excludes: [],
            requires: []
        };
        const formArray: FormArray = component.getFormArray();
        expect(formArray.length).toEqual(0);

        component.addTextBlock(textBlock);
        expect(formArray.length).toEqual(1);
        const firstControl = component.getFormArrayControl(0);
        expect(firstControl.value.textblockId).toEqual(textBlock.id);

        component.addTextBlock(undefined);
        expect(formArray.length).toEqual(1);

        component.addTextBlock({...textBlock, type: "newline"});
        const secondControl = component.getFormArrayControl(1);
        expect(secondControl.value.textblockId).toEqual(undefined);
    });

    it("should call move when dropped inside same container, otherwise add", () => {
        const moveSpy = spyOn(component, "moveControl");
        const addTextBlockSpy = spyOn(component, "addTextBlock");
        const container = {};
        const event: CdkDragDrop<any> = {
            previousContainer: container,
            container,
            currentIndex: 0,
            previousIndex: 1,
            item: {
                data: {}
            }
        } as CdkDragDrop<any>;

        component.drop(event);
        expect(moveSpy).toHaveBeenCalledWith(1, 0);

        const eventDifferentContainers: CdkDragDrop<any> = {...event, container: {}} as CdkDragDrop<any>;
        component.drop(eventDifferentContainers);
        expect(addTextBlockSpy).toHaveBeenCalledWith(eventDifferentContainers.item.data, 0);
    });
});
