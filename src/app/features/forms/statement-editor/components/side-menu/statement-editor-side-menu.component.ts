/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from "@angular/core";
import {EAPIProcessTaskDefinitionKey, EAPIUserRoles, IAPIProcessTask, TCompleteTaskVariable} from "../../../../../core";
import {arrayJoin} from "../../../../../util/store";

export interface IStatementEditorSideMenuActionButton<T = any> {

    emit: () => void;

    label?: string;

    icon?: string;

    cssClass?: string;

}

interface ITaskUserLayoutMap<T> {

    [taskKey: string]: {
        [role: string]: T[]
    };

}

@Component({
    selector: "app-statement-editor-side-menu",
    templateUrl: "./statement-editor-side-menu.component.html",
    styleUrls: ["./statement-editor-side-menu.component.scss"]
})
export class StatementEditorSideMenuComponent implements OnChanges {

    @Input()
    public appErrorMessage: string;

    @Input()
    public appForFinalization: boolean;

    @Input()
    public appLoading: boolean;

    @Input()
    public appUserRoles: EAPIUserRoles[];

    @Input()
    public appTask: IAPIProcessTask;


    @Output()
    public appSave = new EventEmitter<void>();

    @Output()
    public appValidate = new EventEmitter<void>();

    @Output()
    public appCompile = new EventEmitter<void>();


    @Output()
    public appSaveAndCompleteTask = new EventEmitter<{
        variables: TCompleteTaskVariable,
        claimNext?: boolean | EAPIProcessTaskDefinitionKey
    }>();

    @Output()
    public appSaveAndContribute = new EventEmitter<void>();

    @Output()
    public appSaveAndFinalize = new EventEmitter<void>();

    @Output()
    public appCompleteFinalization = new EventEmitter<boolean>();


    public buttonLayout: IStatementEditorSideMenuActionButton[] = [];

    public buttonLayoutForFinalization: IStatementEditorSideMenuActionButton[] = [
        {
            emit: emitFactory(this.appCompleteFinalization, false),
            label: "statementEditorForm.sideMenu.continue",
            icon: "edit",
            cssClass: "openk-info"
        },
        {
            emit: emitFactory(this.appCompleteFinalization, true),
            label: "statementEditorForm.sideMenu.releaseForApproval",
            icon: "redo",
            cssClass: "openk-success"
        }
    ];

    private userTaskLayoutMap: ITaskUserLayoutMap<IStatementEditorSideMenuActionButton> = {
        [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE]: {
            [EAPIProcessTaskDefinitionKey.CREATE_DRAFT]: [
                {
                    emit: emitFactory(this.appSaveAndCompleteTask, {variables: {}}),
                    label: "statementEditorForm.sideMenu.releaseForDepartments",
                    icon: "redo",
                    cssClass: "openk-success"
                }
            ],
            [EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE]: [
                {
                    emit: emitFactory(this.appSaveAndCompleteTask, {
                        variables: {
                            data_complete: {type: "Boolean", value: true},
                            response_created: {type: "Boolean", value: false}
                        },
                        claimNext: true
                    }),
                    label: "statementEditorForm.sideMenu.backToInfoData",
                    icon: "subject",
                    cssClass: "openk-danger"
                },
                {
                    emit: emitFactory(this.appSaveAndCompleteTask, {
                        variables: {
                            data_complete: {type: "Boolean", value: false},
                            response_created: {type: "Boolean", value: false}
                        }
                    }),
                    label: "statementEditorForm.sideMenu.backToDepartments",
                    icon: "undo",
                    cssClass: "openk-danger"
                },
                {
                    emit: emitFactory(this.appSaveAndFinalize),
                    label: "statementEditorForm.sideMenu.finalize",
                    icon: "launch",
                    cssClass: "openk-success"
                }
            ],
            [EAPIProcessTaskDefinitionKey.CREATE_NEGATIVE_RESPONSE]: [
                {
                    emit: emitFactory(this.appSaveAndCompleteTask, {
                        variables: {
                            response_created: {type: "Boolean", value: false}
                        },
                        claimNext: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA
                    }),
                    label: "statementEditorForm.sideMenu.backToInfoData",
                    icon: "subject",
                    cssClass: "openk-danger"
                },
                {
                    emit: emitFactory(this.appSaveAndFinalize),
                    label: "statementEditorForm.sideMenu.finalize",
                    icon: "launch",
                    cssClass: "openk-success"
                }
            ]
        },
        [EAPIUserRoles.DIVISION_MEMBER]: {
            [EAPIProcessTaskDefinitionKey.ENRICH_DRAFT]: [
                {
                    emit: emitFactory(this.appSaveAndContribute),
                    label: "statementEditorForm.sideMenu.contribute",
                    icon: "check",
                    cssClass: "openk-success"
                }
            ]
        }
    };

    public ngOnChanges(changes: SimpleChanges) {
        const keys: Array<keyof StatementEditorSideMenuComponent> = ["appTask", "appUserRoles"];
        if (keys.some((_) => changes[_] != null)) {
            this.updateActions();
        }
    }

    public updateActions() {
        const taskDefinitionKey = this.appTask?.taskDefinitionKey;
        this.buttonLayout = arrayJoin(...this.appUserRoles
            .map((_) => this.userTaskLayoutMap[_] == null ? [] : this.userTaskLayoutMap[_][taskDefinitionKey])
        );
    }

}

function emitFactory(ev: EventEmitter<void>): () => void;
function emitFactory<T>(ev: EventEmitter<T>, value: T): () => void;
function emitFactory(eventEmitter: EventEmitter<any>, value?: any): () => void {
    return () => eventEmitter.emit(value);
}
