/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {StatementEditorModule} from "../../statement-editor.module";
import {StatementPreviewComponent} from "./statement-preview.component";

describe("StatementPreviewComponent", () => {
    let component: StatementPreviewComponent;
    let fixture: ComponentFixture<StatementPreviewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementEditorModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementPreviewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
