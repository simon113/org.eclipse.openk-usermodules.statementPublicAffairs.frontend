/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPITextArrangementItemModel} from "../../../../core/api/text";

@Pipe({
    name: "getTitle"
})
export class GetTitlePipe implements PipeTransform {

    public transform(arrangement: IAPITextArrangementItemModel) {
        switch (arrangement?.type) {
            case "text":
                return "textBlocks.standardBlocks.freeText";
            case "newline":
                return "textBlocks.standardBlocks.newLine";
            case "pagebreak":
                return "textBlocks.standardBlocks.pagebreak";
        }

        return arrangement?.textblockId;
    }

}
