/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPITextArrangementErrorModel} from "../../../../core/api/text";
import {arrayJoin} from "../../../../util/store";

/**
 * For the given error model returns the error message for the most important error.
 */

@Pipe({
    name: "errorToMessages"
})
export class ErrorToMessagesPipe implements PipeTransform {

    public transform(error: IAPITextArrangementErrorModel): ITextblockError[] {
        const errorMessages: ITextblockError[] = [];

        if (!error) {
            return [];
        }

        if (error.after != null) {
            errorMessages.push({
                message: "textBlocks.errors.after",
                ids: [error.after]
            });
        }
        if (arrayJoin(error.excludes).length > 0) {
            errorMessages.push({
                message: "textBlocks.errors.excludes",
                ids: error.excludes
            });
        }

        arrayJoin(error.requires)
            .filter((rule) => rule != null)
            .forEach((rule) => {
                const ids = arrayJoin(rule.ids);
                switch (rule.type) {
                    case "and":
                        return errorMessages.push({
                            message: "textBlocks.errors.requires",
                            ids
                        });
                    case "or":
                        return errorMessages.push({
                            message: "textBlocks.errors.requiresAtleastOne",
                            ids
                        });
                    case "xor":
                        return errorMessages.push({
                            message: "textBlocks.errors.requiresOne",
                            ids
                        });
                }
            });

        if (arrayJoin(error.missingVariables).length > 0) {
            errorMessages.push({
                message: "textBlocks.errors.missingVariables",
                ids: error.missingVariables
            });
        }

        return errorMessages;
    }

}

export interface ITextblockError {
    message: string;
    ids: string[];
}
