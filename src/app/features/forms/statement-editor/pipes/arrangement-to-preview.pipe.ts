/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {ITextBlockRenderItem} from "../../../../shared/text-block/model/ITextBlockRenderItem";
import {replaceTags} from "../../../../shared/text-block/pipes/get-blockdata-array";
import {IStatementEditorControlConfiguration} from "../../../../store/statements/model";
import {arrayJoin} from "../../../../util/store";

/**
 * Converts arrangement configuration to text. Integrates the current values set for the select/input/date replacements into the textblocks
 * texts to then be able to display them as text.
 */

@Pipe({
    name: "arrangementToPreview"
})
export class ArrangementToPreviewPipe implements PipeTransform {

    public transform(config: IStatementEditorControlConfiguration[]): ITextBlockRenderItem[] {

        const result: ITextBlockRenderItem[] = [];
        for (const item of arrayJoin(config)) {
            if (item.value?.replacement) {
                result.push({
                    type: "text",
                    value: item.value.replacement
                });
            } else if (item.value?.type === "block" && item.textBlock != null) {
                result.push({
                    type: "text",
                    value: this.replaceInText(item.textBlock.text, item.value?.placeholderValues, item.replacements, item.selects)
                });
            } else if (item.value?.type === "newline" || item.value?.type === "pagebreak") {
                result.push({
                    type: item.value.type,
                    value: ""
                });
            }
        }

        return replaceTags(result, [
            {separator: /(\n)/, type: "newline"}
        ]);
    }

    public replaceInText(text: string,
                         placeholderValues: { [key: string]: string } = {},
                         replacements: { [key: string]: string },
                         selects: { [key: string]: string[] }): string {
        let arrangementText: string = text;
        let textToReplace: string[] = arrangementText.match(/<([fdts]):([0-9a-zA-Z_\\-]+)>/);

        while (textToReplace != null) {

            let replacementValue: string = textToReplace[1] === "t" ? replacements[textToReplace[2]] : placeholderValues[textToReplace[0]];
            replacementValue = textToReplace[1] === "s" ? selects[textToReplace[2]][replacementValue] : replacementValue;
            replacementValue = replacementValue ? replacementValue : "______";

            arrangementText = arrangementText.replace(textToReplace[0], replacementValue);

            textToReplace = arrangementText.match(/<([fdts]):([0-9a-zA-Z_\\-]+)>/);
        }
        return arrangementText;
    }

}
