/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {combineLatest, Observable} from "rxjs";
import {delay, filter, take, takeUntil} from "rxjs/operators";
import {AUTO_SELECTED_TAGS, IAPIAttachmentModel} from "../../../../core/api/attachments";
import {IAPIEmailAttachmentModel, IAPIEmailModel} from "../../../../core/api/mail";
import {
    clearAttachmentCacheAction,
    createAttachmentForm,
    fetchAttachmentTagsAction,
    getAllStatementAttachments,
    getAttachmentControlValueSelector,
    getFilteredAttachmentTagsSelector,
    getStatementAttachmentCacheSelector,
    IAttachmentControlValue,
    IAttachmentFormValue,
    queryParamsIdSelector,
    startAttachmentDownloadAction
} from "../../../../store";
import {downloadEmailAttachmentAction} from "../../../../store/mail/actions";
import {getSelectedEmailSelector, getStatementMailSelector} from "../../../../store/mail/selectors";
import {arrayJoin} from "../../../../util/store";
import {AbstractReactiveFormComponent} from "../../abstract";
import {getMailAttachment, getMailAttachments} from "../util/mail-attachments.util";

/**
 * This form component shows the list of statement attachments. The attachments are split into two categories.
 * Mail attachments are displayed separately from the normal attachments.
 * New attachments can be uploaded via the file drop component. Displayed attachments can be selected and downloaded.
 */

@Component({
    selector: "app-attachments-form-group",
    templateUrl: "./attachments-form-group.component.html",
    styleUrls: ["./attachments-form-group.component.scss"]
})
export class AttachmentsFormGroupComponent
    extends AbstractReactiveFormComponent<IAttachmentFormValue>
    implements OnInit, OnDestroy {

    @Input()
    public appAutoTagIds: string[];

    @Input()
    public appForbiddenTagIds: string[] = [];

    @Input()
    public appRestrictedTagIds: string[] = [];

    @Input()
    public appWithoutTagControl: boolean;

    @Input()
    public appCollapsed: boolean;

    @Input()
    public appFormGroup = createAttachmentForm();

    @Input()
    public appForNewStatement: boolean;

    @Input()
    public appMailId: string;

    @Input()
    public appDisabled: boolean;

    public mail: IAPIEmailModel;

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public selectedMail$ = this.store.pipe(select(getSelectedEmailSelector));

    public statementMail$ = this.store.pipe(select(getStatementMailSelector));

    public attachments$: Observable<IAttachmentControlValue[]>;

    public allAttachments$ = this.store.pipe(select(getAllStatementAttachments));

    public fileCache$ = this.store.pipe(select(getStatementAttachmentCacheSelector));

    public tagList$ = this.store.pipe(select(getFilteredAttachmentTagsSelector, {without: AUTO_SELECTED_TAGS}));

    public constructor(public store: Store) {
        super();
    }

    public ngOnInit() {

        this.attachments$ = this.store.pipe(
            select(getAttachmentControlValueSelector,
                {forbiddenTagIds: this.appForbiddenTagIds, restrictedTagIds: this.appRestrictedTagIds})
        );

        this.attachments$.pipe(delay(0), takeUntil(this.destroy$))
            .subscribe((values) => this.setValueForArray(values, "edit"));

        /**
         * Reacts to changes on email and attachment data and sets the list of email attachments accordingly.
         */
        combineLatest([this.selectedMail$, this.statementMail$, this.allAttachments$]).pipe(
            filter(([_, m, a]) => (_ != null || m != null) && a != null),
            delay(0),
            takeUntil(this.destroy$)
        ).subscribe(async ([selectedMail, statementMail, attachments]) => {
            this.setMailTextAttachmentValue(attachments, this.appForNewStatement);
            this.mail = selectedMail ? selectedMail : statementMail;
            this.setMailAttachmentValues(attachments, arrayJoin(this.mail?.attachments));
        });

        this.fileCache$.pipe(delay(0), takeUntil(this.destroy$))
            .subscribe((values) => this.setValueForArray(values, "add"));
        this.store.dispatch(fetchAttachmentTagsAction());
    }

    public ngOnDestroy() {
        this.statementId$.pipe(take(1))
            .subscribe((statementId) => this.store.dispatch(clearAttachmentCacheAction({statementId})));
        super.ngOnDestroy();
    }

    public async downloadAttachment(attachmentId: number) {
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        this.store.dispatch(startAttachmentDownloadAction({statementId, attachmentId}));
    }

    public async downloadEmailAttachment(attachmentId: string | number) {
        if (typeof (attachmentId) === "number") {
            this.downloadAttachment(attachmentId);
        } else {
            this.store.dispatch(downloadEmailAttachmentAction({mailId: this.appMailId, name: attachmentId}));
        }
    }

    public setMailTextAttachmentValue(attachments: IAPIAttachmentModel[], isNewStatement: boolean) {
        const mailTextAttachmentId = getMailAttachment(attachments)?.id;

        this.appFormGroup.patchValue({mailTextAttachmentId, transferMailText: mailTextAttachmentId != null || isNewStatement});
    }

    public setMailAttachmentValues(attachments: IAPIAttachmentModel[], emailAttachments: IAPIEmailAttachmentModel[]) {
        const mergedAttachmentLists = getMailAttachments(attachments, emailAttachments, this.appForNewStatement);
        this.setValueForArray(mergedAttachmentLists, "email");
    }

}
