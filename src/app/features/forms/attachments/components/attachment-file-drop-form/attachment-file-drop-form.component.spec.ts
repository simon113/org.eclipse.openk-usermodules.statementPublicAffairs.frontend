/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {I18nModule} from "../../../../../core/i18n";
import {AttachmentsFormModule} from "../../attachments-form.module";
import {AttachmentFileDropFormComponent} from "./attachment-file-drop-form.component";

describe("AttachmentFileDropFormComponent", () => {
    let component: AttachmentFileDropFormComponent;
    let fixture: ComponentFixture<AttachmentFileDropFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                I18nModule,
                AttachmentsFormModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AttachmentFileDropFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should add user controls for dropped files", async () => {
        const files = [
            new File(["A"], "FileA.txt"),
            new File(["B"], "FileB.txt"),
            new File(["C"], "FileC.txt")
        ];
        const tagIds = ["X", "Y"];

        component.appAutoTagIds = tagIds;
        component.addControlForFiles(files);

        fixture.detectChanges();
        await fixture.whenStable();

        expect(component.getFormArray().length).toBe(files.length);

        files.forEach((_, i) => {
            expect(component.getValue()[i]).toEqual({name: _.name, file: _, tagIds});
        });
    });

});
