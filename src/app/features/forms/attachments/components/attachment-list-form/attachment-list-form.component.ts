/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {IAPIAttachmentTag} from "../../../../../core";
import {createAttachmentForm, IAttachmentControlValue, IAttachmentFormValue} from "../../../../../store";
import {AbstractReactiveFormArrayComponent} from "../../../abstract";

@Component({
    selector: "app-attachment-list-form",
    templateUrl: "./attachment-list-form.component.html",
    styleUrls: ["./attachment-list-form.component.scss"]
})
export class AttachmentListFormComponent extends AbstractReactiveFormArrayComponent<IAttachmentControlValue> {

    @Input()
    public appFormGroup = createAttachmentForm();

    @Input()
    public appTagList: IAPIAttachmentTag[];

    @Input()
    public appFormArrayName: keyof IAttachmentFormValue = "edit";

    @Input()
    public appTitle: string;

    @Input()
    public appHideNotUsedTags = true;

    @Output()
    public appDownloadAttachment = new EventEmitter<number | string>();

}


