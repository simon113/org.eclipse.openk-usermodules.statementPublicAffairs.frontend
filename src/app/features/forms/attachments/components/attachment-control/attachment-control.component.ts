/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {IAPIAttachmentTag} from "../../../../../core";
import {AbstractControlValueAccessorComponent} from "../../../../../shared/controls/common";
import {IAttachmentControlValue} from "../../../../../store";
import {arrayJoin, filterDistinctValues} from "../../../../../util/store";

@Component({
    selector: "app-attachment-control",
    templateUrl: "./attachment-control.component.html",
    styleUrls: ["./attachment-control.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AttachmentControlComponent),
            multi: true
        }
    ]
})
export class AttachmentControlComponent extends AbstractControlValueAccessorComponent<IAttachmentControlValue> {

    private static id = 0;

    @Input()
    public appId = `AttachmentControlComponent-${AttachmentControlComponent.id++}`;

    @Input()
    public appTagList: IAPIAttachmentTag[];

    @Input()
    public appIsSelectable: boolean;

    @Input()
    public appIsCancelable: boolean;

    @Input()
    public appIsDownloadable: boolean;

    @Output()
    public appCancel = new EventEmitter<void>();

    @Output()
    public appDownloadAttachment = new EventEmitter<number | string>();

    @Input()
    public appHideNotUsedTags: boolean;

    @Input()
    public appEditableTags = true;

    public toggleSelection(isSelected: boolean) {
        if (this.appDisabled) {
            return;
        }
        this.writeValue({...this.appValue, isSelected}, true);
    }

    public toggleTag(tagId: string, isRemoved: boolean) {
        if (this.appDisabled) {
            return;
        }

        const tagIds = isRemoved ?
            this.getSelectedTagIds().filter((_) => _ !== tagId) :
            filterDistinctValues(arrayJoin(this.getSelectedTagIds(), [tagId])).sort();

        this.writeValue({...this.appValue, tagIds}, true);
    }

    public getSelectedTagIds(): string[] {
        return arrayJoin(this.appValue?.tagIds);
    }

}
