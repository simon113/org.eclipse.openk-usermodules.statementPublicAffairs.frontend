/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {I18nModule} from "../../../../../core/i18n";
import {AttachmentsFormModule} from "../../attachments-form.module";
import {AttachmentDisplayListComponent} from "./attachment-display-list.component";

describe("AttachmentDisplayListComponent", () => {
    let component: AttachmentDisplayListComponent;
    let fixture: ComponentFixture<AttachmentDisplayListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                I18nModule,
                AttachmentsFormModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AttachmentDisplayListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
