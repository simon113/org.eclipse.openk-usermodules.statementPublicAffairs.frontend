/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {ActionButtonModule} from "../../../shared/layout/action-button";
import {CollapsibleModule} from "../../../shared/layout/collapsible";
import {FileDropModule} from "../../../shared/layout/file-drop";
import {SharedPipesModule} from "../../../shared/pipes";
import {AttachmentControlComponent, AttachmentListFormComponent, AttachmentsFormGroupComponent} from "./components";
import {AttachmentDisplayListComponent} from "./components/attachment-display";
import {AttachmentFileDropFormComponent} from "./components/attachment-file-drop-form";
import {GetEmailTextAttachmentPipe} from "./pipes/get-email-text-attachment.pipe";
import {GetMailAttachmentsPipe} from "./pipes/get-mail-attachments.pipe";


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatIconModule,
        TranslateModule,
        CollapsibleModule,
        FileDropModule,
        SharedPipesModule,
        FormsModule,
        ActionButtonModule,
    ],
    declarations: [
        AttachmentsFormGroupComponent,
        AttachmentControlComponent,
        AttachmentFileDropFormComponent,
        AttachmentListFormComponent,
        GetEmailTextAttachmentPipe,
        AttachmentDisplayListComponent,
        GetMailAttachmentsPipe
    ],
    exports: [
        AttachmentsFormGroupComponent,
        AttachmentControlComponent,
        AttachmentFileDropFormComponent,
        AttachmentListFormComponent,
        AttachmentDisplayListComponent,
        GetEmailTextAttachmentPipe,
        GetMailAttachmentsPipe
    ]
})
export class AttachmentsFormModule {

}
