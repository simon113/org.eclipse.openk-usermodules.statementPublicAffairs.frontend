/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {take} from "rxjs/operators";
import {ConfirmService, I18nModule} from "../../../../../core";
import {addCommentAction, deleteCommentAction, queryParamsIdSelector, statementCommentsSelector} from "../../../../../store";
import {CommentsFormModule} from "../../comments-form.module";
import {CommentsFormComponent} from "./comments-form.component";

describe("CommentsFormComponent", () => {
    let store: Store;
    let component: CommentsFormComponent;
    let fixture: ComponentFixture<CommentsFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommentsFormModule,
                I18nModule
            ],
            providers: [
                provideMockStore({
                    initialState: {},
                    selectors: [
                        {
                            selector: queryParamsIdSelector,
                            value: 19
                        },
                        {
                            selector: statementCommentsSelector,
                            value: [{id: 1919} as any]
                        }
                    ]
                }),
                {provide: ConfirmService, useValue: {askForConfirmation: (message: string) => true}}
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CommentsFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        store = fixture.componentRef.injector.get(Store);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should dispatch add comment actions", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.addComment("Comment");
        expect(dispatchSpy).toHaveBeenCalledWith(addCommentAction({statementId: 19, text: "Comment"}));
    });

    it("should dispatch delete comment actions", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.deleteComment(1919);
        expect(dispatchSpy).toHaveBeenCalledWith(deleteCommentAction({statementId: 19, commentId: 1919}));
    });

    it("should observe the correct number of comments", async () => {
        const numberOfComments = await component.numberOfComments$.pipe(take(1)).toPromise();
        expect(numberOfComments).toBe(1);
    });

});
