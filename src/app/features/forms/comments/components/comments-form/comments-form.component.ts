/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {TranslateService} from "@ngx-translate/core";
import {defer} from "rxjs";
import {map, take} from "rxjs/operators";
import {ConfirmService} from "../../../../../core";
import {addCommentAction, deleteCommentAction, queryParamsIdSelector, statementCommentsSelector} from "../../../../../store";
import {arrayJoin} from "../../../../../util";


/**
 * This component displays all saved comments to the statement.
 * Also makes it possible to input text for a new comment in a text field. New comment can be added to the statement.
 * Comments can be deleted by the user that created the comment.
 */

@Component({
    selector: "app-comments-form",
    templateUrl: "./comments-form.component.html",
    styleUrls: ["./comments-form.component.scss"]
})
export class CommentsFormComponent {

    @Input()
    public appCollapsed = false;

    @Input()
    public appCommentsToShow = 5;

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public comments$ = this.store.pipe(select(statementCommentsSelector));

    public numberOfComments$ = defer(() => this.comments$).pipe(
        map((comments) => arrayJoin(comments).length)
    );

    public constructor(
        public store: Store,
        public confirmService: ConfirmService,
        private translationService: TranslateService) {

    }

    public async addComment(text: string) {
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        this.store.dispatch(addCommentAction({statementId, text}));
    }

    public async deleteComment(commentId: number) {
        const confirmationText = await this.translationService.get("comments.confirmDelete").toPromise();
        if (this.confirmService.askForConfirmation(confirmationText)) {
            const statementId = await this.statementId$.pipe(take(1)).toPromise();
            this.store.dispatch(deleteCommentAction({statementId, commentId}));
        }
    }

}
