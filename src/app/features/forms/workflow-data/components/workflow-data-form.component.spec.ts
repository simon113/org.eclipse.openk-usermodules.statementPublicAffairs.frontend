/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {FormGroup} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule, IAPISearchOptions} from "../../../../core";
import {
    IWorkflowFormValue,
    openGisAction,
    startStatementSearchAction,
    submitWorkflowDataFormAction,
    taskSelector,
    userNameSelector
} from "../../../../store";
import {ILeafletBounds} from "../../../map";
import {WorkflowDataFormModule} from "../workflow-data-form.module";
import {WorkflowDataFormComponent} from "./workflow-data-form.component";

describe("WorkflowDataFormComponent", () => {

    const user = "userName";

    const initialState = {
        statements: {},
        process: {},
        settings: {}
    };

    let store: MockStore;
    let component: WorkflowDataFormComponent;
    let fixture: ComponentFixture<WorkflowDataFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                WorkflowDataFormModule,
                I18nModule,
                RouterTestingModule
            ],
            providers: [
                provideMockStore({
                    initialState,
                    selectors: [{selector: userNameSelector, value: user}]
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WorkflowDataFormComponent);
        store = fixture.componentRef.injector.get(MockStore);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeDefined();
    });

    it("should dispatch submit workflow form action", async () => {
        store.overrideSelector(taskSelector, {statementId: 1, taskId: "19"} as any);
        const dispatchSpy = spyOn(store, "dispatch");
        const value: IWorkflowFormValue = {
            geographicPosition: "1919",
            departments: {
                selected: [],
                indeterminate: []
            },
            parentIds: [19, 199]
        };
        const formMock = {value} as FormGroup;
        const action = submitWorkflowDataFormAction({
            statementId: 1,
            taskId: "19",
            data: value,
            completeTask: true
        });

        component.appFormGroup = formMock;

        await component.submit(true);
        expect(dispatchSpy).toHaveBeenCalledWith(action);

        action.completeTask = false;
        await component.submit(false);
        expect(dispatchSpy).toHaveBeenCalledWith(action);
    });

    it("should dispatch search statements action", () => {
        const dispatchSpy = spyOn(store, "dispatch");
        const options: IAPISearchOptions = {q: ""};
        component.search(options);
        expect(dispatchSpy).toHaveBeenCalledWith(startStatementSearchAction({options}));
    });

    it("should open GIS", () => {
        spyOn(store, "dispatch");
        const bounds = {} as ILeafletBounds;
        component.openGis(bounds);
        expect(store.dispatch).toHaveBeenCalledWith(openGisAction({bounds, user}));
    });

});
