/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Input, OnDestroy, Output} from "@angular/core";
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {defer, merge, of, Subject} from "rxjs";
import {map} from "rxjs/operators";
import {arrayJoin} from "../../../util/store";

export abstract class AbstractReactiveFormComponent<T extends object> implements OnDestroy {

    @Input()
    public abstract appFormGroup: FormGroup;

    @Output()
    public appValueChange = defer(() => this.appFormGroup.valueChanges).pipe(
        map(() => this.getValue())
    );

    protected value$ = defer(() => merge(of(this.getValue()), this.appValueChange));

    protected destroy$ = new Subject();

    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public getValue(): T;
    public getValue<TKey extends keyof T & string>(key: TKey): T[TKey];
    public getValue(key?: string) {
        return (typeof key !== "string" ? this.appFormGroup : this.appFormGroup?.get(key))?.value;
    }

    public patchValue(value: Partial<T>) {
        this.appFormGroup.patchValue(value);
    }

    public setValueForArray<TKey extends keyof T & string>(values: T[TKey] & any[], key: TKey) {
        const array = this.appFormGroup.get(key) as FormArray;
        array.clear();
        arrayJoin(values)
            .forEach((_) => array.push(this.createControl(_, key)));
    }

    public createControl(value, path?: string | Array<string | number>) {
        const control = new FormControl(value);
        const group = path == null ? this.appFormGroup : this.appFormGroup.get(path);
        if (group?.disabled) {
            control.disable();
        }
        return control;
    }

    public disable(disable: boolean) {
        if (disable && this.appFormGroup.enabled) {
            this.appFormGroup.disable();
        }
        if (!disable && this.appFormGroup.disabled) {
            this.appFormGroup.enable();
        }
    }

}
