/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {FormArray, FormControl} from "@angular/forms";
import {provideMockStore} from "@ngrx/store/testing";
import {createFormGroup} from "../../../util/forms";
import {AbstractReactiveFormArrayComponent} from "./abstract-reactive-form-array.component";

@Component({templateUrl: ""})
class AbstractReactiveFormArraySpecComponent extends AbstractReactiveFormArrayComponent<string> {

    public appFormArray = new FormArray([
        new FormControl("A"),
        new FormControl("B"),
        new FormControl("C")
    ]);

    public appFormGroup = createFormGroup<any>({
        array: this.appFormArray
    });

    public appFormArrayName = "array";

}

describe("AbstractReactiveFormArrayComponent", () => {

    let component: AbstractReactiveFormArraySpecComponent;
    let fixture: ComponentFixture<AbstractReactiveFormArraySpecComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AbstractReactiveFormArraySpecComponent
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AbstractReactiveFormArraySpecComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should set and return the current form value", () => {
        expect(component.getValue()).toEqual(["A", "B", "C"]);
        component.setValue(["D", "E"]);
        expect(component.getValue()).toEqual(["D", "E"]);
    });

    it("should return the form array if present", () => {
        expect(component.getFormArray()).toBe(component.appFormArray);
        expect(component.getFormArrayControl(1)).toBe(component.appFormArray.at(1));

        component.appFormGroup = null;
        expect(component.getFormArray()).not.toBeDefined();
        expect(component.getFormArrayControl(1)).not.toBeDefined();
    });

    it("should add controls", () => {
        component.addControl("X", 1);
        expect(component.getFormArray().length).toBe(4);
        expect(component.getValue()).toEqual(["A", "X", "B", "C"]);

        component.addControl("Y");
        expect(component.getFormArray().length).toBe(5);
        expect(component.getValue()).toEqual(["A", "X", "B", "C", "Y"]);
    });

    it("should remain disabled when new controls are added", () => {
        component.appFormGroup.disable();
        component.addControl("Z");
        expect(component.appFormGroup.disabled).toBeTrue();
    });

    it("should remove controls", () => {
        component.removeControlAt(1);
        expect(component.getFormArray().length).toBe(2);
        expect(component.getValue()).toEqual(["A", "C"]);
    });

    it("should move controls", () => {
        const control = component.getFormArrayControl(1);
        component.moveControl(1, 0);
        expect(component.getFormArrayControl(0)).toBe(control);
    });

});
