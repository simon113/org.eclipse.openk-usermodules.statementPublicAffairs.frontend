/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {provideMockStore} from "@ngrx/store/testing";
import {createFormGroup} from "../../../util/forms";
import {AbstractReactiveFormComponent} from "./abstract-reactive-form.component";

@Component({templateUrl: ""})
class AbstractReactiveFormSpecComponent extends AbstractReactiveFormComponent<{ field: string, array: number[] }> {

    public appFormGroup = createFormGroup({
        field: new FormControl(""),
        array: new FormArray([])
    });

}

describe("AbstractReactiveFormComponent", () => {

    let component: AbstractReactiveFormSpecComponent;
    let fixture: ComponentFixture<AbstractReactiveFormSpecComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AbstractReactiveFormSpecComponent
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AbstractReactiveFormSpecComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
        expect(() => component.ngOnDestroy()).not.toThrow();
    });

    it("should get and patch form values", () => {
        let value = null;
        const field = "19123";
        const array = [];
        component.appValueChange.subscribe((_) => value = _);
        component.patchValue({field});
        expect(component.getValue()).toEqual({field, array});
        expect(component.getValue("field")).toBe(field);
        expect(component.getValue("array")).toEqual(array);
        expect(value).toEqual({field, array});

        component.appFormGroup = new FormGroup({});
        expect(component.getValue("field")).not.toBeDefined();
        component.appFormGroup = null;
        expect(component.getValue("field")).not.toBeDefined();
    });

    it("should set up form arrays", () => {
        const array = [1, 9];
        const formArray = component.appFormGroup.get("array") as FormArray;

        component.disable(true);
        component.setValueForArray(array, "array");
        expect(formArray).toBeInstanceOf(FormArray);
        expect(formArray.length).toBe(array.length);
        expect(component.getValue("array")).toEqual(array);
        expect(component.appFormGroup.disabled).toBeTrue();

        component.disable(false);
        component.setValueForArray(array, "array");
        expect(component.appFormGroup.disabled).toBeFalse();

        expect(() => component.createControl(null, null)).not.toThrow();
        expect(() => component.createControl(null, "array")).not.toThrow();
        component.appFormGroup = null;
        expect(() => component.createControl(null, null)).not.toThrow();
    });

});
