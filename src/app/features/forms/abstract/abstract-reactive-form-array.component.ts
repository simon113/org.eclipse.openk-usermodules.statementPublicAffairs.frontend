/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Input} from "@angular/core";
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {arrayJoin} from "../../../util/store";

export abstract class AbstractReactiveFormArrayComponent<T> {

    @Input()
    public appFormGroup: FormGroup;

    @Input()
    public appFormArrayName: string;

    public getValue(): T[] {
        return arrayJoin(this.getFormArray().value);
    }

    public setValue(value: T[]) {
        this.getFormArray().clear();
        arrayJoin(value).forEach((_) => {
            this.addControl(_);
        });
    }

    public getFormArray(): FormArray {
        const result = this.appFormGroup?.get(this.appFormArrayName);
        return result instanceof FormArray ? result : undefined;
    }

    public getFormArrayControl(index: number) {
        return this.appFormGroup?.get([this.appFormArrayName, index]);
    }

    public addControl(value: T, atIndex?: number) {
        this.getFormArray().insert(atIndex != null ? atIndex : this.getFormArray().length, this.createControl(value));
    }

    public moveControl(fromIndex: number, toIndex: number) {
        const control = this.getFormArrayControl(fromIndex);
        this.removeControlAt(fromIndex);
        this.getFormArray().insert(toIndex, control);
    }

    public removeControlAt(index: number) {
        this.getFormArray().removeAt(index);
    }

    public createControl(value: T) {
        const control = new FormControl(value);
        if (this.getFormArray().disabled) {
            control.disable();
        }
        return control;
    }

}
