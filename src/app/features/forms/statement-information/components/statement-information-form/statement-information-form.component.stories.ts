/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {provideMockStore} from "@ngrx/store/testing";
import {action} from "@storybook/addon-actions";
import {boolean, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../../core";
import {statementInformationFormValueSelector, statementTypesSelector} from "../../../../../store";
import {createSelectOptionsMock} from "../../../../../test";
import {StatementInformationFormModule} from "../../statement-information-form.module";

storiesOf("Features / Forms", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            I18nModule,
            BrowserAnimationsModule,
            StatementInformationFormModule
        ],
        providers: [
            provideMockStore({
                selectors: [
                    {
                        selector: statementTypesSelector,
                        value: createSelectOptionsMock(5, "Statement Type")
                    },
                    {
                        selector: statementInformationFormValueSelector,
                        value: {}
                    }
                ]
            }),

        ]
    }))
    .add("StatementInformationFormComponent", () => ({
        template: `
            <app-statement-information-form
                (appValueChange)="appValueChange($event)"
                [appForNewStatement]="appForNewStatement"
                style="padding: 1em; box-sizing: border-box;">
            </app-statement-information-form>
        `,
        props: {
            appValueChange: action("appValueChange"),
            appForNewStatement: boolean("appForNewStatement", false),
        }
    }));
