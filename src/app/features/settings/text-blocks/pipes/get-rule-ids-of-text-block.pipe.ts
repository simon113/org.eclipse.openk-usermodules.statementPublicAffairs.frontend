/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPITextBlockModel, TAPIRequireRuleType, TAPITextBlockRuleKey} from "../../../../core";
import {arrayJoin} from "../../../../util";

@Pipe({name: "getRuleIdsOfTextBlock"})
export class GetRuleIdsOfTextBlockPipe implements PipeTransform {

    public transform(
        value: IAPITextBlockModel,
        key: TAPITextBlockRuleKey,
        type?: TAPIRequireRuleType
    ): string[] {
        if (value == null) {
            return [];
        }

        switch (key) {
            case "excludes":
                return arrayJoin(value.excludes);
            case "requires":
                return arrayJoin(arrayJoin(value.requires).find((rule) => rule.type === type)?.ids);
        }

        return [];
    }

}
