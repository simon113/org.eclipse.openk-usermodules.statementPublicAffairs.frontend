/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkDrag} from "@angular/cdk/drag-drop";
import {Component, ElementRef, Input, OnDestroy, OnInit, QueryList, ViewChildren} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {merge, of, Subject} from "rxjs";
import {filter, map, switchMap, take, takeUntil, tap} from "rxjs/operators";
import {IAPITextBlockModel, TAPIRequireRuleType, TAPITextBlockRuleKey} from "../../../../../core";
import {arrayJoin, filterDistinctValues} from "../../../../../util/store";
import {GetRuleIdsOfTextBlockPipe} from "../../pipes";
import {TextBlockSettingsForm} from "../../TextBlockSettingsForm";

export interface ITextBlockFormTextControlButton {
    label?: string;
    icon?: string;
    token: string;
    startToken?: string;
    rotateIcon?: boolean;
    requireLineBreak?: boolean;
}

export interface ITextBlockFormDropAreaConfig {
    label: string;
    key: TAPITextBlockRuleKey;
    type?: TAPIRequireRuleType;
    elementId: string;
}

@Component({
    selector: "app-text-block-form",
    templateUrl: "./text-block-form.component.html",
    styleUrls: ["./text-block-form.component.scss"]
})
export class TextBlockFormComponent implements OnInit, OnDestroy {

    private static id = 0;

    public readonly id = `TextBlockFormComponent-${TextBlockFormComponent.id++}`;

    @Input()
    public appTextControlDefaultButtons: ITextBlockFormTextControlButton[] = [];

    @Input()
    public appTextControlReplacementButtons: ITextBlockFormTextControlButton[] = [];

    @Input()
    public appTextControlSelectButtons: ITextBlockFormTextControlButton[] = [];

    public readonly dropAreaConfig: ITextBlockFormDropAreaConfig[] = [
        {
            label: "settings.textBlocks.rules.excludes",
            key: "excludes",
            elementId: this.id + "-DropArea-Excludes",
        },
        {
            label: "settings.textBlocks.rules.requiresAnd",
            key: "requires",
            type: "and",
            elementId: this.id + "-DropArea-Required-And"
        },
        {
            label: "settings.textBlocks.rules.requiresOr",
            key: "requires",
            type: "or",
            elementId: this.id + "-DropArea-Required-Or"
        },
        {
            label: "settings.textBlocks.rules.requiresXOR",
            key: "requires",
            type: "xor",
            elementId: this.id + "-DropArea-Required-Xor"
        }
    ];

    public readonly dropListElementIds = this.dropAreaConfig.map((area) => area.elementId);

    public selectedRule: { id: string, area: ITextBlockFormDropAreaConfig };

    @Input()
    public appFormGroup: FormGroup = new FormGroup({});

    @ViewChildren("textAreaElement")
    public queryList: QueryList<ElementRef<HTMLElement>>;

    private focus$ = new Subject<FocusOptions>();

    private destroy$ = new Subject();

    public constructor(public form: TextBlockSettingsForm) {

    }

    public enterPredicate = (drag: CdkDrag<IAPITextBlockModel>) => drag?.data?.id !== this.getValue()?.id;

    public ngOnInit() {
        this.focus$.pipe(
            switchMap((options) => {
                return merge(of(0), this.queryList.changes).pipe(
                    map(() => this.queryList.first),
                    filter((element) => element != null),
                    take(1),
                    tap((element) => element.nativeElement.focus(options))
                );
            }),
            takeUntil(this.destroy$),
        ).subscribe();
    }

    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public focus(options?: FocusOptions) {
        this.focus$.next(options);
    }

    public getValue(): IAPITextBlockModel {
        return {...this.appFormGroup?.value};
    }

    public getRuleIds(area: ITextBlockFormDropAreaConfig): string[] {
        return new GetRuleIdsOfTextBlockPipe().transform(this.getValue(), area.key, area.type);
    }

    public isDropAllowed(id: string, area: ITextBlockFormDropAreaConfig): boolean {
        return id != null && id !== this.getValue().id && !this.getRuleIds(area).includes(id);
    }

    public onEnter(id: string, area: ITextBlockFormDropAreaConfig) {
        if (this.isDropAllowed(id, area)) {
            this.selectedRule = {id, area};
        }
    }

    public onExit() {
        this.selectedRule = undefined;
    }

    public onDrop(id: string, area: ITextBlockFormDropAreaConfig) {
        if (this.isDropAllowed(id, area)) {
            const ids = arrayJoin(this.getRuleIds(area), [id]);
            this.patchRule(ids, area);
        }
        this.selectedRule = undefined;
    }

    public removeIdFromArea(id: string, area: ITextBlockFormDropAreaConfig) {
        const ids = this.getRuleIds(area).filter((_) => _ !== id);
        this.patchRule(ids, area);
    }

    private patchRule(ids: string[], area: ITextBlockFormDropAreaConfig) {
        ids = filterDistinctValues(ids).sort((a, b) => this.form.compareIds(a, b));
        area = {...area};
        const value = this.getValue();
        switch (area.key) {
            case "excludes":
                value.excludes = ids;
                break;
            case "requires":
                value.requires = arrayJoin(value.requires)
                    .filter((rule) => rule.type !== area.type);
                value.requires.push({type: area.type, ids});
                value.requires = value.requires.filter((rule) => arrayJoin(rule.ids).length > 0);
                break;
        }
        this.appFormGroup.patchValue(value);
    }

}
