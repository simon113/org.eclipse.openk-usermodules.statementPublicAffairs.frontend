/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkDrag} from "@angular/cdk/drag-drop";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {FormGroup} from "@angular/forms";
import {I18nModule, IAPITextBlockModel} from "../../../../../core";
import {createTextBlockModelMock} from "../../../../../test";
import {TextBlockSettingsModule} from "../../text-block-settings.module";
import {TextBlockSettingsForm} from "../../TextBlockSettingsForm";
import {TextBlockFormComponent} from "./text-block-form.component";

describe("TextBlockFormComponent", () => {
    let component: TextBlockFormComponent;
    let fixture: ComponentFixture<TextBlockFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [TextBlockSettingsModule, I18nModule],
            providers: [{
                provide: TextBlockSettingsForm,
                useValue: new TextBlockSettingsForm()
            }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextBlockFormComponent);
        component = fixture.componentInstance;
        component.form.setForm({
            selects: {},
            groups: [{groupName: "Group", textBlocks: [createTextBlockModelMock("", "")]}],
            negativeGroups: []
        });
        component.appFormGroup = component.form.getTextBlockControls()[0] as FormGroup;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should check the text block ID as enter predicate", () => {
        const drag = {data: {id: "1.1"}} as CdkDrag<IAPITextBlockModel>;
        expect(component.enterPredicate(drag)).toBe(false);
        drag.data.id = "1.19";
        expect(component.enterPredicate(drag)).toBe(true);
        drag.data = null;
        expect(component.enterPredicate(drag)).toBe(true);
        expect(component.enterPredicate(null)).toBe(true);
        component.appFormGroup = null;
    });

    it("should focus text area element", () => {
        component.focus();
        expect(document.activeElement).toBe(component.queryList.first.nativeElement);
    });

    it("should add id to rule on drag and drop", () => {
        const id = "1.19";
        component.dropAreaConfig.forEach((area) => {
            component.onEnter(id, area);
            expect(component.selectedRule).toEqual({id, area});
            component.onExit();
            expect(component.selectedRule).not.toBeDefined();
            component.onEnter(id, area);
            expect(component.selectedRule).toEqual({id, area});
            component.onDrop(id, area);
            expect(component.selectedRule).not.toBeDefined();
            expect(component.getRuleIds(area)).toEqual([id]);
        });
    });

    it("should remove id from rule", () => {
        const id = "1.19";
        component.dropAreaConfig.forEach((area) => {
            component.onDrop(id, area);
            expect(component.getRuleIds(area)).toEqual([id]);
        });
        component.dropAreaConfig.forEach((area) => {
            component.removeIdFromArea(id, area);
            component.removeIdFromArea(id, area);
            expect(component.getRuleIds(area)).toEqual([]);
        });
        expect(component.getValue().requires.length).toBe(0);
    });

});
