/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {TranslateModule} from "@ngx-translate/core";
import {ActionButtonModule} from "../../../shared/layout/action-button";
import {SideMenuModule} from "../../../shared/layout/side-menu";
import {SettingsSideMenuComponent} from "./components";
import {GetDepartmentGroupsFromTablePipe} from "./pipes";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,

        ActionButtonModule,
        SideMenuModule
    ],
    declarations: [
        SettingsSideMenuComponent,

        GetDepartmentGroupsFromTablePipe
    ],
    exports: [
        SettingsSideMenuComponent,

        GetDepartmentGroupsFromTablePipe
    ]
})
export class SharedSettingsModule {

}

