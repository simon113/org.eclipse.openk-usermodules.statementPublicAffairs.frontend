/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {I18nModule} from "../../../../core";
import {SharedSettingsModule} from "../shared-settings.module";
import {SettingsSideMenuComponent} from "./settings-side-menu.component";

describe("SettingsSideMenuComponent", () => {
    let component: SettingsSideMenuComponent;
    let fixture: ComponentFixture<SettingsSideMenuComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SharedSettingsModule,
                RouterTestingModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SettingsSideMenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
