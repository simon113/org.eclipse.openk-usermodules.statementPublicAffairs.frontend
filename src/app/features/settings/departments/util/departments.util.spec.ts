/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIDepartmentTable} from "../../../../core/api/settings";
import {parseDepartmentTableFromCsv, reduceDepartmentTableToCsv} from "./departments.util";

describe("DepartmentsUtil", () => {
    const text = "City;District;Sectors;Departments;;;;;;;;;;" +
        "\r\n" +
        "Darmstadt;Darmstadt;Strom,Gas,Wasser,Straßenbeleuchtung;" +
        "Allgemein;Gas;Allgemein;Wasser;Regionalstelle Darmstadt;Gas (Betrieb);Regionalstelle Darmstadt;Gas (Planung/Bau);" +
        "Regionalstelle Darmstadt;Strom (Planung/Bau);Regionalstelle Darmstadt;Strom (Betrieb)" +
        "\r\n" +
        "Heppenheim;Heppenheim;Strom,Gas,Straßenbeleuchtung;" +
        "Allgemein;Gas;Allgemein;Wasser;Regionalstelle Heppenheim;Gas (Betrieb);Regionalstelle Heppenheim;Gas (Planung/Bau);" +
        "Regionalstelle Heppenheim;Strom (Planung/Bau);;";

    const data: IAPIDepartmentTable = {
        "Darmstadt#Darmstadt": {
            provides: ["Strom", "Gas", "Wasser", "Straßenbeleuchtung"],
            departments: {
                Allgemein: ["Gas", "Wasser"],
                "Regionalstelle Darmstadt": ["Gas (Betrieb)", "Gas (Planung/Bau)", "Strom (Planung/Bau)", "Strom (Betrieb)"]
            }
        },
        "Heppenheim#Heppenheim": {
            provides: ["Strom", "Gas", "Straßenbeleuchtung"],
            departments: {
                Allgemein: ["Gas", "Wasser"],
                "Regionalstelle Heppenheim": ["Gas (Betrieb)", "Gas (Planung/Bau)", "Strom (Planung/Bau)"]
            }
        }
    };

    it("parseDepartmentTableFromCsv", () => {
        expect(parseDepartmentTableFromCsv(text + "\r\n;;;;;;" + "\r\n")).toEqual(data);
        expect(() => parseDepartmentTableFromCsv("\r\na;b;c")).not.toThrow();
        expect(() => parseDepartmentTableFromCsv("\r\na;b")).toThrow();
    });

    it("reduceDepartmentTableToCsv", () => {
        expect(reduceDepartmentTableToCsv(data)).toBe(text);
    });

});
