/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {IAPIDepartmentGroups} from "../../../../../core";

@Component({
    selector: "app-departments-settings-table",
    templateUrl: "./departments-settings-table.component.html",
    styleUrls: ["./departments-settings-table.component.scss"]
})
export class DepartmentsSettingsTableComponent {

    @Input()
    public appDepartments: { key: string, value: { provides: string[], departments: IAPIDepartmentGroups }, searchString?: string }[] = [];

    @Input()
    public appColumns = ["city", "district", "sectors", "departments"];

}
