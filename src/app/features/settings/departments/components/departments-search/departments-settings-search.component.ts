/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from "@angular/core";
import {IAPIDepartmentGroups} from "../../../../../core";
import {ISelectOption} from "../../../../../shared/controls/select";
import {arrayJoin} from "../../../../../util";

@Component({
    selector: "app-departments-settings-search",
    templateUrl: "./departments-settings-search.component.html",
    styleUrls: ["./departments-settings-search.component.scss"]
})
export class DepartmentsSettingsSearchComponent implements OnChanges {

    @Input()
    public appDepartmentList: { key: string, value: { provides: string[], departments: IAPIDepartmentGroups } }[];

    @Input()
    public appPage = 0;

    @Input()
    public appPageSize = 10;

    @Input()
    public appPageSizeOptions: ISelectOption[] = [
        {label: "5", value: 5},
        {label: "10", value: 10},
        {label: "25", value: 25},
        {label: "50", value: 50}
    ];

    @Output()
    public appIsSearching = new EventEmitter<boolean>();

    public filteredDepartmentList: { key: string, value: { provides: string[], departments: IAPIDepartmentGroups } }[] = [];

    public totalPages: number;

    public searchText = "";

    public ngOnChanges(changes: SimpleChanges) {
        const onChange = (keys: Array<keyof DepartmentsSettingsSearchComponent>, fn: () => any) => {
            if (keys.some((key) => changes[key] != null)) {
                fn();
            }
        };
        onChange(["appDepartmentList"], () => {
            this.filterDepartmentList("");
        });
        onChange(["appPage", "appPageSize"], () => {
            this.changePage(this.appPage, this.appPageSize);
        });
    }

    public filterDepartmentList(searchText: string) {
        this.searchText = searchText;
        this.filteredDepartmentList = arrayJoin(this.appDepartmentList)
            .filter((_) => {
                const searchValues = arrayJoin(
                    _.key.split("#"), // City and district
                    _.value.provides, // Sectors
                    Object.keys(_.value.departments), // Department group names
                    ...Object.values(_.value.departments) // Department names
                ).join("").toLowerCase();
                const searchTokens = this.searchText
                    .toLowerCase()
                    .replace(/[;,.]/g, " ")
                    .split(" ");
                return !searchTokens.some((token) => !searchValues.includes(token));
            });
        this.changePage(0);
    }

    public changePage(page: number = this.appPage, pageSize: number = this.appPageSize) {
        this.appPageSize = Math.max(1, pageSize);
        this.appPage = Math.max(0, page);
        this.totalPages = Math.ceil(this.filteredDepartmentList.length / this.appPageSize);
    }

}
