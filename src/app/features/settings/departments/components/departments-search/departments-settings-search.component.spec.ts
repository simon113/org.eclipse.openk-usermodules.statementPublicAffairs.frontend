/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {SimpleChange} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule, IAPIDepartmentGroups} from "../../../../../core";
import {DepartmentsSettingsModule} from "../../departments-settings.module";
import {DepartmentsSettingsSearchComponent} from "./departments-settings-search.component";

describe("DepartmentsSettingsSearchComponent", () => {

    const departments: { key: string, value: { provides: string[], departments: IAPIDepartmentGroups }, searchString?: string }[] = [
        {
            key: "Ort#Ortsteil",
            value: {
                provides: [
                    "Gas",
                    "Strom"
                ],
                departments: {
                    Allgemein: [
                        "Medianet",
                        "Planung"
                    ]
                }
            }
        },
        {
            key: "Ort#Süd",
            value: {
                provides: [
                    "Wasser",
                    "Strom"
                ],
                departments: {
                    Allgemein: [
                        "Planung"
                    ]
                }
            }
        }
    ];

    let component: DepartmentsSettingsSearchComponent;
    let fixture: ComponentFixture<DepartmentsSettingsSearchComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                DepartmentsSettingsModule,
                RouterTestingModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DepartmentsSettingsSearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should filter the given department table", () => {
        component.appDepartmentList = departments;
        component.ngOnChanges({appDepartmentList: new SimpleChange([], departments, false)});
        expect(component.filteredDepartmentList).toEqual(departments);

        component.filterDepartmentList("Allgemein, Media");
        expect(component.filteredDepartmentList).toEqual([departments[0]]);
    });

    it("should set page info", () => {
        component.appPage = 1;
        component.appPageSize = 1;
        component.appDepartmentList = departments;
        component.filterDepartmentList("");
        expect(component.appPage).toBe(0);

        component.appPage = 1;
        component.appPageSize = 1;
        component.ngOnChanges({appPage: new SimpleChange(0, 1, false)});
        expect(component.appPageSize).toBe(1);
        expect(component.appPage).toBe(1);
        expect(component.totalPages).toBe(2);
    });

});
