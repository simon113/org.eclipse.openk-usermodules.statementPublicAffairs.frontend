/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from "@angular/core";
import {FormControl, Validators} from "@angular/forms";
import {IAPIUserInfoExtended, IAPIUserSettings} from "../../../../../core";
import {EErrorCode} from "../../../../../store";
import {createFormGroup} from "../../../../../util/forms";

export interface IUserSettingsEditFormValue {
    email: string;
    departmentGroupName?: string;
    departmentName?: string;
}

@Component({
    selector: "app-users-settings-edit",
    templateUrl: "./users-settings-edit.component.html",
    styleUrls: ["./users-settings-edit.component.scss"]
})
export class UsersSettingsEditComponent implements OnChanges {

    @Input()
    public appSelectedUser: IAPIUserInfoExtended;

    @Input()
    public appDisabled: boolean;

    @Input()
    public appDepartments: { key: string, value: string[] }[];

    @Output()
    public appSubmit = new EventEmitter<{ id: number, value: IAPIUserSettings }>();

    @Output()
    public appError = new EventEmitter<EErrorCode>();

    public formGroup = createFormGroup<IUserSettingsEditFormValue>({
        email: new FormControl("", [Validators.email, Validators.required]),
        departmentGroupName: new FormControl(null),
        departmentName: new FormControl(null)
    });

    public patchValue(value: Partial<IUserSettingsEditFormValue>) {
        this.formGroup.patchValue({...value});
    }

    public getValue(): IUserSettingsEditFormValue {
        return this.formGroup.value;
    }

    public ngOnChanges(changes: SimpleChanges) {
        const onChange = (keys: Array<keyof UsersSettingsEditComponent>, fn: () => any) => {
            if (keys.some((key) => changes[key])) {
                fn();
            }
        };
        onChange(["appSelectedUser"], () => this.patchValue({
            email: this.appSelectedUser.settings?.email,
            departmentGroupName: this.appSelectedUser.settings?.department?.group,
            departmentName: this.appSelectedUser.settings?.department?.name
        }));
        onChange(["appDisabled"], () => this.appDisabled ? this.formGroup.disable() : this.formGroup.enable());
    }

    public submit(id: number) {
        const {email, departmentName, departmentGroupName} = {...this.getValue()};
        this.formGroup.markAsTouched();

        if (this.formGroup.invalid) {
            this.appError.emit(EErrorCode.BAD_USER_DATA);
            return;
        }

        if (departmentGroupName != null && departmentName == null) {
            this.appError.emit(EErrorCode.MISSING_FORM_DATA);
            return;
        }

        this.appSubmit.emit({
            id,
            value: {email, department: {name: departmentName, group: departmentGroupName}}
        });
    }
}
