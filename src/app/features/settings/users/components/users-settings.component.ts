/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {IAPIUserSettings} from "../../../../core/api/settings";
import {
    EErrorCode,
    fetchDepartmentsSettingsAction,
    fetchUsersAction,
    getDepartmentsSettingsSelector,
    getSettingsLoadingSelector,
    getUsersSettingsSelector,
    setErrorAction,
    submitUserSettingsAction,
    syncUserDataAction
} from "../../../../store";
import {IUserListFilter} from "../pipes";

@Component({
    selector: "app-users-settings",
    templateUrl: "./users-settings.component.html",
    styleUrls: ["./users-settings.component.scss"]
})
export class UsersSettingsComponent implements OnInit {

    public users$ = this.store.pipe(select(getUsersSettingsSelector));

    public loading$ = this.store.pipe(select(getSettingsLoadingSelector));

    public departmentSettings$ = this.store.pipe(select(getDepartmentsSettingsSelector));

    public selectedUserId: number;

    public filter: IUserListFilter = {};

    public constructor(
        private readonly store: Store
    ) {
    }

    public ngOnInit() {
        this.store.dispatch(fetchUsersAction());
        this.store.dispatch(fetchDepartmentsSettingsAction());
    }

    public sync() {
        this.store.dispatch(syncUserDataAction());
    }

    public submitUserSettings(userId: number, data: IAPIUserSettings) {
        this.store.dispatch(submitUserSettingsAction({userId, data}));
    }

    public showErrorMessage(error: EErrorCode) {
        this.store.dispatch(setErrorAction({error}));
    }

}
