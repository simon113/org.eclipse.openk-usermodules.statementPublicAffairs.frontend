/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, TestBed} from "@angular/core/testing";
import {TranslateService} from "@ngx-translate/core";
import {of} from "rxjs";
import {I18nModule} from "../../../../core";
import {OptionsFromDepartmentStructurePipe} from "./options-from-department-structure.pipe";

describe("OptionsFromDepartmentStructurePipe", () => {

    const translatedLabel = "translatedLabel";
    let pipe: OptionsFromDepartmentStructurePipe;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [I18nModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        const translateService = TestBed.inject(TranslateService);
        pipe = new OptionsFromDepartmentStructurePipe(TestBed.inject(TranslateService));
        spyOn(translateService, "get").and.returnValue(of(translatedLabel));
    });

    it("should get option array for departments", async () => {
        const groupName = "groupName";
        await expectAsync(pipe.transform([{key: groupName, value: []}])).toBeResolvedTo([
            {
                label: translatedLabel,
                value: null
            },
            {
                label: groupName,
                value: groupName
            }
        ]);
    });

});

