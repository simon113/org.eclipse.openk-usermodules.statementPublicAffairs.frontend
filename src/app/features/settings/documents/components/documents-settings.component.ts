/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {Subject} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {getAttachmentTagsSelector, getSettingsLoadingSelector, submitTagsAction} from "../../../../store";

@Component({
    selector: "app-tag-settings",
    templateUrl: "./documents-settings.component.html",
    styleUrls: ["./documents-settings.component.scss"]
})
export class DocumentsSettingsComponent implements OnInit, OnDestroy {

    public tags$ = this.store.pipe(select(getAttachmentTagsSelector)).pipe(
        map((tags) => tags.map((_) => _.label))
    );

    public loading$ = this.store.pipe(select(getSettingsLoadingSelector));

    public tagList: { label: string, add?: boolean }[] = [];

    private destroy$ = new Subject();

    public constructor(private readonly store: Store) {
    }

    public ngOnInit() {
        this.tags$.pipe(
            takeUntil(this.destroy$)
        ).subscribe((tags) => {
            this.tagList = tags.map((_) => ({label: _}));
        });
    }

    public async ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public addTag(tagLabel: string) {
        if (tagLabel) {
            this.tagList.push({label: tagLabel, add: true});
        }
    }

    public deleteTag(index: number) {
        this.tagList.splice(index, 1);
    }

    public save() {
        const labels = this.tagList.filter((tag) => tag.add != null).map((_) => _.label);
        this.store.dispatch(submitTagsAction({labels}));
    }

}
