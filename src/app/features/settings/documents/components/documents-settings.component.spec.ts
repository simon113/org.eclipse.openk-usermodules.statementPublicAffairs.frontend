/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core";
import {submitTagsAction} from "../../../../store/statements/actions";
import {DocumentsSettingsModule} from "../documents.settings.module";
import {DocumentsSettingsComponent} from "./documents-settings.component";

describe("TagSettingsComponent", () => {
    let component: DocumentsSettingsComponent;
    let fixture: ComponentFixture<DocumentsSettingsComponent>;
    let store: Store;

    const tags = [
        {label: "label", add: true},
        {label: "label1"},
        {label: "label2"},
        {label: "label3", add: true}
    ];

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                DocumentsSettingsModule,
                RouterTestingModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DocumentsSettingsComponent);
        component = fixture.componentInstance;
        store = fixture.componentRef.injector.get(Store);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should add string to taglist if set", () => {
        expect(component.tagList).toEqual([]);
        component.addTag(null);
        expect(component.tagList).toEqual([]);
        component.addTag("");
        expect(component.tagList).toEqual([]);
        component.addTag("test");
        expect(component.tagList.length).toEqual(1);
        expect(component.tagList).toEqual([{label: "test", add: true}]);
    });

    it("should delete the specified entry from taglist", () => {
        component.tagList = tags;
        component.deleteTag(14);
        expect(component.tagList).toEqual(tags);
        component.deleteTag(2);
        expect(component.tagList).toEqual([
            {label: "label", add: true},
            {label: "label1"},
            {label: "label3", add: true}
        ]);
    });

    it("should dispatch submitTagsAction with labels of tags to be added", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        component.tagList = tags;
        await component.save();
        expect(dispatchSpy).toHaveBeenCalledWith(submitTagsAction({labels: ["label", "label3"]}));
    });
});
