/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {fakeAsync} from "@angular/core/testing";
import {LeafletEventHandlerFn, Map} from "leaflet";
import {fromLeafletEvent, latLngZoomToString, stringToLatLngZoom} from "./leaflet.util";

describe("LeafletUtil", () => {

    it("fromLeafletEvent", fakeAsync(() => {
        const onItems: Array<{ type: string, fn: LeafletEventHandlerFn }> = [];
        const offItems: Array<{ type: string, fn: LeafletEventHandlerFn }> = [];

        const leafletMock = new LeafletMapMock(onItems, offItems) as any as Map;

        const subscription = fromLeafletEvent(leafletMock, "move").subscribe();

        expect(onItems.length).toBe(1);
        expect(offItems.length).toBe(0);
        expect(onItems[0].type).toBe("move");

        subscription.unsubscribe();

        expect(onItems.length).toBe(1);
        expect(offItems.length).toBe(1);
        expect(offItems[0].type).toBe("move");
        expect(offItems[0].fn).toBe(onItems[0].fn);

        expect(fromLeafletEvent(null, "move").subscribe().closed).toBe(true);
    }));

    it("latLngZoomToString", () => {
        expect(latLngZoomToString({lat: 1, lng: 2}, 3)).toEqual("1,2,3");
        expect(latLngZoomToString({lat: null, lng: 2}, 3)).not.toBeDefined();
        expect(latLngZoomToString({lat: 1, lng: null}, 3)).not.toBeDefined();
        expect(latLngZoomToString({lat: 1, lng: 2}, null)).not.toBeDefined();
        expect(latLngZoomToString(null, 3)).not.toBeDefined();
    });

    it("stringToLatLngZoom", () => {
        expect(stringToLatLngZoom("1,2,3")).toEqual({lat: 1, lng: 2, zoom: 3});
        expect(stringToLatLngZoom("1,2,3,4,5,6")).not.toBeDefined();
        expect(stringToLatLngZoom("1;2;3")).not.toBeDefined();
        expect(stringToLatLngZoom(null)).not.toBeDefined();
        expect(stringToLatLngZoom("1,2")).not.toBeDefined();
    });

});

class LeafletMapMock {

    public constructor(
        private onItems: Array<{ type: string, fn: LeafletEventHandlerFn }> = [],
        private offItems: Array<{ type: string, fn: LeafletEventHandlerFn }> = []
    ) {

    }

    on(type: string, fn: LeafletEventHandlerFn, context?: any): this {
        this.onItems.push({type, fn});
        return this;
    }

    off(type: string, fn: LeafletEventHandlerFn, context?: any): this {
        this.offItems.push({type, fn});
        return this;
    }

}
