/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ViewChild} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {MapModule} from "../../map.module";
import {LeafletPopupDirective} from "./leaflet-popup.directive";

describe("LeafletPopupDirective", () => {

    const latLngZoom = "49.87774673189807,8.651438355445864,17";

    let component: LeafletPopupSpecComponent;
    let fixture: ComponentFixture<LeafletPopupSpecComponent>;
    let directive: LeafletPopupDirective;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MapModule],
            declarations: [LeafletPopupSpecComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LeafletPopupSpecComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        directive = component.directive;
    });

    it("should create", () => {
        expect(directive).toBeDefined();
    });

    it("should open/close popup on input", () => {
        const openOnSpy = spyOn(directive.popup, "openOn");
        const removeFromSpy = spyOn(directive.popup, "removeFrom");

        openOnSpy.calls.reset();
        removeFromSpy.calls.reset();
        directive.appLeafletPopup = latLngZoom;
        fixture.detectChanges();
        expect(openOnSpy).toHaveBeenCalledWith(directive.leafletHandler.instance);
        expect(removeFromSpy).not.toHaveBeenCalled();

        openOnSpy.calls.reset();
        removeFromSpy.calls.reset();
        directive.appLeafletPopup = null;
        fixture.detectChanges();
        expect(removeFromSpy).toHaveBeenCalledWith(directive.leafletHandler.instance);
        expect(openOnSpy).not.toHaveBeenCalled();
    });

    it("should update popup on data changes", () => {
        const updateViewSpy = spyOn(directive, "updateView");
        updateViewSpy.calls.reset();
        component.data = {id: 19};
        fixture.detectChanges();
        expect(updateViewSpy).toHaveBeenCalled();

        updateViewSpy.calls.reset();
        component.coordinates = latLngZoom;
        fixture.detectChanges();
        expect(updateViewSpy).not.toHaveBeenCalled();
    });

});

@Component({
    selector: "app-leaflet-popup-spec",
    template: `
        <div appLeaflet>
            <div *appLeafletPopup="coordinates; data: data"></div>
        </div>
    `
})
class LeafletPopupSpecComponent {

    public coordinates: string;

    public data: any = {};

    @ViewChild(LeafletPopupDirective, {static: true})
    public directive: LeafletPopupDirective;

}
