/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {StringToLatLngZoomPipe} from "./string-to-lat-lng-zoom.pipe";

describe("StringToLatLngZoomPipe", () => {

    it("should transform lat/lng/zoom strings to objects", () => {
        const pipe: StringToLatLngZoomPipe = new StringToLatLngZoomPipe();
        expect(pipe.transform("1,2,3")).toEqual({lat: 1, lng: 2, zoom: 3});
    });

});
