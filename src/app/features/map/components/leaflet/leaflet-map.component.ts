/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, forwardRef, Input, NgZone, OnDestroy, OnInit, Output, ViewChild} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {LeafletMouseEvent, PopupEvent} from "leaflet";
import {Subject} from "rxjs";
import {filter, map, skip, takeUntil} from "rxjs/operators";
import {getNominatimLoadingSelector, getNominatimSearchResultSelector, searchMapAction} from "../../../../store";
import {ILeafletBounds, LeafletDirective, LeafletHandler} from "../../directives";
import {latLngZoomToString, stringToLatLngZoom} from "../../util";

@Component({
    selector: "app-leaflet-map",
    templateUrl: "./leaflet-map.component.html",
    styleUrls: ["./leaflet-map.component.scss"],
    providers: [
        {
            provide: LeafletHandler,
            useExisting: forwardRef(() => LeafletMapComponent)
        }
    ]
})
export class LeafletMapComponent extends LeafletHandler implements OnInit, OnDestroy {

    @Input()
    public appDisabled: boolean;

    @Input()
    public appCenter: string;

    @Output()
    public appClick = new EventEmitter<LeafletMouseEvent>();

    @Output()
    public appPopupClose = new EventEmitter<PopupEvent>();

    @Output()
    public appCenterChange = new EventEmitter<string>();

    @Input()
    public appSubCaption: string;

    @Output()
    public appOpenGis = new EventEmitter<ILeafletBounds>();

    @Output()
    public appSearch = new EventEmitter<string>();

    public appIsLoading$ = this.store.pipe(select(getNominatimLoadingSelector));

    public searchResult$ = this.store.pipe(select(getNominatimSearchResultSelector));

    private destroy$ = new Subject();

    @ViewChild(LeafletDirective, {static: true})
    public leafletDirective: LeafletDirective;

    public constructor(
        public readonly ngZone: NgZone,
        private readonly store: Store
    ) {
        super();
    }

    public get instance() {
        return this.leafletDirective.instance;
    }

    public getZoom() {
        return this.leafletDirective.getZoom();
    }

    public ngOnInit() {
        this.searchResult$.pipe(
            skip(1),
            filter((center) => center != null),
            map((center) => latLngZoomToString(center, this.leafletDirective.getZoom())),
            takeUntil(this.destroy$)
        ).subscribe((result) => {
            this.appCenter = result;
            this.appSearch.emit(result);
            this.leafletDirective.appCenter = stringToLatLngZoom(result);
        });
    }

    public async ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public search(search: string) {
        if (search) {
            this.store.dispatch(searchMapAction({q: search}));
        }
    }
}
