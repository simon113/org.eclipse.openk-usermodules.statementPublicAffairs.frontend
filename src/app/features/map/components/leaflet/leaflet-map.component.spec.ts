/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule, IAPINominatimSearchResult} from "../../../../core";
import {getNominatimResponseContentSelector, searchMapAction} from "../../../../store";
import {MapModule} from "../../map.module";
import {latLngZoomToString} from "../../util";
import {LeafletMapComponent} from "./leaflet-map.component";

describe("LeafletMapComponent", () => {
    let component: LeafletMapComponent;
    let fixture: ComponentFixture<LeafletMapComponent>;
    let store: MockStore;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MapModule, I18nModule],
            providers: [provideMockStore({initialState: {geo: {loading: false, responseContent: null}}})]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LeafletMapComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should dispatch searchMapAction on search", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.search("");
        expect(dispatchSpy).not.toHaveBeenCalled();
        await component.search(null);
        expect(dispatchSpy).not.toHaveBeenCalled();
        await component.search("test");
        expect(dispatchSpy).toHaveBeenCalledWith(searchMapAction({q: "test"}));
    });

    it("should emit appViewChanged when search response is changed", () => {
        const lat = 52.520008;
        const lng = 13.404954;
        const searchResultEntry: IAPINominatimSearchResult = {
            ...{} as IAPINominatimSearchResult,
            lat: "" + lat,
            lon: "" + lng
        };
        store.overrideSelector(getNominatimResponseContentSelector, [searchResultEntry]);
        const searchSpy = spyOn(component.appSearch, "emit");
        store.refreshState();
        const zoom = component.getZoom();
        expect(searchSpy).toHaveBeenCalledWith(latLngZoomToString({lat, lng}, zoom));
    });

});
