/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {provideMockStore} from "@ngrx/store/testing";
import {LatLng, LeafletMouseEvent} from "leaflet";
import {I18nModule} from "../../../../core";
import {MapModule} from "../../map.module";
import {MapSelectComponent} from "./map-select.component";

describe("MapSelectComponent", () => {
    let component: MapSelectComponent;
    let fixture: ComponentFixture<MapSelectComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MapModule, I18nModule],
            providers: [provideMockStore()]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MapSelectComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should select a new value", () => {
        const changeSpy = spyOn(component.appValueChange, "emit");
        const mouseEvent: LeafletMouseEvent = {
            latlng: {
                lat: 52.520008,
                lng: 13.404954
            } as LatLng
        } as unknown as LeafletMouseEvent;

        component.select(mouseEvent, 11);

        const expectedValue = "52.520008,13.404954,11";
        expect(component.appValue).toEqual(expectedValue);
        expect(changeSpy).toHaveBeenCalledWith(expectedValue);
    });

});
