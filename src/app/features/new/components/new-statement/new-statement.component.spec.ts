/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core";
import {StatementInformationFormModule} from "../../../forms/statement-information";
import {NewStatementComponent} from "./new-statement.component";

describe("NewStatementComponent", () => {

    let component: NewStatementComponent;
    let fixture: ComponentFixture<NewStatementComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [
                NewStatementComponent
            ],
            imports: [
                I18nModule,
                RouterTestingModule,
                StatementInformationFormModule
            ],
            providers: [
                provideMockStore({})
            ]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(NewStatementComponent);
        component = fixture.componentInstance;
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
