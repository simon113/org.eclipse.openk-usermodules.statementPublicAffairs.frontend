/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {take} from "rxjs/operators";
import {I18nModule} from "../../../../core/i18n";
import {fetchEmailInboxAction} from "../../../../store/mail/actions";
import {isDivisionMemberSelector, isOfficialInChargeSelector} from "../../../../store/root/selectors";
import {fetchDashboardStatementsAction} from "../../../../store/statements/actions";
import {DashboardModule} from "../../dashboard.module";
import {DashboardComponent} from "./dashboard.component";

describe("DashboardComponent", () => {
    let component: DashboardComponent;
    let store: MockStore;
    let fixture: ComponentFixture<DashboardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                DashboardModule,
                I18nModule,
                RouterTestingModule
            ],
            providers: [
                provideMockStore({})
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should fetch data on init", () => {
        const dispatchSpy = spyOn(store, "dispatch");

        dispatchSpy.calls.reset();
        component.ngOnInit();
        expect(dispatchSpy).toHaveBeenCalledTimes(1);
        expect(dispatchSpy).toHaveBeenCalledWith(fetchDashboardStatementsAction());

        store.overrideSelector(isOfficialInChargeSelector, true);
        dispatchSpy.calls.reset();
        component.ngOnInit();
        expect(dispatchSpy).toHaveBeenCalledTimes(2);
        expect(dispatchSpy).toHaveBeenCalledWith(fetchDashboardStatementsAction());
        expect(dispatchSpy).toHaveBeenCalledWith(fetchEmailInboxAction());
    });

    it("should check if user is not an official in charge but a department member", () => {
        store.overrideSelector(isOfficialInChargeSelector, false);
        store.overrideSelector(isDivisionMemberSelector, false);
        component.isNotOfficialInChargeAndDivisionMember$.pipe(take(1))
            .subscribe((result) => expect(result).toBe(false));

        store.overrideSelector(isOfficialInChargeSelector, true);
        store.overrideSelector(isDivisionMemberSelector, false);
        component.isNotOfficialInChargeAndDivisionMember$.pipe(take(1))
            .subscribe((result) => expect(result).toBe(false));

        store.overrideSelector(isOfficialInChargeSelector, true);
        store.overrideSelector(isDivisionMemberSelector, true);
        component.isNotOfficialInChargeAndDivisionMember$.pipe(take(1))
            .subscribe((result) => expect(result).toBe(false));

        store.overrideSelector(isOfficialInChargeSelector, false);
        store.overrideSelector(isDivisionMemberSelector, true);
        component.isNotOfficialInChargeAndDivisionMember$.pipe(take(1))
            .subscribe((result) => expect(result).toBe(true));
    });

});
