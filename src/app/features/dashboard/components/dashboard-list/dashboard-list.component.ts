/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {BreakpointObserver} from "@angular/cdk/layout";
import {Component, Input} from "@angular/core";
import {ISelectOption} from "../../../../shared/controls/select/model";
import {StatementTableComponent} from "../../../../shared/layout/statement-table/components";
import {IStatementTableEntry} from "../../../../shared/layout/statement-table/model";

@Component({
    selector: "app-dashboard-list",
    templateUrl: "./dashboard-list.component.html",
    styleUrls: ["./dashboard-list.component.scss"]
})
export class DashboardListComponent {

    @Input()
    public appCaption: string;

    @Input()
    public appEntries: IStatementTableEntry[];

    @Input()
    public appShowContributionStatusForMyDepartment: boolean;

    @Input()
    public appShowSubCaption: boolean;

    @Input()
    public appStatementTypeOptions: ISelectOption<number>[];

    @Input()
    public appSubCaption: string;

    public columns = [...StatementTableComponent.DASHBOARD_COLUMNS];

    public columnsShort = [...StatementTableComponent.DASHBOARD_COLUMNS_SHORT];

    public large$ = this.breakpointObserver.observe("(min-width: 1280px)");

    public constructor(public breakpointObserver: BreakpointObserver) {

    }

}
