/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {EmailTextToArrayPipe} from "./email-text.pipe";

describe("EmailTextToArrayPipe", () => {

    const pipe = new EmailTextToArrayPipe();

    describe("transform", () => {

        it("should convert the input text to an array of the text divided by the newlines", () => {
            const inputText = `This is a test text \n\n and this is another row \n last row.`;
            const result: string[] = pipe.transform(inputText);
            expect(result.length).toBe(4);
            expect(result).toEqual(
                [
                    "This is a test text ",
                    "",
                    " and this is another row ",
                    " last row."
                ]
            );
        });
    });
});

