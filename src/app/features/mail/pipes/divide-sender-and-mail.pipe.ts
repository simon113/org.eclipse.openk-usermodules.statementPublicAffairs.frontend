/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {arrayJoin} from "../../../util/store";

/**
 * From given string with format: <name e@mail.com>
 * removes the brackets and returns name and email as separate strings in an array:
 * ["name", "e@mail.com"]
 */

@Pipe({
    name: "appDivideSenderAndMail"
})
export class DivideSenderAndMailPipe implements PipeTransform {

    public transform(text: string): Array<string> {
        const textAsArray = arrayJoin(text?.split(/(?=<)/g));
        return [textAsArray[0], textAsArray.slice(1).join("")].filter(x => x);
    }
}
