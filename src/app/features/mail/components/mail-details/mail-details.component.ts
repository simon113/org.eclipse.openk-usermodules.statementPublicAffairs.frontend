/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {IAPIEmailModel} from "../../../../core/api/mail";
import {momentFormatDisplayFullDateAndTime} from "../../../../util/moment";

@Component({
    selector: "app-mail-details",
    templateUrl: "./mail-details.component.html",
    styleUrls: ["./mail-details.component.scss"]
})
export class MailDetailsComponent {

    @Input()
    public appEmail: IAPIEmailModel;

    @Input()
    public appHideControls: boolean;

    @Input()
    public appDeleting: boolean;

    @Output()
    public appDownloadAttachment = new EventEmitter<{ mailId: string, name: string }>();

    @Output()
    public appRemoveFromInbox = new EventEmitter<string>();

    public dateFormat = momentFormatDisplayFullDateAndTime;

}
