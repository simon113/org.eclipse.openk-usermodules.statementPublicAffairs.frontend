/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core";
import {MailModule} from "../../mail.module";
import {MailComponent} from "./mail.component";

describe("MailComponent", () => {
    let store: MockStore;
    let component: MailComponent;
    let fixture: ComponentFixture<MailComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MailModule,
                RouterTestingModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        store = TestBed.inject(MockStore);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
