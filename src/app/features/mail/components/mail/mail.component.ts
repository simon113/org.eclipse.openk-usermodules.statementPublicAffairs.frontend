/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnDestroy, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {select, Store} from "@ngrx/store";
import {TranslateService} from "@ngx-translate/core";
import {combineLatest, Subject} from "rxjs";
import {distinctUntilChanged, filter, map, takeUntil, withLatestFrom} from "rxjs/operators";
import {ConfirmService} from "../../../../core";
import {
    deleteEmailFromInboxAction,
    downloadEmailAttachmentAction,
    fetchEmailAction,
    fetchEmailInboxAction,
    getEmailInboxSelector,
    getEmailLoadingSelector,
    getSelectedEmailSelector,
    queryParamsMailIdSelector
} from "../../../../store";

/**
 * This component displays the list of mails in the inbox.
 * Upon loading the inbox the first time, the first mail is automatically selected.
 * For the selected mail the mail details are displayed. That includes mail text, mail attachments, subject and sender information.
 * The mails can be deleted. Attachments can be downloaded.
 */

@Component({
    selector: "app-mail",
    templateUrl: "./mail.component.html",
    styleUrls: ["./mail.component.scss"]
})
export class MailComponent implements OnInit, OnDestroy {

    public loading$ = this.store.pipe(select(getEmailLoadingSelector));

    public emailInbox$ = this.store.pipe(select(getEmailInboxSelector));

    public selectedEmailId$ = this.store.pipe(select(queryParamsMailIdSelector)).pipe(distinctUntilChanged());

    public selectedEmail$ = this.store.pipe(select(getSelectedEmailSelector));

    private destroy$ = new Subject();

    public constructor(
        public store: Store,
        public readonly router: Router,
        private confirmService: ConfirmService,
        private translationService: TranslateService
    ) {

    }

    public ngOnInit() {
        this.fetchInbox();

        // Automatically fetch selected emails which are not part of the inbox
        this.selectedEmailId$.pipe(
            filter((mailId) => mailId != null),
            withLatestFrom(this.emailInbox$),
            filter(([mailId, inbox]) => !inbox.some((mail) => mail.identifier === mailId)),
            takeUntil(this.destroy$)
        ).subscribe(([mailId]) => this.fetch(mailId));

        // Automatically navigate to the first inbox item if no email is selected
        combineLatest([this.selectedEmailId$, this.emailInbox$]).pipe(
            filter(([mailId]) => mailId == null),
            map(([_, inbox]) => inbox[0]?.identifier),
            filter((firstMailId) => firstMailId != null),
            takeUntil(this.destroy$)
        ).subscribe((firstMailId) => this.selectMail(firstMailId));
    }

    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public fetchInbox() {
        this.store.dispatch(fetchEmailInboxAction());
    }

    public fetch(mailId: string) {
        this.store.dispatch(fetchEmailAction({mailId}));
    }

    public downloadAttachment(mailId: string, name: string) {
        this.store.dispatch(downloadEmailAttachmentAction({mailId, name}));
    }

    public async remove(mailId: string) {
        const confirmationText = await this.translationService.get("mails.confirmDelete").toPromise();
        if (this.confirmService.askForConfirmation(confirmationText)) {
            this.store.dispatch(deleteEmailFromInboxAction({mailId}));
        }
    }

    public selectMail(mailId: string) {
        return this.router.navigate([], {queryParams: {mailId}, replaceUrl: true});
    }

}
