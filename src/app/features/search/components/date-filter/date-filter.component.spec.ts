/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {I18nModule} from "../../../../core/i18n";
import {SearchModule} from "../../search.module";
import {DateFilterComponent} from "./date-filter.component";

describe("DateFilterComponent", () => {
    let component: DateFilterComponent;
    let fixture: ComponentFixture<DateFilterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SearchModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DateFilterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should emit appValueChange only if filter is active", () => {
        spyOn(component.appValueChange, "emit").and.callThrough();
        const date = new Date();
        component.emitNewValue(date);
        expect(component.appValueChange.emit).not.toHaveBeenCalled();
        component.appActive = true;
        component.emitNewValue(date);
        expect(component.appValueChange.emit).toHaveBeenCalledWith(date);
    });
});
