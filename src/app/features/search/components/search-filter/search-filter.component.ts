/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {defer, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {IAPISearchOptions} from "../../../../core/api/shared";
import {AbstractControlValueAccessorComponent} from "../../../../shared/controls/common";
import {ISelectOption} from "../../../../shared/controls/select/model";
import {momentFormatInternal, parseMomentToString} from "../../../../util/moment";
import {ALL_SEARCH_FILTER, ISearchFilterToDisplay} from "./ISearchFilterToDisplay";

/**
 * This component displays a selection of filters and generates search parameters.
 * Those search parameters can be used in the backend calls to filter the list of all statements for e.g. specific keywords or date ranges.
 * The displayed filters are set by the input property appFilters, e.g.
 *  {
 *      status: true,
 *      editedByMe: false
 *  }
 *  only displays the filter dropdown for status (finished/not finished) and the toggle for editedByMe state.
 */
@Component({
    selector: "app-search-filter",
    templateUrl: "./search-filter.component.html",
    styleUrls: ["./search-filter.component.scss"]
})
export class SearchFilterComponent extends AbstractControlValueAccessorComponent<IAPISearchOptions> implements OnInit {

    @Input()
    public appLoading: boolean;

    @Input()
    public appStatementTypeOptions: ISelectOption<number>[];

    @Input()
    public appShowSearch = true;

    @Output()
    public appFilterParams = new EventEmitter<IAPISearchOptions>();

    /**
     * For each set property key, the specific filter is displayed in the component.
     */
    @Input()
    public appFilters: ISearchFilterToDisplay = ALL_SEARCH_FILTER;

    public finishedOptions$: Observable<ISelectOption[]> = defer(() => this.getFinishedOptions());

    public constructor(public readonly translateService: TranslateService) {
        super();
    }

    public ngOnInit() {
        this.writeValue({}, true);
    }

    public toggleSearchParameter(label: keyof IAPISearchOptions, value?: boolean | string | number | Date) {
        this.setSearchParameter(label, this.appValue[label] != null ? undefined : value);
    }

    public setSearchParameter(label: keyof IAPISearchOptions, value?: boolean | string | number | Date) {
        const newValue: IAPISearchOptions = {
            ...this.appValue,
            [label]: typeof value === "object" ? parseMomentToString(value, momentFormatInternal, momentFormatInternal) : value
        };
        this.writeValue(newValue, true);
    }

    public disableAllFilters() {
        this.writeValue({q: this.appValue.q}, true);
    }

    public writeValue(obj: IAPISearchOptions, emit?: boolean) {
        obj = {...obj};
        Object.keys(obj)
            .filter((key) => obj[key] == null || obj[key] === "")
            .forEach((key) => delete obj[key]);
        super.writeValue(obj, emit);
    }

    private getFinishedOptions() {
        const options: ISelectOption[] = ["search.open", "search.finished"]
            .map((label, index) => ({label, value: index > 0}));
        return this.translateService.get(options.map((option) => option.label)).pipe(
            map((translation) => options.map((option) => ({...option, label: translation[option.label]})))
        );
    }

}

