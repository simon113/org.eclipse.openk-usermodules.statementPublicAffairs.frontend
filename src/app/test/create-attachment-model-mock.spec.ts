/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIAttachmentModel} from "../core";

export function createAttachmentModelMock(attachmentId: number, ...tagIds: string[]): IAPIAttachmentModel {
    return {
        id: attachmentId,
        name: "Attachment" + attachmentId,
        tagIds,
        size: Math.floor(Math.random() * 1024),
        timestamp: new Date().toISOString(),
        type: "text/plain"
    };
}
