/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {
    arrayJoin,
    arrayToEntities,
    deleteEntities,
    entitiesToArray,
    filterDistinctValues,
    objectToArray,
    setEntitiesObject,
    TStoreEntities,
    updateEntitiesObject
} from "./store.util";

describe("StoreUtil", () => {

    const list = [
        {
            id: 60,
            a: "a",
            b: "b"
        },
        {
            id: 100,
            c: "c",
            d: "d"
        }
    ];

    const getId = (item: { id: number }): number => 2 * item.id;
    const entities = {[getId(list[0])]: list[0], [getId(list[1])]: list[1]};

    it("should transform entities to arrays", () => {
        expect(entitiesToArray({})).toEqual([]);
        expect(entitiesToArray(null)).toEqual([]);
        expect(entitiesToArray(entities)).toEqual(list);
    });

    it("should delete entities", () => {
        expect(deleteEntities({})).toEqual({});
        expect(deleteEntities(null)).toEqual({});
        expect(deleteEntities(undefined)).toEqual({});

        expect(deleteEntities(entities)).toEqual(entities);
        expect(deleteEntities({1: 2, 3: 4}, [1])).toEqual({3: 4});
        expect(deleteEntities({a: "b", c: "d"}, ["a"])).toEqual({c: "d"});
        expect(deleteEntities({a: "b", c: "d"}, ["a", "c"])).toEqual({});
        expect(deleteEntities({1: 2, 3: 4}, [1, 19, 100])).toEqual({3: 4});
    });

    it("should transform arrays to entities", () => {
        expect(arrayToEntities([], getId)).toEqual({});
        expect(arrayToEntities(null, getId)).toEqual({});
        expect(arrayToEntities(list, getId)).toEqual(entities);
    });

    it("should join arrays", () => {
        expect(arrayJoin([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
        expect(arrayJoin([1, 2, 3], undefined)).toEqual([1, 2, 3]);
        expect(arrayJoin(null, [4, 5, 6])).toEqual([4, 5, 6]);
        expect(arrayJoin(undefined, null)).toEqual([]);
    });

    it("should set entities object", () => {
        const state: TStoreEntities<{ id: number; property: any }> = {1: {id: 1, property: "18"}};
        expect(setEntitiesObject(null, null, () => null)).toEqual(null);
        expect(setEntitiesObject(state, null, () => null)).toBe(state);
        expect(setEntitiesObject(state, [], () => null)).toBe(state);
        expect(setEntitiesObject(state, [null, {id: 1, property: "19"}], () => null)).toBe(state);
        expect(setEntitiesObject(state, [null, {id: 1, property: "19"}], (item) => item.id))
            .toEqual({1: {id: 1, property: "19"}});
        expect(setEntitiesObject(state, [null, {id: 1, property: "19"}], (item) => item.id))
            .toEqual({1: {id: 1, property: "19"}});
        expect(setEntitiesObject<{ id: number; property: any }>(undefined, [null, {id: 1, property: "19"}], (item) => item.id))
            .toEqual({1: {id: 1, property: "19"}});
    });

    it("should update entities object", () => {
        const state: TStoreEntities<{ id: number; property: any }> = {1: {id: 1, property: "18"}};
        expect(updateEntitiesObject(null, null, () => null)).toEqual(null);
        expect(updateEntitiesObject(state, null, () => null)).toBe(state);
        expect(updateEntitiesObject(state, [], () => null)).toBe(state);
        expect(updateEntitiesObject(state, [null, {id: 1}], () => null)).toBe(state);
        expect(updateEntitiesObject(state, [null, {id: 1}], (item) => item.id))
            .toEqual({1: {id: 1, property: "18"}});
        expect(updateEntitiesObject(state, [null, {id: 1, property: "19"}], (item) => item.id))
            .toEqual({1: {id: 1, property: "19"}});
        expect(updateEntitiesObject<{ id: number; property: any }>(undefined, [null, {id: 1, property: "19"}], (item) => item.id))
            .toEqual({1: {id: 1, property: "19"}});
    });

    it("should filter distinct values in arrays", () => {
        const array = [1, 1, 2, 3, 3, 3, undefined, 1, 2, 3, 3, null, null];
        expect(filterDistinctValues(array)).toEqual([1, 2, 3]);
        expect(filterDistinctValues(array, true)).toEqual([1, 2, 3, undefined, null]);
        expect(filterDistinctValues(undefined)).toEqual([]);
        expect(filterDistinctValues(null)).toEqual([]);
    });

    it("should transform object to key value arrays", () => {
        const testObject = {
            prop1: "propValue1",
            prop2: undefined,
            prop3: null
        };
        expect(objectToArray(testObject)).toEqual([{key: "prop1", value: "propValue1"}]);
        expect(objectToArray(testObject, false)).toEqual([{key: "prop1", value: "propValue1"}]);
        expect(objectToArray(testObject, true)).toEqual([
            {key: "prop1", value: "propValue1"},
            {key: "prop2", value: undefined},
            {key: "prop3", value: null}]
        );
        expect(objectToArray(undefined)).toEqual([]);
        expect(objectToArray(null)).toEqual([]);
    });

});

