/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {InjectionToken} from "@angular/core";
import {gis, leaflet, nominatim, routes} from "../../../../app.config.json";

export interface IAppConfiguration {
    gis: {
        urlTemplate: string;
        projectionFrom: string;
        projectionTo: string;
    };
    leaflet: {
        urlTemplate: string;
        attribution?: string;
        lat: number;
        lng: number;
        zoom: number;
    };
    nominatim: {
        url: string;
        searchQueryPrefix?: string;
    };
    routes: {
        spaBackend: string;
        portal: string;
        contactDataBase: string;
        userDocu: string;
    };
}

export const APP_CONFIGURATION = new InjectionToken<IAppConfiguration>("App configuration object", {
    providedIn: "root",
    factory: () => ({gis, leaflet, nominatim, routes})
});
