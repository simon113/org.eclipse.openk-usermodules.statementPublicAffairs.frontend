/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Attachment tags that cannot be set manually. Those are automatically set on specified task steps.
 * E.g. attachments tranferred from a mail are automatically assigned the "email" tag.
 */
export enum EAPIStaticAttachmentTagIds {

    // Used for the attachment generated from the mail body.
    EMAIL_TEXT = "email-text",

    // Used for the transferred mail attachments.
    EMAIL = "email",

    // Used for all attachments that will be send with the generated statement pdf.
    OUTBOX = "outbox",

    // Used for the attachments uploaded after the statement has been finished.
    CONSIDERATION = "consideration",

    // Used for the generated statement pdf from text block arrangement.
    STATEMENT = "statement",

    COVER_LETTER = "cover-letter"

}

export const AUTO_SELECTED_TAGS = [
    EAPIStaticAttachmentTagIds.EMAIL_TEXT,
    EAPIStaticAttachmentTagIds.EMAIL,
    EAPIStaticAttachmentTagIds.OUTBOX,
    EAPIStaticAttachmentTagIds.CONSIDERATION,
    EAPIStaticAttachmentTagIds.STATEMENT
];
