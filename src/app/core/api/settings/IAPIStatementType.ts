/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which models a statement type.
 */
export interface IAPIStatementType {

    /**
     * Unique ID of a specific satement type in the back end data base.
     */
    id: number;

    /**
     * Name of a specific statement type which is used for display.
     */
    name: string;
}
