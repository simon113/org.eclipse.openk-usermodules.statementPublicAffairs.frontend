/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which represents the model of all basic information of a specific statement in the back end data base.
 */
export interface IAPIStatementModel extends IAPIPartialStatementModel {

    id: number;

    finished: boolean;

}

export interface IAPIPartialStatementModel {

    title: string;

    dueDate: string;

    receiptDate: string;

    typeId: number;

    city: string;

    district: string;

    contactId: string;

    sourceMailId: string;

    creationDate: string;

    customerReference?: string;

}
