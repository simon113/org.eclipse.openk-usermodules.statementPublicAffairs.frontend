/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {objectToHttpParams, urlJoin} from "../../../util/http";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPIPaginationResponse, IAPISearchOptions} from "../shared";
import {IAPIContactPerson} from "./IAPIContactPerson";
import {IAPIContactPersonDetails} from "./IAPIContactPersonDetails";

@Injectable({providedIn: "root"})
export class ContactsApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string
    ) {

    }

    /**
     * Fetches a paginated list of contacts from the back end data base.
     */
    public getContacts(searchOptions: IAPISearchOptions) {
        const endPoint = `contacts`;
        const params = objectToHttpParams({...searchOptions});
        return this.httpClient.get<IAPIPaginationResponse<IAPIContactPerson>>(urlJoin(this.baseUrl, endPoint), {params});
    }

    /**
     * Fetches details of a specific contact from the back end data base.
     */
    public getContactDetails(contactId: string) {
        const endPoint = `contacts/${contactId}`;
        return this.httpClient.get<IAPIContactPersonDetails>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Fetches contact details of a specific statement from the back end data base.
     */
    public getStatementsContact(statementId: number) {
        const endPoint = `/statements/${statementId}/contact`;
        return this.httpClient.get<IAPIContactPersonDetails>(urlJoin(this.baseUrl, endPoint));
    }

}
