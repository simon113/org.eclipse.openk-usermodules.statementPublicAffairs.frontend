/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export enum EAPIProcessTaskDefinitionKey {

    ADD_BASIC_INFO_DATA = "addBasicInfoData",

    ADD_WORK_FLOW_DATA = "addWorkflowData",

    CREATE_DRAFT = "createDraft",

    CREATE_NEGATIVE_RESPONSE = "createNegativeResponse",

    ENRICH_DRAFT = "enrichDraft",

    CHECK_AND_FORMULATE_RESPONSE = "checkAndFormulateResponse",

    APPROVE_STATEMENT = "approveStatement",

    SEND_STATEMENT = "sendStatement"

}
