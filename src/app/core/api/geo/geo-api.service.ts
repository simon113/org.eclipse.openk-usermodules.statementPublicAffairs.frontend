/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {objectToHttpParams, urlJoin} from "../../../util/http";
import {APP_CONFIGURATION, IAppConfiguration} from "../../configuration";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPIGeographicPositions} from "./IAPIGeographicPositions";
import {IAPINominatimSearchResult} from "./IAPINominatimSearchResult";

@Injectable({providedIn: "root"})
export class GeoApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string,
        @Inject(APP_CONFIGURATION) protected readonly configuration: IAppConfiguration
    ) {

    }

    /**
     * Sends geo positions and input + output format and receives the transformed positions back.
     */
    public transform(geographicPositions: IAPIGeographicPositions, from: string, to: string): Observable<IAPIGeographicPositions> {
        const params = objectToHttpParams({from, to});
        const endPoint = `/geo-coordinate-transform`;
        return this.httpClient.post<IAPIGeographicPositions>(urlJoin(this.baseUrl, endPoint), geographicPositions, {params});
    }

    /**
     * Searches for a specific place to retrieve coordinates to display.
     */
    public search(searchQuery: string) {
        const endPoint = `search`;
        const params = objectToHttpParams({q: this.configuration.nominatim.searchQueryPrefix + " " + searchQuery, format: "json"});
        return this.httpClient.get<IAPINominatimSearchResult[]>(urlJoin(this.configuration.nominatim.url, endPoint), {params});
    }

}
