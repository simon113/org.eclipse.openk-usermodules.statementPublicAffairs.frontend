/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule, Optional, SkipSelf} from "@angular/core";
import {AuthInterceptorService, AuthModule} from "./auth";
import {I18nModule} from "./i18n";

@NgModule({
    imports: [
        AuthModule,
        I18nModule,
    ],
    providers: [
        AuthInterceptorService.HTTP_INTERCEPTOR_PROVIDER
    ]
})
export class CoreModule {

    constructor(@Optional() @SkipSelf() duplicate: CoreModule) {
        if (duplicate != null) {
            throw new Error("CoreModule is imported multiple times");
        }
    }
}
