/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Inject, Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from "@angular/router";
import {HELP_ROUTE} from "./user-docu-route.token";

@Injectable({
    providedIn: "root"
})
export class RedirectRouteGuard implements CanActivate {

    public constructor(
        @Inject(HELP_ROUTE) public helpRoute: string
    ) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (route.data.externalUrl === "help") {
            window.location.href = this.helpRoute;
        }
        return false;
    }

}
