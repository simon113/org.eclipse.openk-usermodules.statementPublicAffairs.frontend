/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DOCUMENT} from "@angular/common";
import {Inject, Injectable} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {concatMap, filter} from "rxjs/operators";
import {LOCAL_STORAGE} from "../dom";

@Injectable({providedIn: "root"})
export class AuthService {

    public readonly urlSearchParamKey = "accessToken";

    public readonly storageKey = "AuthService.token";

    private readonly deleteTokenFromActivatedRoute$ = this.activatedRoute.queryParams.pipe(
        filter((params) => params[this.urlSearchParamKey] != null),
        concatMap(() => {
            return this.router.navigate(this.activatedRoute.snapshot.url, {
                queryParams: {[this.urlSearchParamKey]: null},
                queryParamsHandling: "merge"
            });
        })
    );

    constructor(
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        @Inject(DOCUMENT) private readonly document: Document,
        @Inject(LOCAL_STORAGE) private readonly localStorage: Storage
    ) {

    }

    private _token: string;

    public get token(): string {
        return this._token;
    }

    /**
     * Initializes the service with a token extracted either from the current URL's query parameters or from the web storage.
     * @return Returns only true if a token can be successfully extracted.
     */
    public initialize(): boolean {
        this._token = this.extractTokenFromCurrentLocation();

        if (this._token != null) {
            this.localStorage.setItem(this.storageKey, this._token);
            this.deleteTokenFromActivatedRoute$.subscribe();
            return true;
        }

        this._token = this.extractTokenFromStorage();
        return this._token != null;
    }

    /**
     * Deletes the token and empties the web storage.
     */
    public clear() {
        this._token = null;
        this.localStorage.removeItem(this.storageKey);
    }

    /**
     * Extracts the token from the query parameters of the current URL.
     */
    public extractTokenFromCurrentLocation(): string {
        const search = this.document.location.search;
        const token = new URLSearchParams(search).get(this.urlSearchParamKey);
        return typeof token === "string" && token.length > 0 ? token : undefined;
    }

    /**
     * Extracts the token from the web storage.
     */
    public extractTokenFromStorage(): string {
        const token = this.localStorage.getItem(this.storageKey);
        return typeof token === "string" && token.length > 0 ? token : undefined;
    }

}
