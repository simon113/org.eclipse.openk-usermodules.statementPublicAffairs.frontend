/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component} from "@angular/core";
import {TestBed} from "@angular/core/testing";
import {TranslateService} from "@ngx-translate/core";
import * as moment from "moment";
import {take} from "rxjs/operators";
import {WINDOW} from "../dom";
import {I18nModule} from "./i18n.module";
import {I18nService} from "./i18n.service";

class WindowMockDE {
    public navigator: any = {language: "de-XX"};
}

class WindowMockError {
    public navigator: undefined;
}

@Component({
    selector: `app-translate-component`,
    template: `{{text | translate}}`
})
class TestDisplayTranslateTextComponent {
    public text: string;
}

describe("I18nService", () => {

    describe("(basic)", () => {
        let i18nService: I18nService;
        let translateService: TranslateService;

        beforeEach(async () => {
            await TestBed.configureTestingModule({
                imports: [I18nModule],
                declarations: [
                    TestDisplayTranslateTextComponent
                ],
                providers: [
                    {provide: WINDOW, useClass: WindowMockDE}
                ]
            }).compileComponents();

            i18nService = TestBed.inject(I18nService);
            translateService = TestBed.inject(TranslateService);
        });

        it("should be created", () => {
            expect(i18nService).toBeTruthy();
        });

        it("should check if a certain language is supported", () => {
            expect(i18nService.isLanguageSupported("nl")).toBe(false);
            expect(i18nService.isLanguageSupported("not supported language")).toBe(false);
            expect(i18nService.isLanguageSupported("de")).toBe(true);
        });

        it("should get the current browser language", () => {
            expect(i18nService.getCurrentLanguageFromWindow()).toEqual("de");
        });

        it("should load translations from the assets folder", async () => {
            const fixture = TestBed.createComponent(TestDisplayTranslateTextComponent);
            const component = fixture.componentInstance;
            const text = "core.title";

            const translatedText = await translateService.get(text).pipe(take(1)).toPromise();
            expect(translatedText).toBe("Stellungnahmen öffentlicher Belange");

            component.text = "core.title";
            fixture.detectChanges();
            await fixture.whenStable();
            const nativeElement: HTMLElement = fixture.nativeElement;
            expect(nativeElement.innerText).toBe("Stellungnahmen öffentlicher Belange");
        });

        it("should change the language of MomentJS", async () => {
            await i18nService.setLanguageForMomentJS("de");
            expect(moment.locale()).toEqual("de");
        });

        it("should set the TranslateServices language to window language", async () => {
            spyOn(translateService, "use");
            await i18nService.initialize();
            expect(translateService.use).toHaveBeenCalledWith("de");
        });
    });

    describe("(error handling)", () => {
        let i18nService: I18nService;
        let translateService: TranslateService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [I18nModule],
                declarations: [],
                providers: [
                    {provide: WINDOW, useClass: WindowMockError}
                ]
            });
            i18nService = TestBed.inject(I18nService);
            translateService = TestBed.inject(TranslateService);
        });

        it("should ignore errors from MomentJS", async () => {
            spyOn(moment, "locale").and.throwError("error");
            const returnValue = await i18nService.setLanguageForMomentJS("de");
            expect(returnValue).toEqual(undefined);
        });

        it("should ignore errors from Window and return no language instead", async () => {
            const returnValue = await i18nService.getCurrentLanguageFromWindow();
            expect(returnValue).toEqual(undefined);
        });
    });
});
